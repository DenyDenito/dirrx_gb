﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemExecutionAssignment;

namespace SC.BurGazSolution.Server
{
	partial class ActionItemExecutionAssignmentFunctions
	{
		[Remote, Public]
		public static void CompliteAssignment(IActionItemExecutionAssignment job)
		{
			AccessRights.AllowRead(
				() =>
				{
					job.ActiveText = "Автоматическое выполнение";
					job.Complete(SC.BurGazSolution.ActionItemExecutionAssignment.Result.Done);
				});
		}
	}
}