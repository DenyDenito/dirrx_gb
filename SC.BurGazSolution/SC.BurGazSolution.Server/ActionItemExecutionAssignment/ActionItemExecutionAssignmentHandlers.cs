﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemExecutionAssignment;

namespace SC.BurGazSolution
{
  partial class ActionItemExecutionAssignmentServerHandlers
  {

		public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
		{
			base.BeforeComplete(e);
//			AccessRights.AllowRead(
//				() =>
//				{
			if (_obj.Result == Result.Done) //Если результат "выполнено"
			{
				//Выполнить ведущее задание автоматически
				var task = ActionItemExecutionTasks.As(_obj.Task);
				if (task.AutoPerfGB != null && task.AutoPerfGB.Value && !task.IsUnderControl.Value)
				{
					IActionItemExecutionTask current_task;
					if (task.ParentTask!= null && ActionItemExecutionTasks.Is(task.ParentTask))
					{
						current_task = ActionItemExecutionTasks.As(task.ParentTask);
						//Проверяем, что все остальные задания выполнены
						if (current_task.Subtasks.Count(x => x.Status == (ActionItemExecutionAssignment.Status.Completed) || x.Status == (ActionItemExecutionAssignment.Status.Aborted)) == (current_task.Subtasks.Count()-1))
						{
							if (current_task.ParentAssignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
							{
								var job = ActionItemExecutionAssignments.As(current_task.ParentAssignment);
								if (job != null)
									PublicFunctions.Module.Remote.CompliteAssignmentSC(job);
								//syscon.Assignment.PublicFunctions.Module.Remote.CompliteAssignment2(job);
								//syscon.Assignment.PublicFunctions.ActionItemExecutionAssignment.Remote.CompliteAssignment(job);
							}
						}
					}
					else
					{
						if (task.ParentAssignment != null)
						{
							if (task.ParentAssignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
							{
								var job = ActionItemExecutionAssignments.As(task.ParentAssignment);
								if (job != null)
									PublicFunctions.Module.Remote.CompliteAssignmentSC(job);
								//syscon.Assignment.PublicFunctions.ActionItemExecutionAssignment.Remote.CompliteAssignment(job);
							}
						}
					}
					
					
				}
			}
			//	});
		}
  }

}