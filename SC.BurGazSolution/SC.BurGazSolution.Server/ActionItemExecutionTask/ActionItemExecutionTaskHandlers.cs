﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemExecutionTask;

namespace SC.BurGazSolution
{


	partial class ActionItemExecutionTaskServerHandlers
	{

		public override void Created(Sungero.Domain.CreatedEventArgs e)
		{
			base.Created(e);
			
			_obj.AutoPerfGB = false;
		}

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
			if (_obj.Importance == Importance.High && _obj.Supervisor == null)
			{
				_obj.IsUnderControl = true;
				var supervisor = Sungero.Docflow.PublicFunctions.PersonalSetting.GetSupervisor(Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(Employees.Current));
				_obj.Supervisor = supervisor != null ? supervisor : Employees.Current;
			}
			base.BeforeSave(e);
		}
	}

}