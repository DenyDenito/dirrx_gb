﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using SC.BurGazSolution.ActionItemExecutionTask;

namespace SC.BurGazSolution.Server
{
	partial class ActionItemExecutionTaskRouteHandlers
	{

		public override void CompleteAssignment6(Sungero.RecordManagement.IActionItemSupervisorAssignment assignment, Sungero.RecordManagement.Server.ActionItemSupervisorAssignmentArguments e)
		{
			base.CompleteAssignment6(assignment, e);
			
			var tasks = assignment.Subtasks.Where(s => GB.RouteDev.DoubleControlTasks.Is(s));
			
			foreach (var task in tasks)
			{
				if (task.Status == Status.InProcess || task.Status == Status.UnderReview || task.Status == Status.Suspended)
				{
					task.Abort();
					task.Save();
				}
			}
		}

		public override void StartBlock8(Sungero.RecordManagement.Server.ActionItemExecutionNotificationArguments e)
		{
			base.StartBlock8(e);
			
			var assignment = GB.RouteDev.DoubleControlAssignments.GetAll()
				.Where(s => s.Task.ParentAssignment != null && Equals(s.Task.ParentAssignment.Task, _obj))
				.OrderByDescending(s => s.Id)
				.FirstOrDefault();

			if (assignment != null && assignment.Result == GB.RouteDev.DoubleControlAssignment.Result.Agree)
			{
				if (_obj.StartedBy != null)
					e.Block.Performers.Remove(_obj.StartedBy);
				
				e.Block.Performers.Remove(_obj.Author);
					
//				var performer = e.Block.Performers.Where(p => p.Id == _obj.Author.Id
//				                                         || (_obj.StartedBy != null && p.Id == _obj.StartedBy.Id)).FirstOrDefault();
//				
//				if (!e.Block.Performers.Remove(performer) && e.Block.Performers.Count > 1)
//					e.Block.Performers.RemoveAt(1);
			}
		}
	}

}