﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.IncomingLetter;
using Sungero.Docflow;
using Sungero.RecordManagement;
using Sungero.Workflow;

namespace SC.BurGazSolution.Server
{
	partial class IncomingLetterFunctions
	{
		/// <summary>
		/// Создать задачу по процессу "Рассмотрение входящего".
		/// </summary>
		/// <param name="document">Документ на рассмотрение.</param>
		/// <returns>Задача по процессу "Рассмотрение входящего".</returns>
		[Remote(PackResultEntityEagerly = true)]
		public static ITask CreateDocumentReview(IEmployee addressee, IOfficialDocument document)
		{
			var task = DocumentReviewTasks.Create();
			var doc = IncomingLetters.As(document);

			task.DocumentForReviewGroup.All.Add(document);
			task.Addressee = addressee;
			
			if (doc.DeadlineGB != null && doc.DeadlineGB > Calendar.Now)
			{
				task.Deadline = doc.DeadlineGB;
			}
			else if (doc.DeadlineGB < Calendar.Now)
				task.Deadline = Calendar.Today;
			else
				task.Deadline = null;
			
			// Выдать права группе регистрации документа.
			if (document.DocumentRegister != null)
			{
				var registrationGroup = document.DocumentRegister.RegistrationGroup;

				if (registrationGroup != null && !task.AccessRights.IsGranted(DefaultAccessRightsTypes.Change, registrationGroup))
					task.AccessRights.Grant(registrationGroup, DefaultAccessRightsTypes.Change);
			}

			return task;
		}
	}
}