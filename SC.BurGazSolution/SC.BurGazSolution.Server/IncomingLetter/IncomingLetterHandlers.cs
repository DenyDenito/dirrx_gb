﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.IncomingLetter;

namespace SC.BurGazSolution
{
	partial class IncomingLetterServerHandlers
	{

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
			if (string.IsNullOrEmpty(_obj.AddresseesStrGB) && _obj.AddresseesGB.Count > 0)
			{
				_obj.AddresseesStrGB = string.Join(", ", _obj.AddresseesGB.Select(s => s.Addressee.Person.ShortName));
			}
			base.BeforeSave(e);
		}
	}

}