﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace SC.BurGazSolution.Module.Shell.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			base.Initializing(e);
			var allUsers = Roles.AllUsers;
			if (allUsers != null)
			{
				Shell.SpecialFolders.ImportantGB.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
				Shell.SpecialFolders.ImportantCompletedGB.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
//				Shell.SpecialFolders.NotReadGB.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
				Shell.SpecialFolders.ImportantGB.AccessRights.Save();
				Shell.SpecialFolders.ImportantCompletedGB.AccessRights.Save();
//				Shell.SpecialFolders.NotReadGB.AccessRights.Save();
			}
		}
		
	}
}
