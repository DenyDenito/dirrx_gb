﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.BurGazSolution.Module.Shell.Server
{

	partial class NotWrittenAssignmentGBFolderHandlers
	{

		public virtual IQueryable<Sungero.Workflow.IAssignmentBase> NotWrittenAssignmentGBDataQuery(IQueryable<Sungero.Workflow.IAssignmentBase> query)
		{
			var result = query
				.Where(e => Equals(e.Performer, Users.Current))
				.Where(e => ReviewManagerAssignments.Is(e)
				       || ReviewResolutionAssignments.Is(e)
				       || ActionItemExecutionAssignments.Is(e)
				       || ApprovalReviewAssignments.Is(e))
				.Where(e => !Sungero.Workflow.Tasks.GetAll().Any(s => Equals(s.StartedBy, Users.Current) && Equals(s.ParentAssignment, e)))
				.Where(e => e.Status == Sungero.Workflow.AssignmentBase.Status.InProcess);
			
			// Запрос непрочитанных без фильтра.
//			if (_filter == null)
//				return Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result);
			
			// Фильтры по статусу, замещению и периоду.
//			result = Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result, _filter.InProcessGB,
//			                                                                             _filter.Last30DaysGB, _filter.Last90DaysGB, _filter.Last180DaysGB, false);
			
			return result;
		}
		
	}


	partial class ImportantCompletedGBFolderHandlers
	{

		public virtual bool IsImportantCompletedGBVisible()
		{
			return Sungero.Docflow.PublicFunctions.Module.IncludedInBusinessUnitHeadsRole() ||
				Sungero.Docflow.PublicFunctions.Module.IncludedInDepartmentManagersRole() ||
				Sungero.Docflow.PublicFunctions.Module.IncludedInClerksRole();
		}

		public virtual IQueryable<Sungero.RecordManagement.IActionItemExecutionTask> ImportantCompletedGBDataQuery(IQueryable<Sungero.RecordManagement.IActionItemExecutionTask> query)
		{
			var result = query
				.Where(t => t.Importance == Sungero.RecordManagement.ActionItemExecutionTask.Importance.High && t.Status == Sungero.RecordManagement.ActionItemExecutionTask.Status.Completed);
			
			return result;
		}
	}

	partial class ImportantGBFolderHandlers
	{

		public virtual bool IsImportantGBVisible()
		{
			return Sungero.Docflow.PublicFunctions.Module.IncludedInBusinessUnitHeadsRole() ||
				Sungero.Docflow.PublicFunctions.Module.IncludedInDepartmentManagersRole() ||
				Sungero.Docflow.PublicFunctions.Module.IncludedInClerksRole();
		}
		
		public virtual IQueryable<Sungero.RecordManagement.IActionItemExecutionTask> ImportantGBDataQuery(IQueryable<Sungero.RecordManagement.IActionItemExecutionTask> query)
		{
//			var typeFilterEnabled = _filter != null && (_filter.RuleBased || _filter.Free);
//			var showRuleBasedApproval = !typeFilterEnabled || _filter.RuleBased;
//			var showFreeApproval = !typeFilterEnabled || _filter.Free;
			
			var result = query
				.Where(t => t.Importance == Sungero.RecordManagement.ActionItemExecutionTask.Importance.High && t.Status == Sungero.RecordManagement.ActionItemExecutionTask.Status.InProcess);
//				                                                                                                 || t.Status == Sungero.RecordManagement.ActionItemExecutionTask.Status.Suspended
//				                                                                                                 || t.Status == Sungero.RecordManagement.ActionItemExecutionTask.Status.UnderReview));
			
//			if (_filter == null)
//				return Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result);
//
//			// Фильтры по статусу и периоду.
//			result = Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result, _filter.InProcess,
//			                                                                             _filter.Last30Days, _filter.Last90Days, _filter.Last180Days, false);
			
			return result;
		}
		
	}

	partial class OnChekingFolderHandlers
	{
		
		public override IQueryable<Sungero.Workflow.IAssignmentBase> OnChekingDataQuery(IQueryable<Sungero.Workflow.IAssignmentBase> query)
		{
			var queryOld = base.OnChekingDataQuery(query).Select(s => s.Id);
			
			// Фильтр по типу.
			var typeFilterEnabled = _filter != null && (_filter.ActionItem || _filter.Other);
			var showActionItems = !typeFilterEnabled || _filter.ActionItem;
			var showOthers = !typeFilterEnabled || _filter.Other;
			
			var result = query
				.Where(a => showActionItems && GB.RouteDev.DoubleControlAssignments.Is(a));
			
			// Запрос непрочитанных без фильтра.
			if (_filter == null)
				result = Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result);
			else
				result = Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result, _filter.InProcess,
				                                                                                     _filter.Last30Days, _filter.Last90Days, _filter.Last180Days, false);
			var queryNew = result.Select(s => s.Id);
			
			return query.Where(q => queryOld.Contains(q.Id) || queryNew.Contains(q.Id));
		}
		
	}

	partial class ShellHandlers
	{
	}
}