﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemSupervisorAssignment;

namespace SC.BurGazSolution.Server
{
	partial class ActionItemSupervisorAssignmentFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public GB.RouteDev.IDoubleControlTask CreateDoubleControlSubtask()
		{
			return GB.RouteDev.DoubleControlTasks.CreateAsSubtask(_obj);
		}

	}
}