﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Memo;

namespace SC.BurGazSolution.Server
{
	partial class MemoFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public StateView GetMemoSС()
		{
			
			var stateView = StateView.Create();

			//Информация по созданию документа
			string created_info = "Создание: " + _obj.Created.ToString() + " Автор: " + _obj.Author.Name + "\n Подписант: "  + _obj.OurSignatory.Name + "\n Адресаты: " + _obj.AddresseesStrGB ;
			var mainblock = addBlock(stateView,created_info,Sungero.Core.StateBlockIconType.User, StateBlockIconSize.Small,true,null,null,null,false, null);
			
			#region формируем задачу по согласованию
			var tasks = Sungero.Docflow.ApprovalTasks.GetAll()
				.Where(task => task.AttachmentDetails
				       .Any(a => a.AttachmentId == _obj.Id))
				.OrderBy(task => task.Created)
				.ToList();

			foreach (var task in tasks)
			{
				string task_info = "Согласование: " + task.Started.ToString() + " Конечный срок: " + task.MaxDeadline.ToString();
				var block_apptask = addBlock(stateView,task_info,ApprovalTasks.Resources.OldApprove, StateBlockIconSize.Small,true,null,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),true, task.MaxDeadline);
				
				//получаем все задания задачи
				
				var AppAss = Sungero.Workflow.Assignments.GetAll().Where(x => x.Task.Equals(task)).OrderBy(y => y.Created);
				foreach (var AppAs in AppAss)
				{
					var block_AppAs = addBlock(stateView,"",null,StateBlockIconSize.Small,true,block_apptask,AppAs,AppAs.Info.Properties.Status.GetLocalizedValue(AppAs.Status.Value),false, AppAs.Deadline);
					//проверяем есть ли подзадачи
					addsimpletaskblockSC(stateView,block_AppAs,AppAs.Id);
				}
				//Пробегаемся по всем блокам, записывая заголовок и иконку.
				foreach (var block in block_apptask.ChildBlocks)
				{
					StateBlockIconType icon;
					string label;
					IconForAssignment(Sungero.Workflow.Assignments.As(block.Entity), out icon, out label);
					block.AssignIcon(icon,StateBlockIconSize.Small);
					block.AddLabel(label);
				}
			}
			
			#endregion
			
			#region формируем задачи на рассмотрение
			var tasks_review = SC.BurGazSolution.DocumentReviewTasks.GetAll()
				.Where(task => task.AttachmentDetails
				       .Any(a => a.AttachmentId == _obj.Id))
				.OrderBy(task => task.Created)
				.ToList();
			
			foreach (var task in tasks_review)
			{
				
				//var block_apptask = addBlock(stateView,task_info,ApprovalTasks.Resources.OldApprove, StateBlockIconSize.Small,true,null,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),true, task.MaxDeadline);
				
				//получаем все задания задачи
				
				var AppAss = Sungero.Workflow.Assignments.GetAll().Where(x => x.Task.Equals(task)).OrderBy(y => y.Created);
				foreach (var AppAs in AppAss)
				{
					
					StateBlockIconType icon;
					string label;
					var block_AppAs = addBlock(stateView,"",null,StateBlockIconSize.Small,true,null,AppAs,AppAs.Info.Properties.Status.GetLocalizedValue(AppAs.Status.Value),false, AppAs.Deadline);
					IconForAssignment(Sungero.Workflow.Assignments.As(block_AppAs.Entity), out icon, out label);
					block_AppAs.AssignIcon(icon,StateBlockIconSize.Small);
					block_AppAs.AddLabel(label);
					
					//проверяем есть ли подзадачи
					addsimpletaskblockSC(stateView,block_AppAs,AppAs.Id);
					//Пробегаемся по всем блокам, записывая заголовок и иконку.
					foreach (var block in block_AppAs.ChildBlocks)
					{
						IconForAssignment(Sungero.Workflow.Assignments.As(block.Entity), out icon, out label);
						block.AssignIcon(icon,StateBlockIconSize.Small);
						block.AddLabel(label);
					}
					
//					x => x.MainTask.Equals(task)
//					Sungero.RecordManagement.
					var ExItms = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
						.Where(x => Equals(x.Task.ParentAssignment, AppAs)
						       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, x.Task.ParentTask) && t.IsCompoundActionItem == true && Equals(t.ParentAssignment, AppAs)))
						.OrderBy(y => y.Created);
					
					foreach (var ExItm in ExItms)
					{
						var block_ExItm = addBlock(stateView,"",null,StateBlockIconSize.Small,true,block_AppAs,ExItm,ExItm.Info.Properties.Status.GetLocalizedValue(ExItm.Status.Value),false, ExItm.Deadline);
						IconForAssignment(Sungero.Workflow.Assignments.As(block_ExItm.Entity), out icon, out label);
						block_ExItm.AssignIcon(icon,StateBlockIconSize.Small);
						block_ExItm.AddLabel(label);
						addexicutiontaskblockSC(stateView,block_ExItm,ExItm.Id);
						addsimpletaskblockSC(stateView,block_ExItm,ExItm.Id);
						
						//Задание контроль по блоку
					}
					
				}
				
				
			}
			
			#endregion
			
			return stateView;
		}
				
		private static void IconForAssignment(Sungero.Workflow.IAssignment Assignment, out Sungero.Core.StateBlockIconType icon, out string label)
		{
			bool AppAs = false;
			label = "";
			icon = StateBlockIconType.OfEntity;
			if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
			{
				icon = StateBlockIconType.Completed;
			}
			if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
			{
				icon = StateBlockIconType.Abort;
			}
			string base_label = Assignment.Completed.ToString() + " " + Assignment.Performer.Name  + " Cрок: " + Assignment.Deadline.ToString();
			
			//Если тип согласование, согласование с руководителем
			AppAs = Sungero.Docflow.ApprovalAssignments.Is(Assignment) || Sungero.Docflow.ApprovalManagerAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Согласование: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип задание
			AppAs = Sungero.Docflow.ApprovalCheckingAssignments.Is(Assignment) || Sungero.Docflow.ApprovalSimpleAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Задание: " + base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип контроль возврата
			AppAs = Sungero.Docflow.CheckReturnAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Контроль возврата: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Создание поручений
			AppAs = Sungero.Docflow.ApprovalExecutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Создание поручений: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип печать
			AppAs = Sungero.Docflow.ApprovalPrintingAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Печать: " + base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Регистрация
			AppAs = Sungero.Docflow.ApprovalRegistrationAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Регистрация документа: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассмотрение в рамках регламента
			AppAs = Sungero.Docflow.ApprovalReviewAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Рассмотрение: " + base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}

			//Если тип Доработка документа
			AppAs = Sungero.Docflow.ApprovalReworkAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Доработка документа: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип отпрвка документа
			AppAs = Sungero.Docflow.ApprovalSendingAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Отправка документа: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Подписание
			AppAs = Sungero.Docflow.ApprovalSigningAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Подписание: " + base_label;
				if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
					icon = StateBlockIconType.Signed;
				}
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Уведомление
			AppAs = Sungero.Docflow.ApprovalNotifications.Is(Assignment) || Sungero.Docflow.ApprovalSimpleNotifications.Is(Assignment);
			if (AppAs)
			{
				label = "Уведомление: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			string base_label_review = Assignment.Completed.ToString() + " " + Assignment.Performer.Name  + " Cрок: " + Assignment.Deadline.ToString();
			//Если тип Подготовка проекта резолюции
			AppAs = Sungero.RecordManagement.PreparingDraftResolutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Подготовка проекта резолюции: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассморение проекта резолюции
			AppAs = Sungero.RecordManagement.ReviewDraftResolutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Рассморение проекта резолюции: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассморение проекта резолюции
			AppAs = Sungero.RecordManagement.ReviewManagerAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Рассморение: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассморение проекта резолюции
			AppAs = Sungero.RecordManagement.ReviewResolutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Резолюция: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Исполнение поручений
			AppAs = Sungero.RecordManagement.ActionItemExecutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Поручение: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Контроль исполнения поручений
			AppAs = Sungero.RecordManagement.ActionItemSupervisorAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Приемка работ: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
		}
		
		[Public]
		public static Sungero.Core.StateBlock addBlock(StateView st, string label, Sungero.Core.StateBlockIconType icon, Sungero.Core.StateBlockIconSize iconsize, bool showborder, Sungero.Core.StateBlock mainblock, Sungero.Domain.Shared.IEntity entity, string content, bool Expanded, Nullable<DateTime> deadline)
		{
			StateBlock block;
			if (mainblock != null)
			{
				block = mainblock.AddChildBlock();
			}
			else
			{
				block = st.AddBlock();
			}
			if (content != null)
			{
//				var cont1 = block.AddContent();
//				Logger.DebugFormat("!!!!!!!!!!!!!!! {0}", label);
//				cont1.AddLabel(label);
				block.AddLabel(label);
				var cont2 = block.AddContent();
//				Logger.DebugFormat("!!!!!!!!!!!!!!! {0}", content);
				cont2.AddLabel(content);
			}
			else
			{
				block.AddLabel(label);
			}
			
			if (deadline != null && deadline < Calendar.Now)
			{
				block.Background = Colors.FromRgb(255,200,200);
			}
			if (icon !=null && iconsize != null)
			{
				block.AssignIcon(icon,iconsize);
			}
			block.ShowBorder = showborder;
			block.Entity = entity;
			block.IsExpanded = Expanded;
			
			
			
			return block;
			
		}
		[Public]
		public static Sungero.Core.StateBlock addBlock(StateView st, string label, string icon, Sungero.Core.StateBlockIconSize iconsize, bool showborder, Sungero.Core.StateBlock mainblock, Sungero.Domain.Shared.IEntity entity, string content, bool Expanded, Nullable<DateTime> deadline)
		{
			StateBlock block;
			if (mainblock != null)
			{
				block = mainblock.AddChildBlock();
			}
			else
			{
				block = st.AddBlock();
			}
			if (content != null)
			{
//				var cont1 = block.AddContent();
//				cont1.AddLabel(label);
				block.AddLabel(label);
				var cont2 = block.AddContent();
				cont2.AddLabel(content);
			}
			else
			{
				block.AddLabel(label);
			}
			if (icon != "")
				block.AssignIcon(icon,iconsize);
			
			if (deadline != null && deadline < Calendar.Now)
			{
				block.Background = Colors.FromRgb(255,200,200);
			}
			
			block.ShowBorder = showborder;
			block.Entity = entity;
			block.IsExpanded = Expanded;
			
			
			return block;
			
		}
		
		[Public]
		public static void addsimpletaskblockSC(Sungero.Core.StateView st, Sungero.Core.StateBlock mainblock, int IDAss)
		{
			//прояеряем есть ли для задачи подзадачи
			var tasks = Sungero.Workflow.SimpleAssignments.GetAll().Where(y => Sungero.Workflow.SimpleTasks.GetAll().Any(x => x.ParentAssignment.Id == IDAss && y.Task.Equals(x)));
			foreach (var task in tasks)
			{
				string label = task.Subject.Trim() + " " + task.Created.ToString()  +  "\n" + task.Performer.Name + " Срок: " +  task.Deadline.ToString();
//				Logger.DebugFormat("!!!!!!!!!!!!!!! {0}", label);
				StateBlockIconType icon = StateBlockIconType.OfEntity;
				StateBlockIconSize iconsize = StateBlockIconSize.Small;
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
//					label += "; " + task.ActiveText;
					label += !string.IsNullOrWhiteSpace(task.ActiveText) ? "; «" + task.ActiveText + "»" : string.Empty;
					icon = StateBlockIconType.Completed;
				}
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
				{
					icon = StateBlockIconType.Abort;
				}

				var block = addBlock(st,label,icon,iconsize,true,mainblock,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),false,task.Deadline);
				addsimpletaskblockSC(st,block,task.Id);
			}
			
			//Создаёт блок по подзадаче
			
			//Вызываем рекурсивно данную функцию
		}
		
		[Public]
		public static void addexicutiontaskblockSC(Sungero.Core.StateView st, Sungero.Core.StateBlock mainblock, int IDAss)
		{
			//прояеряем есть ли для задачи подзадачи
			
//			var ExItms = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
//				.Where(x => Equals(x.Task.ParentAssignment, AppAs)
//				       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, x.Task.ParentTask) && t.IsCompoundActionItem == true && Equals(t.ParentAssignment, AppAs)))
//				.OrderBy(y => y.Created);
			
			//var tasks = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll().Where(x => x.ParentAssignment.Id == IDAss);
			
//			var tasks = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
//				.Where(y => Sungero.RecordManagement.ActionItemExecutionTasks.GetAll()
//				       .Any(x => y.Task.Equals(x) && x.ParentAssignment.Id == IDAss));
			
			var tasks = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
				.Where(y => y.Task.ParentAssignment.Id == IDAss
				       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, y.Task.ParentTask) && t.IsCompoundActionItem == true && t.ParentAssignment.Id == IDAss));
			foreach (var task in tasks)
			{
				string label = task.Subject + " " + task.Created.ToString()  +  "\n" + task.Performer.Name + " Срок: " +  task.Deadline.ToString();
				StateBlockIconType icon = StateBlockIconType.OfEntity;
				StateBlockIconSize iconsize = StateBlockIconSize.Small;
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
					label += ": " + task.ActiveText;
					icon = StateBlockIconType.Completed;
				}
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
				{
					icon = StateBlockIconType.Abort;
				}

				var block = addBlock(st,label,icon,iconsize,true,mainblock,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),false, task.Deadline);
				addsimpletaskblockSC(st,block, task.Id);
				addexicutiontaskblockSC(st,block, task.Id);
			}
			
			//Создаёт блок по подзадаче
			
			//Вызываем рекурсивно данную функцию
		}
		
		
		[Public]
		public static StateView GetStateViewAppTask(StateView stateView, Sungero.Docflow.IApprovalTask task)
		{
//			// Добавить заголовок отправки для стартованной задачи.
//			//var stateView = StateView.Create();
//
//			var iterations = Functions.Module.GetIterationDates(task);
//
//			var comment = Functions.Module.GetTaskUserComment(task, ApprovalTasks.Resources.ApprovalText);
//
//			var startedByUser = Sungero.CoreEntities.Users.As(Sungero.Workflow.WorkflowHistories.GetAll()
//			                                                  .Where(h => h.EntityId == task.Id)
//			                                                  .Where(h => h.Operation == Sungero.Workflow.WorkflowHistory.Operation.Start)
//			                                                  .OrderBy(h => h.HistoryDate)
//			                                                  .Select(h => h.User)
//			                                                  .FirstOrDefault());
//
//			if (task.Started.HasValue)
//				Docflow.PublicFunctions.OfficialDocument
//					.AddUserActionBlock(stateView, task.Author, ApprovalTasks.Resources.StateViewDocumentSentForApproval, task.Started.Value, task, comment, startedByUser);
//			else
//				Docflow.PublicFunctions.OfficialDocument
//					.AddUserActionBlock(stateView, task.Author, ApprovalTasks.Resources.StateViewTaskDrawCreated, task.Created.Value, task, comment, task.Author);
//
//			// Добавить основной блок для задачи.
//			var taskBlock = this.AddTaskBlock(stateView);
//
//			// Получить все задания по задаче.
//			var taskAssignments = Assignments.GetAll(a => Equals(a.Task, task)).OrderBy(a => a.Created).ToList();
//
//			foreach (var iteration in iterations)
//			{
//				var date = iteration.Date;
//				var hasReworkBefore = iteration.IsRework;
//				var hasRestartBefore = iteration.IsRestart;
//
//				var nextIteration = iterations.Where(d => d.Date > date).FirstOrDefault();
//				var nextDate = Calendar.Now;
//				var hasRestartAfter = false;
//
//				var isLastRound = nextIteration == null;
//				if (!isLastRound)
//				{
//					nextDate = nextIteration.Date;
//					hasRestartAfter = nextIteration.IsRestart;
//				}
//
//				// Получить задания в рамках круга согласования.
//				var iterationAssignments = taskAssignments
//					.Where(a => a.Created >= date && a.Created < nextDate)
//					.OrderBy(a => a.Created)
//					.ToList();
//
//				if (!iterationAssignments.Any())
//					continue;
//
//				var firstAssignment = iterationAssignments.First();
//				if (hasReworkBefore)
//				{
//					var reworkComment = Functions.Module.GetAssignmentUserComment(firstAssignment);
//					Docflow.PublicFunctions.OfficialDocument
//						.AddUserActionBlock(taskBlock, firstAssignment.Performer, ApprovalTasks.Resources.StateViewDocumentSentForReApproval,
//						                    firstAssignment.Completed.Value, task, reworkComment, firstAssignment.CompletedBy);
//				}
//
//				if (hasRestartBefore)
//				{
//					var restartComment = Functions.Module.GetTaskUserComment(task, firstAssignment.Created.Value, ApprovalTasks.Resources.ApprovalText);
//
//					var restartedByUser = Sungero.CoreEntities.Users.As(Sungero.Workflow.WorkflowHistories.GetAll()
//					                                                    .Where(h => h.EntityId == firstAssignment.Task.Id)
//					                                                    .Where(h => h.HistoryDate.Between(date, nextDate))
//					                                                    .Where(h => h.Operation == Sungero.Workflow.WorkflowHistory.Operation.Restart)
//					                                                    .Select(h => h.User)
//					                                                    .FirstOrDefault());
//
//					Docflow.PublicFunctions.OfficialDocument
//						.AddUserActionBlock(taskBlock, firstAssignment.Author, ApprovalTasks.Resources.StateViewDocumentSentAfterRestart,
//						                    task.Started.Value, task, restartComment, restartedByUser);
//				}
//
//				if (!isLastRound)
//				{
//					// Добавить блок группировки для круга согласования.
//					var roundBlock = taskBlock.AddChildBlock();
//					roundBlock.AddLabel(ApprovalTasks.Resources.StateViewApprovalRound, Functions.Module.CreateStyle(true, true));
//					roundBlock.AssignIcon(ApprovalTasks.Resources.OldApprove, StateBlockIconSize.Large);
//
//					var roundStatus = hasRestartAfter ? ApprovalTasks.Resources.StateViewAborted : ApprovalTasks.Resources.StateViewNotApproved;
//					Functions.Module.AddInfoToRightContent(roundBlock, roundStatus);
//
//					this.AddAssignmentsBlocks(roundBlock, taskAssignments, iterationAssignments, StateBlockIconSize.Small);
//				}
//				else
//					this.AddAssignmentsBlocks(taskBlock, taskAssignments, iterationAssignments, StateBlockIconSize.Large);
//			}
//
//
//			if (task.Status == Workflow.Task.Status.InProcess)
//			{
//				var currentStage = task.ApprovalRule.Stages.FirstOrDefault(s => s.Number == task.StageNumber);
//				if (currentStage != null && currentStage.Stage.StageType == Docflow.ApprovalStage.StageType.CheckReturn)
//				{
//					var delay = currentStage.Stage.StartDelayDays;
//					if (delay.HasValue && delay > 0)
//					{
//						var activeReturnAssignments = ApprovalCheckReturnAssignments.GetAll(a => Equals(a.Task, task) &&
//						                                                                    a.Status == Workflow.AssignmentBase.Status.InProcess);
//						if (!activeReturnAssignments.Any())
//						{
//							var block = taskBlock.AddChildBlock();
//							block.AssignIcon(ApprovalTasks.Resources.WaitControl, StateBlockIconSize.Large);
//							block.AddLabel(ApprovalTasks.Resources.StateViewWaitForCheckReturn, Docflow.PublicFunctions.Module.CreateHeaderStyle());
//							block.AddLineBreak();
//							block.AddLabel(string.Format("{0}: {1}",
//							                             ApprovalTasks.Resources.StateViewAssignmentCreationTerms,
//							                             Functions.Module.ToShortDateShortTime(task.ControlReturnStartDate.Value.ToUserTime())),
//							               Docflow.PublicFunctions.Module.CreatePerformerDeadlineStyle());
//
//							Functions.Module.AddInfoToRightContent(block, ApprovalTasks.Resources.StateViewInProcess);
//						}
//					}
//				}
//			}

			return stateView;
		}

		
//		/// <summary>
//		/// Построение модели задачи, в которую вложен документ.
//		/// </summary>
//		/// <param name="stateView">Схема представления.</param>
//		/// <param name="task">Задача.</param>
//		/// <param name="document">Документ.</param>
//		private static void AddTaskViewXml(StateView stateView, ITask task, IElectronicDocument document)
//		{
//			// Добавить предметное отображение для простых задач.
//			if (Sungero.Workflow.SimpleTasks.Is(task))
//			{
//				AddSimpleTaskView(stateView, Sungero.Workflow.SimpleTasks.As(task));
//				return;
//			}
//
//			// Добавить предметное отображения для прикладных задач.
//			var taskStateView = Functions.Module.GetServerEntityFunctionResult(task, "GetStateView", new List<object>() { document });
//			if (taskStateView != null)
//			{
//				// Избавиться от дублирующих блоков, если таковые были.
//				List<StateBlock> blockWhiteList = new List<StateBlock>() { };
//
//				foreach (var block in ((StateView)taskStateView).Blocks)
//				{
//					if (block.Entity == null || !stateView.Blocks.Any(b => b.HasEntity(block.Entity)))
//						blockWhiteList.Add(block);
//				}
//
//				foreach (var block in blockWhiteList)
//					stateView.AddBlock(block);
//			}
//		}
		
		
		
		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public StateView GetMemoState()
		{

			var stateView = StateView.Create();
			using (var connection = SQL.CreateConnection())
			{
				var command = connection.CreateCommand();
				command.CommandText = "Select task from Sungero_WF_Attachment where AttachmentId in (" + _obj.Id + ")";
				var reader = command.ExecuteReader();
				while (reader.Read())
				{
					var task = Sungero.Workflow.Tasks.Get(Convert.ToInt32(reader.GetValue(0)));
					var block = stateView.AddBlock();
					block.AddLabel(task.Subject);
					block.AddLineBreak();
					var style = StateBlockLabelStyle.Create();
					style.Color = Colors.FromRgb(255,0,0);
					block.AddLabel(task.Started.ToString(), style);
					
					//взять задания
					using (var connection1 = SQL.CreateConnection())
					{
						var command1 = connection1.CreateCommand();
						command1.CommandText = "Select id from Sungero_WF_Assignment where Task in (" + task.Id + ")";
						var reader1 = command1.ExecuteReader();
						while (reader1.Read())
						{
							var job = Sungero.Workflow.Assignments.Get(Convert.ToInt32(reader1.GetValue(0)));
							var block2 = block.AddChildBlock();
							var style2 = StateBlockLabelStyle.Create();
							style2.Color = Colors.FromRgb(0,0,255);
							block2.AddLabel(job.Performer.Name, style2);
						}
					};
					
				}
			};
//			//var tasks = Sungero.Workflow.Tasks.GetAll().Where(x => x..Attachments.Where(y => y.Id == _obj.Id).Count() > 0);
//			foreach (Sungero.Workflow.ITask task in tasks)
//			{
//
//			}
			
			
			return stateView;
		}
		
	}
}