﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Memo;

namespace SC.BurGazSolution
{

	partial class MemoServerHandlers
	{

		public override void BeforeSigning(Sungero.Domain.BeforeSigningEventArgs e)
		{
			base.BeforeSigning(e);
		}

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
			if (string.IsNullOrEmpty(_obj.AddresseesStrGB) && _obj.AddresseesGB.Count > 0)
			{
				_obj.AddresseesStrGB = string.Join(", ", _obj.AddresseesGB.Select(s => s.Addressee));
			}

			base.BeforeSave(e);
		}
	}



}