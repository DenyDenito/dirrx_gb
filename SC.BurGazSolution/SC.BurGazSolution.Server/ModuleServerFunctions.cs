﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.BurGazSolution.Server
{
	public class ModuleFunctions
	{
		[Remote, Public]
		public static SC.IntegrationLD.ISettings return_settigs()
		{
			return SC.IntegrationLD.Settingses.GetAll().FirstOrDefault();
		}
		
		
		[Remote, Public]
		public static bool check_role(Sungero.CoreEntities.IRole  role, IRecipient user)
		{
			if (role != null)
			{
				foreach (IGroupRecipientLinks group in role.RecipientLinks)
				{
					if (group.Member != null && group.Member.Id == user.Id)
					{
						return true;
					}
				}
			}
			return false;
		}
		
		[Remote, Public]
		public static void CompliteAssignmentSC(IActionItemExecutionAssignment job)
		{
			// HACK: если нет прав, то задание будет заполнено независимо от прав доступа.
			if (!job.AccessRights.CanUpdate())
			{
				using (var session = new Sungero.Domain.Session())
				{
					AddFullRightsInSessionSC(session, job);
					job.ActiveText = "Автоматическое выполнение";
					job.Complete(SC.BurGazSolution.ActionItemExecutionAssignment.Result.Done);
				}
			}
			else
			{
				job.ActiveText = "Автоматическое выполнение";
				job.Complete(SC.BurGazSolution.ActionItemExecutionAssignment.Result.Done);
			}
			
		}
		
		/// <summary>
		/// Выдать полные права на сущность в рамках сессии.
		/// </summary>
		/// <param name="session">Сессия.</param>
		/// <param name="entity">Сущность, на которую будут выданы полные права.</param>
		public static void AddFullRightsInSessionSC(Sungero.Domain.Session session, Sungero.Domain.Shared.IEntity entity)
		{
			if (session == null || entity == null)
				return;
			
			var submitAuthorizationManager = session.GetType()
				.GetField("submitAuthorizationManager", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
				.GetValue(session);
			var authManagerType = submitAuthorizationManager.GetType();
			var authCache = (Dictionary<Sungero.Domain.Shared.IEntity, int>)authManagerType
				.GetField("authorizedOperationsCache", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
				.GetValue(submitAuthorizationManager);
			if (!authCache.ContainsKey(entity))
				authCache.Add(entity, -1);
			else
				authCache[entity] = -1;
		}
	}
}