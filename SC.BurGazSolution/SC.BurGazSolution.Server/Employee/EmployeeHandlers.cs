using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Employee;

namespace SC.BurGazSolution
{



  partial class EmployeeServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
    	if (_obj.BusinessUnitGB != _obj.Department.BusinessUnit)
      	_obj.BusinessUnitGB = _obj.Department.BusinessUnit;
      base.BeforeSave(e);
    }
  }

}