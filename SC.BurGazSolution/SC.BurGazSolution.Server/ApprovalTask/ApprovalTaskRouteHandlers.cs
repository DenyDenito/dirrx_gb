﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using SC.BurGazSolution.ApprovalTask;

namespace SC.BurGazSolution.Server
{
	partial class ApprovalTaskRouteHandlers
	{

		public override void StartBlock3(Sungero.Docflow.Server.ApprovalManagerAssignmentArguments e)
		{
			base.StartBlock3(e);
			
//			Sungero.Docflow.Constants.Module.RoleGuid..DepartmentManagersRole
//			Users.Current.IncludedIn(roleSid)
			//			Sungero.CoreEntities.Roles.Administrators.IsSingleUser
			
			
			
//			var GD = Roles.GetAll().Where(q => q.Name == "ГД").FirstOrDefault();
//			e.Block.Performers.Remove(GD);
		}

		public override void CompleteAssignment9(Sungero.Docflow.IApprovalSigningAssignment assignment, Sungero.Docflow.Server.ApprovalSigningAssignmentArguments e)
		{
			base.CompleteAssignment9(assignment, e);
			
			if (assignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.Sign || assignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.ConfirmSign)
			{
				// Отправить задачи на рассмотрение
				Functions.ApprovalTask.SendSubtaskReview(assignment);
			}
		}
	}

}