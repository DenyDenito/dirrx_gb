﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ApprovalTask;
using Sungero.RecordManagement;

namespace SC.BurGazSolution.Server
{
	partial class ApprovalTaskFunctions
	{
		
		/// <summary>
		/// Отправка задач адресатам на рассмотрение служебной записки
		/// </summary>
		/// <param name="assignment"></param>
		public static void SendSubtaskReview(Sungero.Docflow.IApprovalSigningAssignment assignment)
		{
			var errorMessages = new List<string>();
			var lastMessage = string.Empty;
			
			var officialDocument = assignment.DocumentGroup.OfficialDocuments.FirstOrDefault();
			
			if (officialDocument != null && Memos.Is(officialDocument))
			{
//				e.AddError(DocumentReviewTasks.Resources.DocumentOnExecution);
				
//				if (officialDocument.ExecutionState != Sungero.Docflow.OfficialDocument.ExecutionState.OnExecution)
//				{
				var document = Memos.As(officialDocument);
				
				foreach (var record in document.AddresseesGB)
				{
					var task = DocumentReviewTasks.Create();
					
					try
					{
//							var task = DocumentReviewTasks.CreateAsSubtask(assigment);

						task.DocumentForReviewGroup.All.Add(document);
						task.Addressee = record.Addressee;
						
						if (document.DeadlineMemoGB != null && document.DeadlineMemoGB > Calendar.Now)
						{
							task.Deadline = document.DeadlineMemoGB;
						}
						else if (document.DeadlineMemoGB < Calendar.Now)
							task.Deadline = Calendar.Today;
						else
							task.Deadline = null;
						
						// Выдать права группе регистрации документа.
						if (document.DocumentRegister != null)
						{
							var registrationGroup = document.DocumentRegister.RegistrationGroup;
							var assignmentman = assignment.Performer;
							
							if (!task.AccessRights.IsGranted(DefaultAccessRightsTypes.Read, assignmentman))
								task.AccessRights.Grant(assignmentman, DefaultAccessRightsTypes.Read);
							
							if (registrationGroup != null && !task.AccessRights.IsGranted(DefaultAccessRightsTypes.Change, registrationGroup))
								task.AccessRights.Grant(registrationGroup, DefaultAccessRightsTypes.Change);
						}

						task.Author = document.Author;
						
						task.Save();
						task.Start();
					}
					catch (Exception ex)
					{
						errorMessages.Add(record.Addressee.Name);
						lastMessage = ex.Message;
					}
					
//						if (!Equals(task.Status, Sungero.Workflow.Task.Status.InProcess))
//						{
//							errorMessages.Add(record.Addressee.Name);
//						}
				}
				
				if (!string.IsNullOrEmpty(lastMessage))
					errorMessages.Insert(0, lastMessage);
				
				if (errorMessages.Count > 0)
				{
					var simpleTask = Sungero.Workflow.SimpleTasks.CreateAsSubtask(assignment);
					simpleTask.Subject = "Ошибки при отправке задач на рассмотрение.";
					simpleTask.AssignmentType = Sungero.Workflow.SimpleTaskRouteSteps.AssignmentType.Notice;
					var step = simpleTask.RouteSteps.AddNew();
					step.Performer = assignment.Performer;
//					var simpleTask = Sungero.Workflow.SimpleTasks.CreateWithNotices("Ошибки при отправке задач на рассмотрение", assignment.Performer);
					simpleTask.ActiveText = string.Format("Не всем адресатам удалось отправить задачи на рассмотрение.\nНе отправлены: {0}.\n{1}",
					                                      string.Join(", ", errorMessages.Skip(1)),
					                                      errorMessages.First());
					simpleTask.Save();
					simpleTask.Start();
				}
//				}
//				else
//				{
//					var simpleTask = Sungero.Workflow.SimpleTasks.CreateAsSubtask(assignment);
//					simpleTask.Subject = "Задачи рассмотрения не отправлены.";
//					simpleTask.AssignmentType = Sungero.Workflow.SimpleTaskRouteSteps.AssignmentType.Notice;
//					var step = simpleTask.RouteSteps.AddNew();
//					step.Performer = assignment.Performer;
//					simpleTask.ActiveText = "Служебная записка уже находится на исполнении.";
//					simpleTask.Save();
//					simpleTask.Start();
//				}
			}
		}
		
		
		
		/// <summary>
		/// Получить локализованное имя результата согласования по подписи.
		/// </summary>
		/// <param name="signature">Подпись.</param>
		/// <param name="emptyIfNotValid">Вернуть пустую строку, если подпись не валидна.</param>
		/// <returns>Локализованный результат подписания.</returns>
		public static string GetEndorsingResultFromSignature1(Sungero.Domain.Shared.ISignature signature, bool emptyIfNotValid)
		{
			var result = string.Empty;
			
			if (emptyIfNotValid && !signature.IsValid)
				return result;
			
			switch (signature.SignatureType)
			{
				case SignatureType.Approval:
					result = ApprovalTasks.Resources.ApprovalFormApproved;
					break;
				case SignatureType.Endorsing:
					result = ApprovalTasks.Resources.ApprovalFormEndorsed;
					break;
				case SignatureType.NotEndorsing:
					result = ApprovalTasks.Resources.ApprovalFormNotEndorsed;
					break;
			}
			
			return result;
		}
		/// <summary>
		/// Получить ключ для подписи.
		/// </summary>
		/// <param name="signature">Подпись.</param>
		/// <param name="versionNumber">Номер версии.</param>
		/// <returns>Ключ для подписи.</returns>
		private static string GetSignatureKey(Sungero.Domain.Shared.ISignature signature, int versionNumber)
		{
			// Если подпись не "несогласующая" она должна схлапываться вне версий.
			if (signature.SignatureType != SignatureType.NotEndorsing)
				versionNumber = 0;
			
			if (signature.Signatory != null)
			{
				if (signature.SubstitutedUser != null && !signature.SubstitutedUser.Equals(signature.Signatory))
					return string.Format("{0} - {1}:{2}:{3}", signature.Signatory.Id, signature.SubstitutedUser.Id, signature.SignatureType == SignatureType.Approval, versionNumber);
				else
					return string.Format("{0}:{1}:{2}", signature.Signatory.Id, signature.SignatureType == SignatureType.Approval, versionNumber);
			}
			else
				return string.Format("{0}:{1}:{2}", signature.SignatoryFullName, signature.SignatureType == SignatureType.Approval, versionNumber);
		}
		/// <summary>
		/// Добавить перенос в конец строки, если она не пуста.
		/// </summary>
		/// <param name="row">Строка.</param>
		/// <returns>Результирующая строка.</returns>
		private static string AddEndOfLine(string row)
		{
			return string.IsNullOrWhiteSpace(row) ? string.Empty : row + Environment.NewLine;
		}
		
		
		/// <summary>
		/// Заполнить SQL таблицу для формирования отчета "Лист согласования".
		/// </summary>
		/// <param name="document">Документ.</param>
		/// <param name="reportSessionId">Идентификатор отчета.</param>
		[Public, Remote]
		public static void UpdateApprovalSheetReportTable1(Sungero.Docflow.IOfficialDocument document, string reportSessionId)
		{
			var filteredSignatures = new Dictionary<string, Sungero.Domain.Shared.ISignature>();
			
			var setting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(null);
			var showNotApproveSign = setting != null ? setting.ShowNotApproveSign == true : false;
			
			foreach (var version in document.Versions.OrderByDescending(v => v.Created))
			{
				var versionSignatures = Signatures.Get(version).Where(s => (showNotApproveSign || s.SignatureType != SignatureType.NotEndorsing)
				                                                      && s.IsExternal != true
				                                                      && !filteredSignatures.ContainsKey(GetSignatureKey(s, version.Number.Value)));
				var lastSignaturesInVersion = versionSignatures
					.GroupBy(v => GetSignatureKey(v, version.Number.Value))
					.Select(grouping => grouping.Where(s => s.SigningDate == grouping.Max(last => last.SigningDate)).First());
				
				foreach (Sungero.Domain.Shared.ISignature signature in lastSignaturesInVersion)
				{
					filteredSignatures.Add(GetSignatureKey(signature, version.Number.Value), signature);
					if (!signature.IsValid)
						foreach (var error in signature.ValidationErrors)
							Logger.DebugFormat("UpdateApprovalSheetReportTable: reportSessionId {0}, document {1}, version {2}, signatory {3}, substituted user {7}, signature {4}, with error {5} - {6}",
							                   reportSessionId, document.Id, version.Number,
							                   signature.Signatory != null ? signature.Signatory.Name : signature.SignatoryFullName, signature.Id, error.ErrorType, error.Message,
							                   signature.SubstitutedUser != null ? signature.SubstitutedUser.Name : string.Empty);
					
					var employeeName = string.Empty;
					if (signature.SubstitutedUser == null)
					{
						var additionalInfo = (signature.AdditionalInfo ?? string.Empty).Replace("|", " ").Trim();
						employeeName = string.Format("<b>{1}</b>{0}", signature.SignatoryFullName, AddEndOfLine(additionalInfo)).Trim();
					}
					else
					{
						var additionalInfos = (signature.AdditionalInfo ?? string.Empty).Split(new char[] { '|' }, StringSplitOptions.None);
						if (additionalInfos.Count() == 3)
						{
							// Замещающий.
							var signatoryAdditionalInfo = additionalInfos[0];
							if (!string.IsNullOrEmpty(signatoryAdditionalInfo))
								signatoryAdditionalInfo = AddEndOfLine(string.Format("<b>{0}</b>", signatoryAdditionalInfo));
							var signatoryText = AddEndOfLine(string.Format("{0}{1}", signatoryAdditionalInfo, signature.SignatoryFullName));
							
							// Замещаемый.
							var substitutedUserAdditionalInfo = additionalInfos[1];
							if (!string.IsNullOrEmpty(substitutedUserAdditionalInfo))
								substitutedUserAdditionalInfo = AddEndOfLine(string.Format("<b>{0}</b>", substitutedUserAdditionalInfo));
							var substitutedUserText = string.Format("{0}{1}", substitutedUserAdditionalInfo, signature.SubstitutedUserFullName);
							
							// Замещающий за замещаемого.
							var onBehalfOfText = AddEndOfLine(ApprovalTasks.Resources.OnBehalfOf);
							employeeName = string.Format("{0}{1}{2}", signatoryText, onBehalfOfText, substitutedUserText);
						}
						else if (additionalInfos.Count() == 2)
						{
							// Замещаюший / Система.
							var signatoryText = AddEndOfLine(signature.SignatoryFullName);
							
							// Замещаемый.
							var substitutedUserAdditionalInfo = additionalInfos[0];
							if (!string.IsNullOrEmpty(substitutedUserAdditionalInfo))
								substitutedUserAdditionalInfo = AddEndOfLine(string.Format("<b>{0}</b>", substitutedUserAdditionalInfo));
							var substitutedUserText = string.Format("{0}{1}", substitutedUserAdditionalInfo, signature.SubstitutedUserFullName);
							
							// Система за замещаемого.
							var onBehalfOfText = AddEndOfLine(ApprovalTasks.Resources.OnBehalfOf);
							employeeName = string.Format("{0}{1}{2}", signatoryText, onBehalfOfText, substitutedUserText);
						}
					}
					
					var commandText = Sungero.Docflow.Queries.ApprovalSheetReport.InsertIntoApprovalSheetReportTable;
					
					using (var command = SQL.GetCurrentConnection().CreateCommand())
					{
						var separator = ", ";
						var errorString = Sungero.Docflow.PublicFunctions.Module.GetSignatureValidationErrorsAsString(signature, separator);
						var signErrors = string.IsNullOrWhiteSpace(errorString)
							? Sungero.Docflow.Reports.Resources.ApprovalSheetReport.SignStatusActive
							: Sungero.Docflow.PublicFunctions.Module.ReplaceFirstSymbolToUpperCase(errorString.ToLower());
						var resultString = GetEndorsingResultFromSignature(signature, false);
						command.CommandText = commandText;
						SQL.AddParameter(command, "@reportSessionId",  reportSessionId, System.Data.DbType.String);
						SQL.AddParameter(command, "@employeeName",  employeeName, System.Data.DbType.String);
						SQL.AddParameter(command, "@resultString",  resultString, System.Data.DbType.String);
						SQL.AddParameter(command, "@comment",  signature.Comment, System.Data.DbType.String);
						SQL.AddParameter(command, "@signErrors",  signErrors, System.Data.DbType.String);
						SQL.AddParameter(command, "@versionNumber",  version.Number, System.Data.DbType.Int32);
						SQL.AddParameter(command, "@SignatureDate",  signature.SigningDate.FromUtcTime(), System.Data.DbType.DateTime);
						
						command.ExecuteNonQuery();
						
					}
				}
				

			}
			
			//				//получаем задачи на рассмотрение
			//var tasks = Sungero.RecordManagement.ReviewManagerAssignments.GetAll(x => x.).ToList().Where(x => x.DocumentForReviewGroup.OfficialDocuments.Contains(document));
			AccessRights.AllowRead(
				() =>
				{
					var tasks = Sungero.RecordManagement.ReviewManagerAssignments.GetAll(x => x.Status == Sungero.Workflow.AssignmentBase.Status.Completed && x.Task.AttachmentDetails.Any(a => a.AttachmentId == document.Id)).ToList();//Where(x => x.Id == 93).ToList(); //x => x.Task.AttachmentDetails.Any(a => a.AttachmentId == document.Id)).ToList();
					if (tasks.Count() > 0)
					{
						foreach (Sungero.RecordManagement.IReviewManagerAssignment t in tasks)
						{
							
							var commandText_review = Sungero.Docflow.Queries.ApprovalSheetReport.InsertIntoApprovalSheetReportTable;
							using (var command2 = SQL.CreateConnection().CreateCommand())
							{
								command2.CommandText = commandText_review;
								SQL.AddParameter(command2, "@reportSessionId",  reportSessionId, System.Data.DbType.String);
								string user = "";
								if (!t.CompletedBy.Equals(t.Performer))
								{
									user = t.CompletedBy.Name + " за " + t.Performer.Name;
								}
								else
								{
									user = t.Performer.Name;									
								}
								SQL.AddParameter(command2, "@employeeName",  user, System.Data.DbType.String);
								SQL.AddParameter(command2, "@resultString",  "Рассмотрено", System.Data.DbType.String);
								SQL.AddParameter(command2, "@comment",  t.ActiveText??"-", System.Data.DbType.String);
								SQL.AddParameter(command2, "@signErrors",  "-", System.Data.DbType.String);
								SQL.AddParameter(command2, "@versionNumber",  document.Versions.Count, System.Data.DbType.Int32);
								SQL.AddParameter(command2, "@SignatureDate", t.Completed.Value.FromUtcTime(), System.Data.DbType.DateTime);
								command2.ExecuteNonQuery();
							}
						}
					}
//
				});
		}
		
		
	}
}