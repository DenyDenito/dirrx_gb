﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Order;
using Sungero.Commons;

namespace SC.BurGazSolution.Server
{
	partial class OrderFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public StateView GetMemoSС()
		{
			var stateView = StateView.Create();

			//Информация по созданию документа
			string created_info = "Создание: " + _obj.Created.ToString() + " Автор: " + _obj.Author.Name + "\n Подписант: "  + _obj.OurSignatory + "\n Исполнитель: " + _obj.Assignee;
			var mainblock = addBlock(stateView,created_info,Sungero.Core.StateBlockIconType.User, StateBlockIconSize.Small,true,null,null,null,false,null);
			
			#region формируем задачу по согласованию
			var tasks = Sungero.Docflow.ApprovalTasks.GetAll()
				.Where(task => task.AttachmentDetails
				       .Any(a => a.AttachmentId == _obj.Id))
				.OrderBy(task => task.Created)
				.ToList();

			foreach (var task in tasks)
			{
				string task_info = "Согласование: " + task.Started.ToString() + " Конечный срок: " + task.MaxDeadline.ToString();
				var block_apptask = addBlock(stateView,task_info,ApprovalTasks.Resources.OldApprove, StateBlockIconSize.Small,true,null,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),true,task.MaxDeadline);
				
				//получаем все задания задачи
				
				var AppAss = Sungero.Workflow.Assignments.GetAll().Where(x => x.Task.Equals(task)).OrderBy(y => y.Created);
				foreach (var AppAs in AppAss)
				{
					StateBlockIconType icon;
					string label;
					var block_AppAs = addBlock(stateView,"",null,StateBlockIconSize.Small,true,block_apptask,AppAs,AppAs.Info.Properties.Status.GetLocalizedValue(AppAs.Status.Value),false,AppAs.Deadline);
					//проверяем есть ли подзадачи
					addsimpletaskblockSC(stateView,block_AppAs,AppAs.Id);
					
					var ExItms = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
						.Where(x => Equals(x.Task.ParentAssignment, AppAs)
						       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, x.Task.ParentTask) && t.IsCompoundActionItem == true && Equals(t.ParentAssignment, AppAs)))
						.OrderBy(y => y.Created);
					
					foreach (var ExItm in ExItms)
					{
						var block_ExItm = addBlock(stateView,"",null,StateBlockIconSize.Small,true,block_AppAs,ExItm,ExItm.Info.Properties.Status.GetLocalizedValue(ExItm.Status.Value),false, ExItm.Deadline);
						IconForAssignment(Sungero.Workflow.Assignments.As(block_ExItm.Entity), out icon, out label);
						block_ExItm.AssignIcon(icon,StateBlockIconSize.Small);
						block_ExItm.AddLabel(label);
						addexicutiontaskblockSC(stateView,block_ExItm,ExItm.Id);
						addsimpletaskblockSC(stateView,block_ExItm,ExItm.Id);
						
						//Задание контроль по блоку
					}
				}
				//Пробегаемся по всем блокам, записывая заголовок и иконку.
				foreach (var block in block_apptask.ChildBlocks)
				{
					StateBlockIconType icon;
					string label;
					IconForAssignment(Sungero.Workflow.Assignments.As(block.Entity), out icon, out label);
					block.AssignIcon(icon,StateBlockIconSize.Small);
					block.AddLabel(label);
				}
			}
			
			#endregion
			
			#region формируем задачи на рассмотрение
			var tasks_review = SC.BurGazSolution.DocumentReviewTasks.GetAll()
				.Where(task => task.AttachmentDetails
				       .Any(a => a.AttachmentId == _obj.Id))
				.OrderBy(task => task.Created)
				.ToList();
			
			foreach (var task in tasks_review)
			{
				
				//var block_apptask = addBlock(stateView,task_info,ApprovalTasks.Resources.OldApprove, StateBlockIconSize.Small,true,null,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),true, task.MaxDeadline);
				
				//получаем все задания задачи
				
				var AppAss = Sungero.Workflow.Assignments.GetAll().Where(x => x.Task.Equals(task)).OrderBy(y => y.Created);
				foreach (var AppAs in AppAss)
				{
					
					StateBlockIconType icon;
					string label;
					var block_AppAs = addBlock(stateView,"",null,StateBlockIconSize.Small,true,null,AppAs,AppAs.Info.Properties.Status.GetLocalizedValue(AppAs.Status.Value),false,AppAs.Deadline);
					IconForAssignment(Sungero.Workflow.Assignments.As(block_AppAs.Entity), out icon, out label);
					block_AppAs.AssignIcon(icon,StateBlockIconSize.Small);
					block_AppAs.AddLabel(label);
					
					//проверяем есть ли подзадачи
					addsimpletaskblockSC(stateView,block_AppAs,AppAs.Id);
					//Пробегаемся по всем блокам, записывая заголовок и иконку.
					foreach (var block in block_AppAs.ChildBlocks)
					{
						IconForAssignment(Sungero.Workflow.Assignments.As(block.Entity), out icon, out label);
						block.AssignIcon(icon,StateBlockIconSize.Small);
						block.AddLabel(label);
					}
					
//					x => x.MainTask.Equals(task)
//					Sungero.RecordManagement.
					var ExItms = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
						.Where(x => Equals(x.Task.ParentAssignment, AppAs)
						       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, x.Task.ParentTask) && t.IsCompoundActionItem == true && Equals(t.ParentAssignment, AppAs)))
						.OrderBy(y => y.Created);
					
					foreach (var ExItm in ExItms)
					{
						var block_ExItm = addBlock(stateView,"",null,StateBlockIconSize.Small,true,block_AppAs,ExItm,ExItm.Info.Properties.Status.GetLocalizedValue(ExItm.Status.Value),false, ExItm.Deadline);
						IconForAssignment(Sungero.Workflow.Assignments.As(block_ExItm.Entity), out icon, out label);
						block_ExItm.AssignIcon(icon,StateBlockIconSize.Small);
						block_ExItm.AddLabel(label);
						addexicutiontaskblockSC(stateView,block_ExItm,ExItm.Id);
						addsimpletaskblockSC(stateView,block_ExItm,ExItm.Id);
						
						//Задание контроль по блоку
					}
					
				}
				
				
			}
			
			#endregion
			
			return stateView;
		}
		
		private static void IconForAssignment(Sungero.Workflow.IAssignment Assignment, out Sungero.Core.StateBlockIconType icon, out string label)
		{
			bool AppAs = false;
			label = "";
			icon = StateBlockIconType.OfEntity;
			if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
			{
				icon = StateBlockIconType.Completed;
			}
			if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
			{
				icon = StateBlockIconType.Abort;
			}
			string base_label = Assignment.Completed.ToString() + " " + Assignment.Performer.Name  + " Cрок: " + Assignment.Deadline.ToString();
			
			//Если тип согласование, согласование с руководителем
			AppAs = Sungero.Docflow.ApprovalAssignments.Is(Assignment) || Sungero.Docflow.ApprovalManagerAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Согласование: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип задание
			AppAs = Sungero.Docflow.ApprovalCheckingAssignments.Is(Assignment) || Sungero.Docflow.ApprovalSimpleAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Задание: " + base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип контроль возврата
			AppAs = Sungero.Docflow.CheckReturnAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Контроль возврата: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Создание поручений
			AppAs = Sungero.Docflow.ApprovalExecutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Создание поручений: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип печать
			AppAs = Sungero.Docflow.ApprovalPrintingAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Печать: " + base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Регистрация
			AppAs = Sungero.Docflow.ApprovalRegistrationAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Регистрация документа: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассмотрение в рамках регламента
			AppAs = Sungero.Docflow.ApprovalReviewAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Рассмотрение: " + base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}

			//Если тип Доработка документа
			AppAs = Sungero.Docflow.ApprovalReworkAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Доработка документа: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип отпрвка документа
			AppAs = Sungero.Docflow.ApprovalSendingAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Отправка документа: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Подписание
			AppAs = Sungero.Docflow.ApprovalSigningAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Подписание: " + base_label;
				if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
					icon = StateBlockIconType.Signed;
				}
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Уведомление
			AppAs = Sungero.Docflow.ApprovalNotifications.Is(Assignment) || Sungero.Docflow.ApprovalSimpleNotifications.Is(Assignment);
			if (AppAs)
			{
				label = "Уведомление: " +  base_label;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			string base_label_review = Assignment.Completed.ToString() + " " + Assignment.Performer.Name  + " Cрок: " + Assignment.Deadline.ToString();
			//Если тип Подготовка проекта резолюции
			AppAs = Sungero.RecordManagement.PreparingDraftResolutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Подготовка проекта резолюции: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассморение проекта резолюции
			AppAs = Sungero.RecordManagement.ReviewDraftResolutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Рассморение проекта резолюции: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассморение проекта резолюции
			AppAs = Sungero.RecordManagement.ReviewManagerAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Рассморение: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Если тип Рассморение проекта резолюции
			AppAs = Sungero.RecordManagement.ReviewResolutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Резолюция: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Исполнение поручений
			AppAs = Sungero.RecordManagement.ActionItemExecutionAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Поручение: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
			
			//Контроль исполнения поручений
			AppAs = Sungero.RecordManagement.ActionItemSupervisorAssignments.Is(Assignment);
			if (AppAs)
			{
				label = "Приемка работ: " +  base_label_review;
				label += !string.IsNullOrWhiteSpace(Assignment.ActiveText) ? "; «" + Assignment.ActiveText + "»" : string.Empty;
				return;
			}
		}
		
		[Public]
		public static Sungero.Core.StateBlock addBlock(StateView st, string label, Sungero.Core.StateBlockIconType icon, Sungero.Core.StateBlockIconSize iconsize, bool showborder, Sungero.Core.StateBlock mainblock, Sungero.Domain.Shared.IEntity entity, string content, bool Expanded, Nullable<DateTime> deadline)
		{
			StateBlock block;
			if (mainblock != null)
			{
				block = mainblock.AddChildBlock();
			}
			else
			{
				block = st.AddBlock();
			}
			if (content != null)
			{
//				var cont1 = block.AddContent();
//				cont1.AddLabel(label);
				block.AddLabel(label);
				var cont2 = block.AddContent();
				cont2.AddLabel(content);
			}
			else
			{
				block.AddLabel(label);
			}
			
			if (deadline != null && deadline < Calendar.Now)
			{
				block.Background = Colors.FromRgb(255,200,200);
			}
			if (icon !=null && iconsize != null)
			{
				block.AssignIcon(icon,iconsize);
			}
			block.ShowBorder = showborder;
			block.Entity = entity;
			block.IsExpanded = Expanded;
			
			
			
			return block;
			
		}
		[Public]
		public static Sungero.Core.StateBlock addBlock(StateView st, string label, string icon, Sungero.Core.StateBlockIconSize iconsize, bool showborder, Sungero.Core.StateBlock mainblock, Sungero.Domain.Shared.IEntity entity, string content, bool Expanded, Nullable<DateTime> deadline)
		{
			StateBlock block;
			if (mainblock != null)
			{
				block = mainblock.AddChildBlock();
			}
			else
			{
				block = st.AddBlock();
			}
			if (content != null)
			{
//				var cont1 = block.AddContent();
//				cont1.AddLabel(label);
				block.AddLabel(label);
				var cont2 = block.AddContent();
				cont2.AddLabel(content);
			}
			else
			{
				block.AddLabel(label);
			}
			if (icon != "")
				block.AssignIcon(icon,iconsize);
			
			if (deadline != null && deadline < Calendar.Now)
			{
				block.Background = Colors.FromRgb(255,200,200);
			}
			
			block.ShowBorder = showborder;
			block.Entity = entity;
			block.IsExpanded = Expanded;
			
			
			return block;
			
		}
		
		[Public]
		public static void addsimpletaskblockSC(Sungero.Core.StateView st, Sungero.Core.StateBlock mainblock, int IDAss)
		{
			//прояеряем есть ли для задачи подзадачи
			var tasks = Sungero.Workflow.SimpleAssignments.GetAll().Where(y => Sungero.Workflow.SimpleTasks.GetAll().Any(x => x.ParentAssignment.Id == IDAss && y.Task.Equals(x)));
			foreach (var task in tasks)
			{
				string label = task.Subject.Trim() + " " + task.Created.ToString()  +  "\n" + task.Performer.Name + " Срок: " +  task.Deadline.ToString();
//				Logger.DebugFormat("!!!!!!!!!!!!!!! {0}", label);
				StateBlockIconType icon = StateBlockIconType.OfEntity;
				StateBlockIconSize iconsize = StateBlockIconSize.Small;
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
//					label += "; " + task.ActiveText;
					label += !string.IsNullOrWhiteSpace(task.ActiveText) ? "; «" + task.ActiveText + "»" : string.Empty;
					icon = StateBlockIconType.Completed;
				}
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
				{
					icon = StateBlockIconType.Abort;
				}

				var block = addBlock(st,label,icon,iconsize,true,mainblock,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),false,task.Deadline);
				addsimpletaskblockSC(st,block,task.Id);
			}
			
			//Создаёт блок по подзадаче
			
			//Вызываем рекурсивно данную функцию
		}
		
		[Public]
		public static void addexicutiontaskblockSC(Sungero.Core.StateView st, Sungero.Core.StateBlock mainblock, int IDAss)
		{
			//прояеряем есть ли для задачи подзадачи
			
//			var ExItms = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
//				.Where(x => Equals(x.Task.ParentAssignment, AppAs)
//				       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, x.Task.ParentTask) && t.IsCompoundActionItem == true && Equals(t.ParentAssignment, AppAs)))
//				.OrderBy(y => y.Created);
			
			//var tasks = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll().Where(x => x.ParentAssignment.Id == IDAss);
			
//			var tasks = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
//				.Where(y => Sungero.RecordManagement.ActionItemExecutionTasks.GetAll()
//				       .Any(x => y.Task.Equals(x) && x.ParentAssignment.Id == IDAss));
			
			var tasks = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
				.Where(y => y.Task.ParentAssignment.Id == IDAss
				       || Sungero.RecordManagement.ActionItemExecutionTasks.GetAll().Any(t => Equals(t, y.Task.ParentTask) && t.IsCompoundActionItem == true && t.ParentAssignment.Id == IDAss));
			foreach (var task in tasks)
			{
				string label = task.Subject + " " + task.Created.ToString()  +  "\n" + task.Performer.Name + " Срок: " +  task.Deadline.ToString();
				StateBlockIconType icon = StateBlockIconType.OfEntity;
				StateBlockIconSize iconsize = StateBlockIconSize.Small;
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
					label += ": " + task.ActiveText;
					icon = StateBlockIconType.Completed;
				}
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
				{
					icon = StateBlockIconType.Abort;
				}

				var block = addBlock(st,label,icon,iconsize,true,mainblock,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),false, task.Deadline);
				addsimpletaskblockSC(st,block, task.Id);
				addexicutiontaskblockSC(st,block, task.Id);
			}
			
			//Создаёт блок по подзадаче
			
			//Вызываем рекурсивно данную функцию
		}
	}
}