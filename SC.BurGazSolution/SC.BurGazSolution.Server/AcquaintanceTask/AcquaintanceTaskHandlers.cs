﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.RecordManagement;
using Sungero.Docflow;
using Sungero.Company;
using SC.BurGazSolution.AcquaintanceTask;

namespace SC.BurGazSolution
{
  partial class AcquaintanceTaskServerHandlers
  {

    public override void BeforeStart(Sungero.Workflow.Server.BeforeStartEventArgs e)
    {
      // Проверить наличие участников ознакомления.
      var employees = Sungero.RecordManagement.PublicFunctions.AcquaintanceTask.Remote.GetNonSystemActivePerformers(_obj);
      if (!employees.Any())
      {
        e.AddError(AcquaintanceTasks.Resources.PerformersCantBeEmpty);
        return;
      }

      // Запрещено отправлять ознакомления неавтоматизированным сотрудникам без замещения.
      var notAutomatedEmployees = Sungero.Company.PublicFunctions.Module.Remote.GetNotAutomatedEmployees(employees);
      if (notAutomatedEmployees.Any())
      {
        e.AddError(AcquaintanceTasks.Resources.NotAutomatedUserWithoutSubstitutionError, _obj.Info.Actions.ShowNotAutomatedEmployees);
        return;
      }
      
      // Проверить корректность срока.
      if (!Sungero.Docflow.PublicFunctions.Module.CheckDeadline(_obj.Deadline, Calendar.Now))
        e.AddError(_obj.Info.Properties.Deadline, Sungero.RecordManagement.Resources.ImpossibleSpecifyDeadlineLessThenToday);
      
      // Проверить существование тела документа.
      var document = _obj.DocumentGroup.OfficialDocuments.First();
      if (_obj.IsElectronicAcquaintance.Value && !document.HasVersions)
        e.AddError(AcquaintanceTasks.Resources.AcquaintanceTaskDocumentWithoutBodyMessage);
      
      // Выдать права на просмотр участникам и наблюдателям.
      var documents = _obj.DocumentGroup.OfficialDocuments.Concat(_obj.AddendaGroup.OfficialDocuments).ToList();
      var observers = _obj.Observers.Select(x => x.Observer).ToList();
      var performers = _obj.Performers.Select(x => x.Performer).ToList();
      Sungero.Docflow.PublicFunctions.Module.GrantReadRightsForAttachments(documents, observers);
      Sungero.Docflow.PublicFunctions.Module.GrantReadRightsForAttachments(documents, performers);
    }
  }

}