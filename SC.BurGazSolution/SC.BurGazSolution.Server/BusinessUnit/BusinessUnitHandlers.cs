﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.BusinessUnit;

namespace SC.BurGazSolution
{
	partial class BusinessUnitServerHandlers
	{

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
			//Проверяем НОРы на заполненность поля "Роль для доступа к закупкам", если не заполнено, создаём Роль и добавляем
			var role = Sungero.CoreEntities.Roles.Create();
			role.Name  = "Доступ к закупкам - " + _obj.Name;
			role.Save();
			_obj.RolePurchaseSC = role;
			base.BeforeSave(e);
		}

		public override void Created(Sungero.Domain.CreatedEventArgs e)
		{
			
			base.Created(e);
		}
	}

}