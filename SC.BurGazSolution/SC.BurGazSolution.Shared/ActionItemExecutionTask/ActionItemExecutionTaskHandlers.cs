﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemExecutionTask;

namespace SC.BurGazSolution
{
	partial class ActionItemExecutionTaskSharedHandlers
	{

		public override void IsUnderControlChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
		{
			base.IsUnderControlChanged(e);
			
			if (e.NewValue == true)
				_obj.State.Properties.AutoPerfGB.IsEnabled = false;
			else
				_obj.State.Properties.AutoPerfGB.IsEnabled = true;
		}

		public override void ImportanceChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
		{
			base.ImportanceChanged(e);
			
			if (e.NewValue == Importance.High && _obj.Supervisor == null)
			{
				_obj.IsUnderControl = true;
				var author = _obj.AssignedBy ?? Employees.Current;
				var supervisor = Sungero.Docflow.PublicFunctions.PersonalSetting.GetSupervisor(Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(author));
				_obj.Supervisor = supervisor != null ? supervisor : Employees.Current;
			}
		}

	}
}