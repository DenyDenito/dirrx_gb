﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.IncomingLetter;

namespace SC.BurGazSolution
{
	partial class IncomingLetterSharedHandlers
	{

		public override void AddresseeChanged(Sungero.Docflow.Shared.IncomingDocumentBaseAddresseeChangedEventArgs e)
		{
			base.AddresseeChanged(e);
			
			if (_obj.AddresseesGB.Count == 0)
				_obj.AddresseesGB.AddNew().Addressee = SC.BurGazSolution.Employees.As(e.NewValue);
		}

		public virtual void AddresseesGBChanged(Sungero.Domain.Shared.CollectionPropertyChangedEventArgs e)
		{
			if (_obj.AddresseesGB.Count > 0)
			{
				_obj.Addressee = _obj.AddresseesGB.First().Addressee;
			}
		}

	}
}