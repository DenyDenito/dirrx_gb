﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ApprovalStage;

namespace SC.BurGazSolution.Shared
{
	partial class ApprovalStageFunctions
	{
		
		public override List<Enumeration?> GetPossibleRoles()
		{
			var baseRoles = base.GetPossibleRoles();
			
			if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers)
			{
				baseRoles.Add(GB.RouteDev.Role.Type.Inspection);
				baseRoles.Add(GB.RouteDev.Role.Type.Lawyer);
				baseRoles.Add(GB.RouteDev.Role.Type.DeputyGD);
				baseRoles.Add(GB.RouteDev.Role.Type.GD);
				baseRoles.Add(GB.RouteDev.Role.Type.SecurityService);
			}
			
			return baseRoles;
		}
		
	}
}