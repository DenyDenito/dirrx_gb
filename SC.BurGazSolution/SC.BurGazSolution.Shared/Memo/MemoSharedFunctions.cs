﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Memo;

namespace SC.BurGazSolution.Shared
{
	partial class MemoFunctions
	{
		/// <summary>
		/// Сменить доступность реквизитов документа.
		/// </summary>
		/// <param name="isEnabled">True, если свойства должны быть доступны.</param>
		/// <param name="isRepeatRegister">Перерегистриация.</param>
		public override void ChangeDocumentPropertiesAccess(bool isEnabled, bool isRepeatRegister)
		{
			base.ChangeDocumentPropertiesAccess(isEnabled, isRepeatRegister);

			var enabledState = !(_obj.InternalApprovalState == Sungero.Docflow.OfficialDocument.InternalApprovalState.OnApproval ||
			                     _obj.InternalApprovalState == Sungero.Docflow.OfficialDocument.InternalApprovalState.PendingSign ||
			                     _obj.InternalApprovalState == Sungero.Docflow.OfficialDocument.InternalApprovalState.Signed ||
			                     _obj.InternalApprovalState == Sungero.Docflow.Memo.InternalApprovalState.PendingReview ||
			                     _obj.InternalApprovalState == Sungero.Docflow.Memo.InternalApprovalState.Reviewed);
			
			_obj.State.Properties.AddresseesGB.IsEnabled = enabledState || _obj.AddresseesGB.Count == 0;
		}
	}
}