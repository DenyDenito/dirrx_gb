﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ApprovalTask;

namespace SC.BurGazSolution.Client
{
	partial class ApprovalTaskActions
	{
    public virtual void OpenRoleGB(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var settings = SC.BurGazSolution.PublicFunctions.Module.Remote.return_settigs();
      	settings.Role2.Show();
    }

    public virtual bool CanOpenRoleGB(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

		public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var settings = SC.BurGazSolution.PublicFunctions.Module.Remote.return_settigs();
			if (_obj.Signatory != null)
			{
				//проверка, что нельзя отправлять за подписанием Rol1, если в согласующих небыло кого-то из Role2
				bool check_role1 = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role1, Recipients.As(_obj.Signatory));
				if (check_role1)
				{
					bool check_role2 = false;
					foreach (var emp in _obj.AddApprovers) //проверяем в доп. согласующих
					{
						bool check_temp = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role2, emp.Approver);
						if (check_temp)
						{
							check_role2 = true;
						}
					}
					foreach (var emp in _obj.ReqApprovers) //проверяем в обяз. согласующих
					{
						bool check_temp = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role2, emp.Approver);
						if (check_temp)
						{
							check_role2 = true;
						}
					}
					foreach (var emp in _obj.RolesGB) //проверяем в табличной части ролей
					{
						if (emp.Employee != null)
						{
							bool check_temp = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role2, Recipients.As(emp.Employee));
							if (check_temp)
							{
								check_role2 = true;
							}
						}
					}
					
					if (!check_role2)
					{
						e.AddError("Для отправки документа на подпись генеральному директору необходимо согласовать документ, как минимум, с одним сотрудником из списка '" + settings.Role2.Name +"'. Обратитесь к Администратору", _obj.Info.Actions.OpenRoleGB);
						return;
					}
				}
			}
			
			//Проверка чтобы Гд не был в согласовании
			bool check_role3 = false;
			foreach (var emp in _obj.AddApprovers) //проверяем в доп. согласующих
			{
				bool check_temp = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role1, emp.Approver);
				if (check_temp)
				{
					check_role3 = true;
				}
			}
			foreach (var emp in _obj.RolesGB) //проверяем в табличной части ролей
			{
				if (emp.Employee != null)
				{
					bool check_temp = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role1, Recipients.As(emp.Employee));
					if (check_temp)
					{
						check_role3 = true;
					}
				}
			}
			if (check_role3)
			{
				e.AddError("Генеральный директор не может быть указан в роли согласующего");
				return;
			}
			
			
			base.Start(e);
		}

		public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanStart(e);
		}

	}

}