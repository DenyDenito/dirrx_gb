﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemSupervisorAssignment;

namespace SC.BurGazSolution
{
	partial class ActionItemSupervisorAssignmentClientHandlers
	{

		public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
		{
			base.Showing(e);
			
			var personalSetting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(Employees.As(_obj.Task.Author));
			var personalSettingGB = PersonalSettings.As(personalSetting);
			
			if (_obj.Status != Status.InProcess
			    || _obj.Task.Importance != Importance.High
			    || Equals(_obj.Performer, _obj.Task.Author)
			    || !personalSettingGB.DoubleControlGB.HasValue
			    || !personalSettingGB.DoubleControlGB.Value)
			{
				e.HideAction(_obj.Info.Actions.CreateDoubleControlGB);
			}
			else
				e.HideAction(_obj.Info.Actions.Agree);
		}

	}
}