﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.ActionItemSupervisorAssignment;

namespace SC.BurGazSolution.Client
{
	partial class ActionItemSupervisorAssignmentActions
	{
		public override bool CanAgree(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			var personalSetting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(Employees.As(_obj.Task.Author));
			var personalSettingGB = PersonalSettings.As(personalSetting);

			return !(_obj.Status == Status.InProcess
			         && _obj.Task.Importance == Importance.High
			         && !Equals(_obj.Performer, _obj.Task.Author)
			         && (personalSettingGB.DoubleControlGB.HasValue ? personalSettingGB.DoubleControlGB.Value : false));
		}


		public override void Agree(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			base.Agree(e);
			//Диалог запроса
			var task = ActionItemExecutionTasks.As(_obj.Task);
			IActionItemExecutionTask current_task;
			if (task.ParentTask != null)
			{
				Logger.DebugFormat("SC_ActionItemSupervisorAssignmentActions Agree: ParentTask {0}", task.ParentTask.Id.ToString());
				current_task = ActionItemExecutionTasks.As(task.ParentTask);
				//Проверяем, что все остальные задания выполнены
				if (current_task != null && current_task.Subtasks.Count(x => x.Status == (ActionItemExecutionAssignment.Status.Completed) || x.Status == (ActionItemExecutionAssignment.Status.Aborted)) == (current_task.Subtasks.Count() - 1))
				{
					Logger.DebugFormat("SC_ActionItemSupervisorAssignmentActions Agree: ActionItemExecutionParentTask {0}", current_task.Id.ToString());
					if (current_task.ParentAssignment != null && current_task.ParentAssignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess && current_task.ParentAssignment.Performer == _obj.Performer)
					{
						Logger.DebugFormat("SC_ActionItemSupervisorAssignmentActions Agree: ParentAssignment {0}", current_task.ParentAssignment.Id.ToString());
						// Создать диалог с пользователем.
						var dialog = Dialogs.CreateTaskDialog("Что сделать с ведущим заданием","Выполнить?", MessageType.Question,"Подтверждение");
						// Добавить в диалог кнопку «Да».
						dialog.Buttons.AddYes();
						// Добавить в диалог кнопку «Нет».
						dialog.Buttons.AddNo();
						var pressButton = dialog.Buttons.AddCustom("Открыть");
						// Добавить в диалог кнопку «Отмена».
						dialog.Buttons.AddCancel();
						// Показать диалог.
						var result = dialog.Show();
						var job = ActionItemExecutionAssignments.As(current_task.ParentAssignment);
						if (result == DialogButtons.Yes)
						{
							PublicFunctions.ActionItemExecutionAssignment.Remote.CompliteAssignment(job);
						}
						if (result == pressButton)
						{
							job.Show();
						}
						if (result == DialogButtons.Cancel)
						{
							e.Cancel();
						}
					}
				}
			}
			else
			{
				if (task.ParentAssignment != null && task.ParentAssignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess && task.ParentAssignment.Performer == _obj.Performer)
				{
					Logger.DebugFormat("SC_ActionItemSupervisorAssignmentActions Agree: Parent_ParentAssignment {0}", task.ParentAssignment.Id.ToString());
					// Создать диалог с пользователем.
					var dialog = Dialogs.CreateTaskDialog("Что сделать с ведущим заданием","Выполнить?", MessageType.Question,"Подтверждение");
					// Добавить в диалог кнопку «Да».
					dialog.Buttons.AddYes();
					// Добавить в диалог кнопку «Нет».
					dialog.Buttons.AddNo();
					var pressButton = dialog.Buttons.AddCustom("Открыть");
					// Добавить в диалог кнопку «Отмена».
					dialog.Buttons.AddCancel();
					// Показать диалог.
					var result = dialog.Show();
					var job = ActionItemExecutionAssignments.As(task.ParentAssignment);
					if (result == DialogButtons.Yes)
					{
						PublicFunctions.ActionItemExecutionAssignment.Remote.CompliteAssignment(job);
					}
					if (result == pressButton)
					{
						job.Show();
					}
					if (result == DialogButtons.Cancel)
					{
						e.Cancel();
					}
				}
			}
		}

		public virtual bool CanCreateDoubleControlGB(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			var personalSetting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(Employees.As(_obj.Task.Author));
			var personalSettingGB = PersonalSettings.As(personalSetting);

			return _obj.Status == Status.InProcess
				&& _obj.Task.Importance == Importance.High
				&& !Equals(_obj.Performer, _obj.Task.Author)
				&& (personalSettingGB.DoubleControlGB.HasValue ? personalSettingGB.DoubleControlGB.Value : false);
		}
		
		public virtual void CreateDoubleControlGB(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var tasks = _obj.Subtasks
				.Where(s => s.Status == GB.RouteDev.DoubleControlTask.Status.InProcess
				       || s.Status == GB.RouteDev.DoubleControlTask.Status.Draft
				       || s.Status == GB.RouteDev.DoubleControlTask.Status.UnderReview
				       || s.Status == GB.RouteDev.DoubleControlTask.Status.Suspended)
				.Where(s => GB.RouteDev.DoubleControlTasks.Is(s));
			
			if (tasks.Count() == 0)
			{
				try
				{
					var task = Functions.ActionItemSupervisorAssignment.Remote.CreateDoubleControlSubtask(_obj);
					
//				task.Subject = _obj.Subject;
//				task.ActiveText = _obj.ActiveText;
					task.Importance = Importance.High;
					task.Recipient = _obj.Task.Author;
					task.MaxDeadline = null;
					
					task.Save();
					task.Start();
					
					e.AddInformation("Отправлена подзадача контроля.\r\nЭта задача остается в работе");
					
					e.CloseFormAfterAction = true;
				}
				catch (Exception ex)
				{
					e.AddError(string.Format("Не удалось отправить подзадачу контроля:\r\n{0}", ex.Message));
				}
			}
			else
				e.AddWarning("Уже есть отправленная подзадача контроля в работе");
		}
	}
}