﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Order;

namespace SC.BurGazSolution.Client
{
	partial class OrderActions
	{
    public override void SendForAcquaintance(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.SendForAcquaintance(e);
    }

    public override bool CanSendForAcquaintance(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForAcquaintance(e);
    }

		public virtual void OpenLotusGB(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var link = IntegrationLD.PublicFunctions.Module.Remote.GetExEntityLinkByID(_obj.Id);

			try
			{
				Hyperlinks.Open(link.ExtSystemId);
			}
			catch(Exception ex){
				Dialogs.ShowMessage("Ошибка открытия ссылки", MessageType.Warning);
				link.ShowModal();
			}
		}

		public virtual bool CanOpenLotusGB(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

	}

}