﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.IncomingLetter;

namespace SC.BurGazSolution
{
	partial class IncomingLetterClientHandlers
	{

		public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
		{
			base.Showing(e);
			
			_obj.State.Properties.InNumber.IsRequired = true;
		}
	}

}