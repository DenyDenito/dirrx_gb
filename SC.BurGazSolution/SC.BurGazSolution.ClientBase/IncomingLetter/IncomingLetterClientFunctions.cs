﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.IncomingLetter;

namespace SC.BurGazSolution.Client
{
	partial class IncomingLetterFunctions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="document"></param>
		/// <returns></returns>
		public static List<string> SendTasksReview(IIncomingLetter document)
		{
			var errorMessages = new List<string>();
			string lastMessage = null;
			
			foreach (var addressee in document.AddresseesGB)
			{
				try
				{
					var task = Functions.IncomingLetter.Remote.CreateDocumentReview(addressee.Addressee, document);
					
					task.Save();
					task.Start();
				}
				catch (Exception ex)
				{
					errorMessages.Add(addressee.Addressee.Name);
					lastMessage = ex.Message;
				}
			}
			
			if (!string.IsNullOrEmpty(lastMessage))
				errorMessages.Insert(0, lastMessage);
			
			return errorMessages;
		}
	}
}