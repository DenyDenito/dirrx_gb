﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.IncomingLetter;
using Sungero.Docflow;
using Sungero.Workflow;

namespace SC.BurGazSolution.Client
{
	partial class IncomingLetterActions
	{
		public virtual void AddressesGB(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			
		}

		public virtual bool CanAddressesGB(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

		public virtual void SendTasksReviewGB(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			// Принудительно сохранить документ, чтобы сохранились связи. Иначе они не попадут в задачу.
			_obj.Save();
			
			var errorMessages = new List<string>();
			
			if (_obj.AddresseesGB.Count == 0)
			{
				e.AddWarning("Адресаты не указаны.");
			}
			else
			{
				var createdTasks = Sungero.Docflow.PublicFunctions.Module.Remote.GetReviewTasks(_obj);
				if (createdTasks.Any())
				{
					var dialog = Dialogs.CreateTaskDialog("Вы действительно хотите повторно отправить документ на рассмотрение?", "Документ ранее был отправлен на рассмотрение.", MessageType.Question);
					var showButton = dialog.Buttons.AddCustom(OfficialDocuments.Resources.ShowButtonText);
					var continueButton = dialog.Buttons.AddCustom(OfficialDocuments.Resources.SendButtonText);
					dialog.Buttons.AddCancel();
					
					CommonLibrary.DialogButton result = showButton;
					
					while (result == showButton)
					{
						result = dialog.Show();
						
						if (result.Equals(showButton))
						{
							if (createdTasks.Count() == 1)
								createdTasks.Single().ShowModal();
							else
								createdTasks.ShowModal();
						}
						if (result.Equals(continueButton))
							errorMessages = Functions.IncomingLetter.SendTasksReview(_obj);
					}
				}
				else
				{
					errorMessages = Functions.IncomingLetter.SendTasksReview(_obj);
				}
				
				_obj.State.Controls.Control.Refresh();

				if (errorMessages.Count > 0)
				{
					e.AddWarning("Не всем адресатам удалось отправить задачи на рассмотрение.\n"
					             + "Не отправлены: " + string.Join(", ", errorMessages.Skip(1)) + ".\n"
					             + errorMessages.First());
				}
				else
				{
					e.AddInformation("Задачи успешно отправлены адресатам.");
				}
			}
		}

		public virtual bool CanSendTasksReviewGB(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanSendForReview(e);
		}

	}

}