﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.BurGazSolution.Client
{
	public class ModuleFunctions
	{

		/// <summary>
		/// Вернуть ссылку
		/// </summary>
		[Public, Hyperlink(DisplayNameResource="CompanyMedia")]
		public void GetLinkExternalEntity(string link)
		{
			Hyperlinks.Open(link);
		}

	}
}