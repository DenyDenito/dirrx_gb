﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.AcquaintanceTask;

namespace SC.BurGazSolution.Client
{
  partial class AcquaintanceTaskActions
  {
    public override void Restart(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.Restart(e);
    }

    public override bool CanRestart(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanRestart(e);
    }

    public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!e.Validate())
        return;
      
      // Проверить корректность срока.
      if (!Sungero.Docflow.PublicFunctions.Module.CheckDeadline(_obj.Deadline, Calendar.Now))
        e.AddError(Sungero.RecordManagement.Resources.ImpossibleSpecifyDeadlineLessThenToday);
      
      // Проверить существование тела документа.
      var document = _obj.DocumentGroup.OfficialDocuments.First();
      if (_obj.IsElectronicAcquaintance.Value && !document.HasVersions)
        e.AddError(AcquaintanceTasks.Resources.AcquaintanceTaskDocumentWithoutBodyMessage);

      // Выдать права на прочие документы.
      var recipients = Sungero.RecordManagement.PublicFunctions.AcquaintanceTask.Remote.GetNonSystemActivePerformers(_obj)
        .Cast<IRecipient>()
        .ToList();
      var otherDocuments = _obj.OtherGroup.All.ToList();
      if (Sungero.Docflow.PublicFunctions.Module.ShowDialogGrantAccessRights(_obj, otherDocuments, recipients) == false)
        return;
      
      base.Start(e);
    }

    public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanStart(e);
    }

  }

}