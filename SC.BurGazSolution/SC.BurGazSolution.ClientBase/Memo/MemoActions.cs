using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.BurGazSolution.Memo;

namespace SC.BurGazSolution.Client
{
	partial class MemoActions
	{
		public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var report = GB.ApprovalReport.Reports.GetApprovalSheet();
			report.Document = Sungero.Docflow.OfficialDocuments.As(_obj);
			report.Open();
			//base.ApprovalForm(e);
		}

		public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanApprovalForm(e);
		}

		public virtual void OpenRoleGB(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var settings = SC.BurGazSolution.PublicFunctions.Module.Remote.return_settigs();
			settings.Role2.Show();
		}

		public virtual bool CanOpenRoleGB(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

		public override void SendForFreeApproval(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			base.SendForFreeApproval(e);
		}

		public override bool CanSendForFreeApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanSendForFreeApproval(e);
		}

		public override void SendForApproval(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var settings = SC.BurGazSolution.PublicFunctions.Module.Remote.return_settigs();
			bool check_gd = false;
			foreach (var emp in _obj.AddresseesGB)
			{
				bool checkrole_signatory = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role1, Recipients.As(emp.Addressee));
				if (checkrole_signatory)
				{
					check_gd = true;
				}
			}
			if (check_gd)
			{
				bool checkrole_signatory = SC.BurGazSolution.PublicFunctions.Module.Remote.check_role(settings.Role2, Recipients.As(_obj.OurSignatory));
				if (!checkrole_signatory)
				{
					e.AddError("Для отправки документа на генерального директора необходима подпись документа сотрудником из списка '" + settings.Role2.Name +"'. Обратитесь к Администратору", _obj.Info.Actions.OpenRoleGB);
					return;
				}
			}

			// Принудительно сохранить документ, чтобы сохранились связи. Иначе они не попадут в задачу.
			_obj.Save();
			
			var createdReviewTasks = Sungero.Docflow.PublicFunctions.Module.Remote.GetReviewTasks(_obj);
			var createdApprovalTasks = Sungero.Docflow.PublicFunctions.Module.Remote.GetApprovalTasks(_obj);
			if (createdReviewTasks.Any())
			{
				var dialog = Dialogs.CreateTaskDialog("Документ прошел этап подписания и находится на рассмотрении.", Sungero.Docflow.OfficialDocuments.Resources.DocumentHasApprovalTasks, MessageType.Warning);
				var showButton = dialog.Buttons.AddCustom(Sungero.Docflow.OfficialDocuments.Resources.ShowButtonText);
//				var continueButton = dialog.Buttons.AddCustom(Sungero.Docflow.OfficialDocuments.Resources.SendButtonText);
				dialog.Buttons.AddCancel();
				
				CommonLibrary.DialogButton result = showButton;
				
				while (result == showButton)
				{
					result = dialog.Show();
					
					if (result.Equals(showButton))
					{
						if (createdReviewTasks.Count() == 1)
							createdReviewTasks.Single().ShowModal();
						else
							createdReviewTasks.ShowModal();
					}
//					if (result.Equals(continueButton))
//						this.CreateApprovalTask(e);
				}
			}
			else if (createdApprovalTasks.Any())
			{
				var dialog = Dialogs.CreateTaskDialog("Документ уже находится на согласовании.", Sungero.Docflow.OfficialDocuments.Resources.DocumentHasApprovalTasks, MessageType.Warning);
				var showButton = dialog.Buttons.AddCustom(Sungero.Docflow.OfficialDocuments.Resources.ShowButtonText);
//				var continueButton = dialog.Buttons.AddCustom(Sungero.Docflow.OfficialDocuments.Resources.SendButtonText);
				dialog.Buttons.AddCancel();
				
				CommonLibrary.DialogButton result = showButton;
				
				while (result == showButton)
				{
					result = dialog.Show();
					
					if (result.Equals(showButton))
					{
						if (createdApprovalTasks.Count() == 1)
							createdApprovalTasks.Single().ShowModal();
						else
							createdApprovalTasks.ShowModal();
					}
//					if (result.Equals(continueButton))
//						this.CreateApprovalTask(e);
				}
			}
			else
			{
				var task = Sungero.Docflow.PublicFunctions.Module.Remote.CreateApprovalTask(_obj);
				if (task.ApprovalRule != null)
				{
					task.Show();
					e.CloseFormAfterAction = true;
				}
				else
				{
					// Если по документу нет регламента, вывести сообщение.
					Dialogs.ShowMessage(Sungero.Docflow.OfficialDocuments.Resources.NoApprovalRuleWarning, MessageType.Warning);
					throw new OperationCanceledException();
				}
			}
		}

		public override bool CanSendForApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanSendForApproval(e);
		}

		public override void Save(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			base.Save(e);
		}

		public override bool CanSave(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanSave(e);
		}

	}



}