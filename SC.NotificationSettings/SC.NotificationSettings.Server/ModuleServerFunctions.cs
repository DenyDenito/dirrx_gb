﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.NotificationSettings.Server
{
	public class ModuleFunctions
	{

		/// <summary>
		/// Отправка документа по электронной почте
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="fileName"></param>
		/// <param name="to"></param>
		public static void SendToMail(string subject, string body, string to)
		{
			using (new Sungero.Domain.Session())
				using (var mailClient = new System.Net.Mail.SmtpClient())
					using (var mail = new System.Net.Mail.MailMessage
					       {
					       	Body = body,
					       	IsBodyHtml = true,
					       	Subject = subject.Replace('\r', ' ').Replace('\n', ' '),
					       	HeadersEncoding = System.Text.Encoding.UTF8,
					       	SubjectEncoding = System.Text.Encoding.UTF8,
					       	BodyEncoding = System.Text.Encoding.UTF8
					       })
			{
				if (!string.IsNullOrEmpty(to))
					mail.To.Add(to);
				
				Logger.DebugFormat("JSON: {{\"operation\": \"SendToMail\", \"Text\": \"Ready to send\", \"Email\": \"{0}\"}}", to);
				
//				mailClient.Host = "smtp.yandex.ru";
//				mailClient.Port = 587; // Обратите внимание что порт 587
//				mailClient.EnableSsl = true;
//				mailClient.Credentials = new System.Net.NetworkCredential("test@sys-consult.ru", "test"); // Ваши логин и пароль
				
				mailClient.Send(mail);
				
				Logger.DebugFormat("JSON: {{\"operation\": \"SendToMail\", \"Text\": \"Successful sending\", \"Email\": \"{0}\"}}", to);
			}
		}

	}
}