using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Company;
using Sungero.Workflow;

namespace SC.NotificationSettings.Server
{
	public class ModuleJobs
	{

		/// <summary>
		/// Отправка писем об истекающих исх. задачах
		/// </summary>
		public virtual void SendTasksToMail()
		{
			if (Calendar.Today.IsWorkingDay())
			{
				var nexWorkingDay = Calendar.Today.NextWorkingDay();
				
				var tasks = Sungero.Workflow.Tasks.GetAll()
					.Where(e => e.Status == Sungero.Workflow.Task.Status.InProcess || e.Status == Sungero.Workflow.Task.Status.UnderReview)
					.Where(e => Calendar.Equals(nexWorkingDay, e.MaxDeadline.Value.Date))
					.Where(e => Sungero.RecordManagement.ActionItemExecutionTasks.Is(e) || Sungero.Workflow.SimpleTasks.Is(e))
					.ToList();
				
				Logger.DebugFormat("JSON: {{\"operation\": \"SendTasksToMail\", \"AllCount\": {0}}}", tasks.Count.ToString());
				
				var taskGroups = tasks.GroupBy(e => e.Author);
				
				foreach (IGrouping<IUser, ITask> taskGroup in taskGroups)
				{
					try
					{
						Logger.DebugFormat("JSON: {{\"operation\": \"SendTasksToMail\", \"UserID\": {0}, \"Count\": {1}}}", taskGroup.Key.Id.ToString(), taskGroup.Count().ToString());
						var employee = Employees.As(taskGroup.Key);
						if (employee == null)
							continue;
						var setting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(employee);
						if (setting != null && SC.BurGazSolution.PersonalSettings.As(setting).IsNotificatedGB == true)
						{
							var stringLinks = String.Join("<hr />",
							                              taskGroup.Select(e => string.Format("<table style=\"width: 100%; border-collapse: collapse;\"><tbody><tr><td style=\"width: 13%;\">Задание:</td><td style=\"width: 87;\"><a href=\"{1}\">{0}</a></td></tr><tr><td style=\"width: 13;\">Отправлено:</td><td style=\"width: 87;\">{2}</td></tr></tbody></table>",
							                                                                  e.Subject, Hyperlinks.Get(e), e.Started.HasValue ? e.Started.Value.ToString() : string.Empty)));
							
							Functions.Module.SendToMail(string.Format("Исходящие задачи с истекающим сроком исполнения {0}", nexWorkingDay.ToString("dd.MM.yyyy")), stringLinks, employee.Email);
						}
					}
					catch {
						Logger.ErrorFormat("JSON: {{\"operation\": \"SendTasksToMail\", \"UserID\": {0}}}", taskGroup.Key.Id.ToString());
						throw;
					}
				}
			}
		}
		
	}
}