﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace GB.ApprovalReport
{
	partial class ApprovalSheetServerHandlers
	{

		public override void AfterExecute(Sungero.Reporting.Server.AfterExecuteEventArgs e)
		{
			Sungero.Docflow.PublicFunctions.Module.DeleteReportData(Constants.ApprovalSheet.SourceTableName, ApprovalSheet.ReportSessionId);
		}

		public override void BeforeExecute(Sungero.Reporting.Server.BeforeExecuteEventArgs e)
		{
			ApprovalSheet.ReportSessionId = Guid.NewGuid().ToString();
			
			SC.BurGazSolution.PublicFunctions.ApprovalTask.Remote.UpdateApprovalSheetReportTable1(ApprovalSheet.Document, ApprovalSheet.ReportSessionId);
			ApprovalSheet.HasRespEmployee = false;
			
			var document = ApprovalSheet.Document;
			if (document == null)
				return;
			
			// Наименование отчета.
			ApprovalSheet.DocumentName = Sungero.Docflow.PublicFunctions.Module.FormatDocumentNameForReport(document, false);
			
			// НОР.
			var ourOrg = document.BusinessUnit;
			if (ourOrg != null)
				ApprovalSheet.OurOrgName = ourOrg.Name;
			
			// Дата отчета.
			ApprovalSheet.CurrentDate = Calendar.Now;
			
			// Ответственный.
			var responsibleEmployee = Sungero.Company.Employees.As(document.Author);
			
			if (responsibleEmployee != null &&
			    responsibleEmployee.IsSystem != true)
			{
				var jobTitle = string.Empty;
				if (responsibleEmployee.JobTitle != null)
				{
					jobTitle = responsibleEmployee.JobTitle.DisplayValue; //Sungero.Docflow.Functions.Module.ReplaceFirstSymbolToLowerCase();
					// = string.Format(", {0}", jobTitleNormalizeValue);
				}
				
				ApprovalSheet.RespEmployee = string.Format("{0}: {1}{2}", "Ответственный",
				                                              responsibleEmployee.Person.ShortName,
				                                              jobTitle);
				ApprovalSheet.HasRespEmployee = true;
			}
			
			// Распечатал.
			if (Sungero.Company.Employees.Current == null)
			{
				ApprovalSheet.Clerk = Users.Current.Name;
			}
			else
			{
				var clerkPerson = Sungero.Company.Employees.Current.Person;
				ApprovalSheet.Clerk = clerkPerson.ShortName;
			}
			
		}

	}
}