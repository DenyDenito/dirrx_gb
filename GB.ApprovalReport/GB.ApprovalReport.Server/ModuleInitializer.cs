﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace GB.ApprovalReport.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			// Выдача прав всем пользователям.
			var allUsers = Roles.AllUsers;
			if (allUsers != null)
			{
				// Отчеты.
				InitializationLogger.Debug("Init: Grant right on reports to all users.");
				Reports.AccessRights.Grant(Reports.GetApprovalSheet().Info, allUsers, DefaultReportAccessRightsTypes.Execute);
			}
		}
	}
}
