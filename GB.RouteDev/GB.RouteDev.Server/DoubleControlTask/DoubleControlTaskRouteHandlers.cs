﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using GB.RouteDev.DoubleControlTask;

namespace GB.RouteDev.Server
{
	partial class DoubleControlTaskRouteHandlers
	{

		public virtual void CompleteAssignment5(GB.RouteDev.IDoubleControlAssignment assignment, GB.RouteDev.Server.DoubleControlAssignmentArguments e)
		{
			if (assignment.Result.HasValue && Sungero.RecordManagement.ActionItemSupervisorAssignments.Is(_obj.ParentAssignment))
			{
				var supervisorAssignment = Sungero.RecordManagement.ActionItemSupervisorAssignments.As(_obj.ParentAssignment);
				if (supervisorAssignment.Status == Sungero.RecordManagement.ActionItemSupervisorAssignment.Status.InProcess)
					supervisorAssignment.ActiveText = string.Format("{1}\r\nКомментарий от руководителя: \r\n{0}", assignment.ActiveText, supervisorAssignment.ActiveText);
				supervisorAssignment.Complete(assignment.Result.Value);
			}

		}

		public virtual void StartBlock5(GB.RouteDev.Server.DoubleControlAssignmentArguments e)
		{
			e.Block.Subject = _obj.Subject;
			e.Block.Text = _obj.ActiveText;
			e.Block.Performers.Add(_obj.Recipient);
		}

	}
}