﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace GB.RouteDev.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			CreateApprovalRole(RouteDev.Role.Type.Inspection, "Нормоконтроль");
			CreateApprovalRole(RouteDev.Role.Type.Lawyer, "Юрист");
			CreateApprovalRole(RouteDev.Role.Type.DeputyGD, "Заместитель генерального директора");
			CreateApprovalRole(RouteDev.Role.Type.GD, "Генеральный директор");
			CreateApprovalRole(RouteDev.Role.Type.SecurityService, "Служба безопасности");
		}

		public void CreateApprovalRole(Enumeration roleType, string description)
		{
			var role = Roles.GetAll().Where(r => Equals(r.Type, roleType)).FirstOrDefault();
			// Проверяет наличие роли.
			if (role == null)
			{
				role = Roles.Create();
				role.Type = roleType;
			}
			role.Description = description;
			role.Save();
		}
		
	}
}
