﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace SC.Purchase.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
    	//выдаём права на папку закупок всем пользователям
    	Purchase.SpecialFolders.ApprovalPurchases.AccessRights.Grant(Roles.AllUsers,DefaultAccessRightsTypes.Read);
    	Purchase.SpecialFolders.ApprovalPurchases.AccessRights.Save();
    	
    	//Проверяем НОРы на заполненность поля "Роль для доступа к закупкам", если не заполнено, создаём Роль и добавляем
    	var bus_units = SC.BurGazSolution.BusinessUnits.GetAll().Where(x => x.RolePurchaseSC == null);
    	foreach (var bus_unit in bus_units)
    	{
    		var role = Sungero.CoreEntities.Roles.Create();
    		role.Name  = "Доступ к закупкам - " + bus_unit.Name;
    		role.Save();
    		bus_unit.RolePurchaseSC = role;
    		bus_unit.Save();
    	}
    
    	
    	var appmtx = ApprovalMatrices.GetAll();
    	if (appmtx.Count() == 0)
    	{
    		//Создаём единственную запись
    		var refs = ApprovalMatrices.Create();
    		refs.Name = "Матрица согласования Газпром бурение";
    		refs.Role = Roles.Administrators;
    		refs.Save();
    	}    	
    	
    }
  }
}
