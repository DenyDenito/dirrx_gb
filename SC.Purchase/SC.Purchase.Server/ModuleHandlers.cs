﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.Purchase.Server
{
	partial class ApprovalPurchasesFolderHandlers
	{

		public virtual IQueryable<SC.Purchase.IApprovalPurchases> ApprovalPurchasesDataQuery(IQueryable<SC.Purchase.IApprovalPurchases> query)
		{
			var settings = ApprovalMatrices.GetAll().FirstOrDefault();
			if (settings != null)
			{
				var check_all = Users.Current.IncludedIn(settings.Role);
				if (check_all)
				{
					return query;
				}
				else
				{
					query = query.Where(o => Equals(o.Author,Users.Current) || SC.BurGazSolution.BusinessUnits.GetAll().Any(x => Equals(o.OurOrg,x) && x.RolePurchaseSC != null &&  x.RolePurchaseSC.RecipientLinks.Any(z => Equals(z.Member, Users.Current))));
				}
				
			}
			return query;
		}
	}

	partial class PurchaseHandlers
	{
	}
}