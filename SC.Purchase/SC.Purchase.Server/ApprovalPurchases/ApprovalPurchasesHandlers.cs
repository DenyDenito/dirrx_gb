﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ApprovalPurchases;

namespace SC.Purchase
{
	partial class ApprovalPurchasesCreatingFromServerHandler
	{

		public override void CreatingFrom(Sungero.Domain.CreatingFromEventArgs e)
		{
			e.Without(_info.Properties.OurOrg);
			e.Without(_info.Properties.PurchaseStatus);
			e.Map(_info.Properties.OurOrg,_source.OurOrg);
			
			
			
		}
	}

	partial class ApprovalPurchasesServerHandlers
	{

		public override void BeforeStart(Sungero.Workflow.Server.BeforeStartEventArgs e)
		{
//			if (_obj.AccessRights.CanManage())
//			{
				var settings = Purchase.ApprovalMatrices.GetAll().FirstOrDefault();
				try
				{
					_obj.AccessRights.Grant(settings.Role,DefaultAccessRightsTypes.Read);
					_obj.AccessRights.Grant(SC.BurGazSolution.BusinessUnits.As(_obj.OurOrg).RolePurchaseSC,DefaultAccessRightsTypes.Read);
					//_obj.AccessRights.Save();
				}
				catch (Exception ex)
				{
					e.AddError(ex.Message);//"У Нашей ораганизации не задана Роль доступа к закупкам");
				}
//			}
		}

		public override void Created(Sungero.Domain.CreatedEventArgs e)
		{
				
		}

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
			
			_obj.Subject = "Закупка " + _obj.Num + " " + _obj.Info.Properties.PurchaseMethod.GetLocalizedValue(_obj.PurchaseMethod) + " " + _obj.OurOrg.Name + ". " + _obj.Note;
		}

		public override void Saving(Sungero.Domain.SavingEventArgs e)
		{

		}
	}

	partial class ApprovalPurchasesPurchasePropertyFilteringServerHandler<T>
	{

		public virtual IQueryable<T> PurchaseFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
		{
			if (_obj.OurOrg == null)
			{
				return null;
			}
			else
			{
				query = query.Where(x => Equals(x.OurOrg, _obj.OurOrg) && Equals(x.StagePurchase,ApprovalPurchases.StagePurchase.Init));
				return query;
			}
		}
	}

}