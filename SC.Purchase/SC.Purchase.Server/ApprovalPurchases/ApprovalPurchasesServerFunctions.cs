﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ApprovalPurchases;

namespace SC.Purchase.Server
{
	partial class ApprovalPurchasesFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public StateView GetApprovalPurchasesState()
		{
			var stateView = StateView.Create();
			var AppAss = Sungero.Workflow.Assignments.GetAll().Where(x => x.Task.Equals(_obj)).OrderBy(y => y.Created);
			foreach (var AppAs in AppAss)
			{
				StateBlockIconType icon;
				string label;
				var block_AppAs = addBlock(stateView,"",null,StateBlockIconSize.Small,true,null,AppAs,AppAs.Info.Properties.Status.GetLocalizedValue(AppAs.Status.Value),false, AppAs.Deadline);
				IconForAssignment(Sungero.Workflow.Assignments.As(block_AppAs.Entity), out icon, out label);
				block_AppAs.AssignIcon(icon,StateBlockIconSize.Small);
				block_AppAs.AddLabel(label);
				
				//проверяем есть ли подзадачи
				addsimpletaskblockSC(stateView,block_AppAs,AppAs.Id);
			}
			return stateView;
		}
		
				[Public]
		public static Sungero.Core.StateBlock addBlock(StateView st, string label, string icon, Sungero.Core.StateBlockIconSize iconsize, bool showborder, Sungero.Core.StateBlock mainblock, Sungero.Domain.Shared.IEntity entity, string content, bool Expanded, Nullable<DateTime> deadline)
		{
			StateBlock block;
			if (mainblock != null)
			{
				block = mainblock.AddChildBlock();
			}
			else
			{
				block = st.AddBlock();
			}
			if (content != null)
			{
				var cont1 = block.AddContent();
				cont1.AddLabel(label);
				var cont2 = block.AddContent();
				cont2.AddLabel(content);
			}
			else
			{
				block.AddLabel(label);
			}
			if (icon != "")
				block.AssignIcon(icon,iconsize);
			
			if (deadline != null && deadline < Calendar.Now)
			{
				block.Background = Colors.FromRgb(255,200,200);
			}
			
			block.ShowBorder = showborder;
			block.Entity = entity;
			block.IsExpanded = Expanded;
			
			
			return block;
			
		}
		
		private static void IconForAssignment(Sungero.Workflow.IAssignment Assignment, out Sungero.Core.StateBlockIconType icon, out string label)
		{
			bool AppAs = false;
			label = "";
			icon = StateBlockIconType.OfEntity;
			if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
			{
				icon = StateBlockIconType.Completed;
			}
			if (Assignment.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
			{
				icon = StateBlockIconType.Abort;
			}
			string base_label = Assignment.Performer.Name + "	Получено: " + Assignment.Completed.ToString()  + "	Cрок: " + Assignment.Deadline.ToString() + "		Результат: " + Assignment.Info.Properties.Result.GetLocalizedValue(Assignment.Result) ;
			//Если тип согласование
			AppAs = Purchase.Approvals.Is(Assignment);
			if (AppAs)
			{
				label = "Согласование: " +  base_label;
			}
			
			//Если тип утверждение
			AppAs = Purchase.Approves.Is(Assignment);
			if (AppAs)
			{
				label = "Утверждение: " + base_label;
			}
			
			//Если тип контроль возврата
			AppAs = Purchase.ReWorks.Is(Assignment);
			if (AppAs)
			{
				label = "Доработка: " +  base_label;
			}
		}
		
		[Public]
		public static Sungero.Core.StateBlock addBlock(StateView st, string label, Sungero.Core.StateBlockIconType icon, Sungero.Core.StateBlockIconSize iconsize, bool showborder, Sungero.Core.StateBlock mainblock, Sungero.Domain.Shared.IEntity entity, string content, bool Expanded, Nullable<DateTime> deadline)
		{
			StateBlock block;
			if (mainblock != null)
			{
				block = mainblock.AddChildBlock();
			}
			else
			{
				block = st.AddBlock();
			}
			if (content != null)
			{
				var cont1 = block.AddContent();
				cont1.AddLabel(label);
				var cont2 = block.AddContent();
				cont2.AddLabel(content);
			}
			else
			{
				block.AddLabel(label);
			}
			
			if (deadline != null && deadline < Calendar.Now)
			{
				block.Background = Colors.FromRgb(255,200,200);
			}
			if (icon !=null && iconsize != null)
			{
				block.AssignIcon(icon,iconsize);
			}
			block.ShowBorder = showborder;
			block.Entity = entity;
			block.IsExpanded = Expanded;
			
			
			
			return block;
			
		}
		
		[Public]
		public static void addsimpletaskblockSC(Sungero.Core.StateView st, Sungero.Core.StateBlock mainblock, int IDAss)
		{
			//прояеряем есть ли для задачи подзадачи
			var tasks = Sungero.Workflow.SimpleAssignments.GetAll().Where(y => Sungero.Workflow.SimpleTasks.GetAll().Any(x => x.ParentAssignment.Id == IDAss && y.Task.Equals(x)));
			foreach (var task in tasks)
			{
				string label = task.Subject + task.Created.ToString()  +  "\n" + task.Performer.Name + " Срок: " +  task.Deadline.ToString();
				StateBlockIconType icon = StateBlockIconType.OfEntity;
				StateBlockIconSize iconsize = StateBlockIconSize.Small;
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Completed)
				{
					label += ": " + task.ActiveText;
					icon = StateBlockIconType.Completed;
				}
				if (task.Status == Sungero.Workflow.AssignmentBase.Status.Aborted)
				{
					icon = StateBlockIconType.Abort;
				}

				var block = addBlock(st,label,icon,iconsize,true,mainblock,task,task.Info.Properties.Status.GetLocalizedValue(task.Status.Value),false, task.Deadline);
				addsimpletaskblockSC(st,block, task.Id);
			}
			
			//Создаёт блок по подзадаче
			
			//Вызываем рекурсивно данную функцию
		}
		
		[Public, Remote]
		public virtual IApprovalMatrix return_setting()
		{
			return ApprovalMatrices.GetAll().FirstOrDefault();
		}
		[Public, Remote]
		public virtual void write_property()
		{
			//			if (_obj.Num == null)
//			{
			
			var next_num = ApprovalPurchaseses.GetAll(y => Equals(y.OurOrg,_obj.OurOrg) && Equals(y.Created.Value.Year, Calendar.Now.Year)).Max(x => x.NextNum);
			if (next_num == null)
			{
				next_num = 1;
			}
			else
			{
				next_num++;
			}
			//DateTime expirationDate = Calendar.Now.Year; // random date
			//string lastTwoDigitsOfYear = expirationDate.ToString("yy");
			var num = next_num + "/" + Calendar.Now.Year.ToString().Substring(2,2); //lastTwoDigitsOfYear;
			_obj.NextNum = next_num;
			_obj.Num = num;
			_obj.PurchaseStatus = Purchase.ApprovalPurchases.PurchaseStatus.Inicialize;
//			}
			_obj.Subject = "Закупка " + _obj.Num + " " + " " + ". " + _obj.Note;
		}
		
		[Public, Remote]
		public virtual void change_status_to_rework()
		{
			_obj.PurchaseStatus = Purchase.ApprovalPurchases.PurchaseStatus.Rework;
			_obj.Save();
		}
		
		[Public, Remote]
		public virtual void change_status_to_abort()
		{
			_obj.PurchaseStatus = Purchase.ApprovalPurchases.PurchaseStatus.Cancel;
			_obj.Save();
		}
		
		
		[Public, Remote]
		public virtual void change_status_to_Approve()
		{
			_obj.PurchaseStatus = Purchase.ApprovalPurchases.PurchaseStatus.Approve;
			_obj.Save();
		}
		
		
		[Public, Remote]
		public virtual void change_status_to_Approval()
		{
			_obj.PurchaseStatus = Purchase.ApprovalPurchases.PurchaseStatus.Approval;
			_obj.Save();
		}
	}
}