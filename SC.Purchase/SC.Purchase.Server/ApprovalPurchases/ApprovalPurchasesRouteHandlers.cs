﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using SC.Purchase.ApprovalPurchases;

namespace SC.Purchase.Server
{
	partial class ApprovalPurchasesRouteHandlers
	{

		public virtual void Script20Execute()
		{
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_Approve(ApprovalPurchaseses.As(_obj));
		}

		public virtual void EndBlock10(SC.Purchase.Server.ReWorkEndBlockEventArguments e)
		{
		}

		public virtual void StartBlock18(Sungero.Workflow.Server.NoticeArguments e)
		{
			e.Block.Performers.Add(_obj.StartedBy);
			e.Block.Subject = "Отказано в: " + _obj.Subject;
		}

		public virtual void StartBlock17(SC.Purchase.Server.ApproveArguments e)
		{
			e.Block.Performers.Add(_obj.ZGDmain);
			e.Block.Subject = "Утвердите: " + _obj.Subject;
		}

		public virtual void StartBlock16(Sungero.Workflow.Server.NoticeArguments e)
		{
			e.Block.Performers.Add(_obj.Secretary);
			e.Block.Subject = "Завершено согласование: " + _obj.Subject;
		}

		public virtual void StartBlock15(SC.Purchase.Server.ApproveArguments e)
		{
			e.Block.Performers.Add(_obj.ZGD);
			e.Block.Subject = "Утвердите: " + _obj.Subject;
		}

		public virtual void StartBlock14(SC.Purchase.Server.ApprovalArguments e)
		{
			e.Block.Performers.Add(_obj.Secretary);
			e.Block.Subject = "Проверьте: " + _obj.Subject;
		}

		public virtual void StartBlock13(SC.Purchase.Server.ApprovalArguments e)
		{
			e.Block.Performers.Add(_obj.SKZ);
			e.Block.Subject = "Согласуйте: " + _obj.Subject;
		}

		public virtual void StartBlock12(SC.Purchase.Server.ApprovalArguments e)
		{
			e.Block.Performers.Add(_obj.ProjectManager);
			e.Block.Subject = "Согласуйте: " + _obj.Subject;
		}

		public virtual void StartBlock10(SC.Purchase.Server.ReWorkArguments e)
		{
			e.Block.Performers.Add(_obj.StartedBy);
			e.Block.Subject = "Доработайте: " + _obj.Subject;
		}

		public virtual void StartBlock9(SC.Purchase.Server.ApprovalArguments e)
		{
			e.Block.Performers.Add(_obj.Secretary);
			e.Block.Subject = "Согласуйте: " + _obj.Subject;
		}

		public virtual void StartBlock8(SC.Purchase.Server.ApprovalArguments e)
		{
			e.Block.Performers.Add(_obj.DirectorPurchase);
			e.Block.Subject = "Согласуйте: " + _obj.Subject;
		}

	}
}