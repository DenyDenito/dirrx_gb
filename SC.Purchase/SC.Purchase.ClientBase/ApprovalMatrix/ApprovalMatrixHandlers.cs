﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ApprovalMatrix;

namespace SC.Purchase
{
	partial class ApprovalMatrixApprovalClientHandlers
	{

		public virtual void ApprovalOurOrgValueInput(SC.Purchase.Client.ApprovalMatrixApprovalOurOrgValueInputEventArgs e)
		{
			if (e.NewValue != null)
			{
				var count = _obj.ApprovalMatrix.Approval.Where(x => Equals(x.OurOrg, e.NewValue)).Count();
				if (count > 0)
				{
					e.AddError("Данная организация уже выбрана");
				}
			}
		}
	}

	partial class ApprovalMatrixClientHandlers
	{

	}
}