﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ReWork;

namespace SC.Purchase.Client
{
  partial class ReWorkActions
  {
    public virtual void ForReapproving(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
    	PublicFunctions.ApprovalPurchases.Remote.change_status_to_Approval(ApprovalPurchaseses.As(_obj.MainTask));
    }

    public virtual bool CanForReapproving(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return true;
    }

    public virtual void Abort(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
    	PublicFunctions.ApprovalPurchases.Remote.change_status_to_abort(ApprovalPurchaseses.As(_obj.MainTask));
    }

    public virtual bool CanAbort(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return true;
    }

  }


}