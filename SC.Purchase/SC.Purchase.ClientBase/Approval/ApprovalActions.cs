﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.Approval;

namespace SC.Purchase.Client
{
	partial class ApprovalActions
	{
		public virtual void Sign(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			
		}

		public virtual bool CanSign(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
		{
			return true;
		}

		public virtual void Rework(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			if (_obj.ActiveText == null || _obj.ActiveText.Equals(string.Empty))
			{
				e.AddError("Необходимо указать причину отправки на доработку");
			}
			
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_rework(ApprovalPurchaseses.As(_obj.MainTask));
		}

		public virtual bool CanRework(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
		{
			return true;
		}

	}


}