﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ApprovalPurchases;

namespace SC.Purchase.Client
{
	partial class ApprovalPurchasesActions
	{
		public override void Restart(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			base.Restart(e);
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_Approval(_obj);
		}

		public override bool CanRestart(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanRestart(e);
		}

		public override void Abort(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			base.Abort(e);
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_abort(_obj);
		}

		public override bool CanAbort(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanAbort(e);
		}

		public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			
			base.Start(e);
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_Approval(_obj);
			
		}

		public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanStart(e);
		}

	}

}