﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.Approve;

namespace SC.Purchase.Client
{
	partial class ApproveActions
	{
		public virtual void Abort(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_abort(ApprovalPurchaseses.As(_obj.MainTask));
		}

		public virtual bool CanAbort(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
		{
			return true;
		}

		public virtual void Approve(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			
		}

		public virtual bool CanApprove(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
		{
			return true;
		}

		public virtual void Rework(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			if (_obj.ActiveText == null)
			{
				e.AddError("Необходимо указать причину отправки на доработку");
			}
			PublicFunctions.ApprovalPurchases.Remote.change_status_to_rework(ApprovalPurchaseses.As(_obj.MainTask));
		}

		public virtual bool CanRework(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
		{
			return true;
		}

	}


}