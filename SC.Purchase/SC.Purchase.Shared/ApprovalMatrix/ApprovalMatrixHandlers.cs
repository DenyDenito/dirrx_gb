﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ApprovalMatrix;

namespace SC.Purchase
{
  partial class ApprovalMatrixApprovalSharedHandlers
  {

    public virtual void ApprovalOurOrgChanged(SC.Purchase.Shared.ApprovalMatrixApprovalOurOrgChangedEventArgs e)
    {

    }
  }

  partial class ApprovalMatrixApprovalSharedCollectionHandlers
  {

    public virtual void ApprovalAdded(Sungero.Domain.Shared.CollectionPropertyAddedEventArgs e)
    {
      
    }
  }

  partial class ApprovalMatrixSharedHandlers
  {

  }
}