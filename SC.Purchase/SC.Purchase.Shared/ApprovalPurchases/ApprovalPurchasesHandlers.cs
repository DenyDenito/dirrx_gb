﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Purchase.ApprovalPurchases;

namespace SC.Purchase
{
	partial class ApprovalPurchasesSharedHandlers
	{

    public override void AuthorChanged(Sungero.Workflow.Shared.TaskAuthorChangedEventArgs e)
    {
    	var worker = Sungero.Company.Employees.As(e.NewValue);
    	if (worker != null)
    	{
    		if (worker.Department != null && worker.Department.Manager != null)
    		{
    			_obj.DirectorPurchase = worker.Department.Manager;
    		}
    	}
    }

		public virtual void PurchaseChanged(SC.Purchase.Shared.ApprovalPurchasesPurchaseChangedEventArgs e)
		{
			if (e.NewValue != null)
			{
				_obj.PurchaseMethod = e.NewValue.PurchaseMethod;
				_obj.Sum = e.NewValue.Sum;
				
			}
		}

		public virtual void StagePurchaseChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
		{
			if (e.NewValue != null && Equals(e.NewValue,ApprovalPurchases.StagePurchase.Init))
			{
				_obj.Purchase = null;
				//Блокируем реквизит "Закупочная процедура"
				_obj.State.Properties.Purchase.IsEnabled = false;
			}
			if (Equals(e.NewValue,ApprovalPurchases.StagePurchase.Approve))
			{
				_obj.Purchase = null;
				//Разблокируем реквизит "Закупочная процедура"
				_obj.State.Properties.Purchase.IsEnabled = true;
			}
		}

		public virtual void OurOrgChanged(SC.Purchase.Shared.ApprovalPurchasesOurOrgChangedEventArgs e)
		{
			//заполняем номер тему и т п
			if (e.NewValue != null)
			{
				PublicFunctions.ApprovalPurchases.Remote.write_property(_obj);
			}
			
			//получаем запись справочника "матрица согласования" и заполняем согласующих с этого справочника
			var settings = PublicFunctions.ApprovalPurchases.Remote.return_setting(_obj);
			if (e.NewValue != null && settings != null)
			{
				var param = settings.Approval.FirstOrDefault(x => Equals(x.OurOrg,e.NewValue));
				if (param != null)
				{
					_obj.SKZ = param.SKZ;
					_obj.ZGD = param.ZGD;
					_obj.ZGDmain = param.ZGDMain;
				}
			}
		}

	}
}