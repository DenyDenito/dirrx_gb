﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace SC.IntegrationLD.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			GrantRightsOnFolder();
			
			if (Settingses.GetAll().Count() == 0)
			{
				var record = Settingses.Create();
				record.Name = "Настройки ГБ";
				record.Save();
			}
			
		}
		
		/// <summary>
		/// Функция инициализации для выдачи прав на вычисляемую папку.
		/// </summary>
		public static void GrantRightsOnFolder()
		{
			var lotusFolderAR = IntegrationLD.SpecialFolders.LotusLinks.AccessRights;
			
			var allUsers = Sungero.CoreEntities.Roles.AllUsers;
			if (lotusFolderAR.CanRead(allUsers))
			{
				lotusFolderAR.RevokeAll(allUsers);
				lotusFolderAR.Save();
			}
			
			var administrators = Sungero.CoreEntities.Roles.Administrators;
			if (!lotusFolderAR.IsGranted(DefaultAccessRightsTypes.FullAccess, administrators))
			{
				lotusFolderAR.Grant(administrators, DefaultAccessRightsTypes.FullAccess);
				lotusFolderAR.Save();
			}

			InitializationLogger.Debug("Выданы права на вычисляемую папку 'Связи c Lotus'");
		}
		
	}
}