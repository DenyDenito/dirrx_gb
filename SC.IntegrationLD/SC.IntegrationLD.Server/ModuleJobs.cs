﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.IntegrationLD.Server
{
	public class ModuleJobs
	{

		/// <summary>
		/// 
		/// </summary>
		public virtual void DeleteInactiveEmployees()
		{	
			SC.BurGazSolution.Employees.GetAll()
				.Where(e => e.Login == null && (e.GUID1CEmpGB == null || e.GUID1CEmpGB.Trim() == string.Empty))
				.ToList()
				.ForEach(e => {
				         	try
				         	{
				         		e.Status = SC.BurGazSolution.Employee.Status.Closed;
				         		e.Save();
				         		SC.BurGazSolution.Employees.Delete(e);
				         		Logger.DebugFormat("SC_DeleteInactiveEmployees: {1} ID {0} Success", e.Id.ToString(), e.Name);
				         	} catch (Exception ex) { Logger.ErrorFormat("SC_DeleteInactiveEmployees: {1} ID {0} Error {2}", e.Id.ToString(), e.Name, ex.Message); }
				         });
		}

	}
}