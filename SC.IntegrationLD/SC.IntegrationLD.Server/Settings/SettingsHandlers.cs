﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.IntegrationLD.Settings;

namespace SC.IntegrationLD
{
	partial class SettingsServerHandlers
	{

		public override void Saving(Sungero.Domain.SavingEventArgs e)
		{
			var rights = SC.BurGazSolution.Module.Shell.SpecialFolders.NotWrittenAssignmentGB.AccessRights;
			if (_obj.Role3 != null && !Equals(_obj.Role3, _obj.State.Properties.Role3.OriginalValue))
			{
				rights.Current.ToList().ForEach(s => rights.RevokeAll(s.Recipient));
				rights.Grant(_obj.Role3, DefaultAccessRightsTypes.Read);
				rights.Save();
			}
			Logger.DebugFormat("SettingsSaving: {0} {1}", "Rights granted", string.Join(", ", rights.Current.Select(s => s.Recipient.Name)));
		}
		
	}
}