﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.RecordManagement;
using Sungero.Parties;
using Sungero.Docflow;
using Sungero.Company;
using Domino;
using Sungero.Commons;
using System.IO;

namespace SC.IntegrationLD.Server
{
	public class ModuleFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public void ReSaveDocsWithAddressesStr()
		{
			var incomingLettersList = SC.BurGazSolution.IncomingLetters.GetAll().OrderByDescending(e => e.Id).ToList();
			incomingLettersList.ForEach(e => {
			                            	try {
			                            		if (e.AddresseesGB.Count > 0)
			                            		{
			                            			e.Subject = e.Subject;
			                            			e.Save();
			                            		}
			                            	} catch {}
			                            });
			
			incomingLettersList = null;
			
			var memosList = SC.BurGazSolution.Memos.GetAll().OrderByDescending(e => e.Id).ToList();
			memosList.ForEach(e => {
			                  	try {
			                  		if (e.AddresseesGB.Count > 0)
			                  		{
			                  			e.Subject = e.Subject;
			                  			e.Save();
			                  		}
			                  	} catch {}
			                  });
			
			memosList = null;
		}

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public int Function1()
		{
			var doc = IncomingLetters.Get(19193);
			doc.InNumber = null;
			doc.Save();
			
			return 0;
			
//			var task = ActionItemExecutionTasks.Get(291);
//			var assignment = ActionItemSupervisorAssignments.Get(235);
//
//			var ass = GB.RouteDev.DoubleControlAssignments.GetAll()
//				.Where(s => s.Task.ParentAssignment != null && Equals(s.Task.ParentAssignment.Task, task))
//				.OrderByDescending(s => s.Id)
//				.FirstOrDefault();
//
//			if (ass != null) // && ass.Result == GB.RouteDev.DoubleControlAssignment.Result.Agree)
//			{
//				return 999;
//			}
//			else
//				return GB.RouteDev.DoubleControlAssignments.GetAll()
//					.OrderByDescending(s => s.Id)
//					.FirstOrDefault().Task.ParentAssignment.Task.Id;
//
//			var subTasks = assignment.Subtasks.Where(s => GB.RouteDev.DoubleControlTasks.Is(s));
//			foreach (var task1 in subTasks)
//			{
//				if (task1.Status == GB.RouteDev.DoubleControlTask.Status.InProcess
//				    || task1.Status == GB.RouteDev.DoubleControlTask.Status.UnderReview
//				    || task1.Status == GB.RouteDev.DoubleControlTask.Status.Suspended)
//				{
//					task1.Abort();
//					task1.Save();
//				}
//			}
			//    	var subTasks = task.Subtasks.Where(s => GB.RouteDev.DoubleControlTasks.Is(s)).Count();
			//    	var subTasks1 = ActionItemSupervisorAssignments.GetAll().Where(s => Equals(s.Task, task)).FirstOrDefault();
			//    	var subTasks2 = GB.RouteDev.DoubleControlTasks.GetAll().Where(s => Equals(s.MainTask, task)).Count();
			//    	subTasks = subTasks1.Subtasks.Where(e => GB.RouteDev.DoubleControlTasks.Is(e)).Count();
			//    	.Where(e => GB.RouteDev.DoubleControlTasks.Is(e))
			//    		.Select(s => s.Subtasks.Where(e => GB.RouteDev.DoubleControlTasks.Is(e)).Count()).Count();
			
//			return subTasks.Count();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[Remote(IsPure = true)]
		public static List<ILogin> GetAccountsWithPostfix(string postfix)
		{
			return Logins.GetAll().Where(e => e.LoginName.Contains(postfix)).ToList();
		}
		
		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public static void DeleteAccount(ILogin account)
		{
			Logins.Delete(account);
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="org"></param>
		/// <returns></returns>
		[Remote(IsPure = true), Public]
		public static List<IPerson> GetAllPersonsWithData()
		{
			return People.GetAll().Where(person => person.TIN != null
			                             || person.INILA != null
			                             || person.City != null
			                             || person.Region != null
			                             || person.LegalAddress != null
			                             || person.PostalAddress != null
			                             || person.Phones != null
			                             || person.DateOfBirth != null
			                             || person.PSRN != null
			                             || person.NCEO != null
			                             || person.NCEA != null).ToList();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="org"></param>
		/// <returns></returns>
		[Remote(IsPure = true), Public]
		public static List<IBusinessUnit> GetAllOurOrg()
		{
			return BusinessUnits.GetAll().ToList();
		}
		
		[Remote]
		public virtual string ClearPersonData()
		{
			var persons = People.GetAll();
			
			var cntLoad = 0;
			var cntError = 0;
			
			foreach (var person in persons)
			{
				try
				{
					person.TIN = null;
					person.INILA = null;
					person.City = null;
					person.Region = null;
					person.LegalAddress = null;
					person.PostalAddress = null;
					person.Phones = null;
					person.DateOfBirth = null;
					person.PSRN = null;
					person.NCEO = null;
					person.NCEA = null;
					
					person.Save();
				}
				catch
				{
					cntError++;
				}
				
				cntLoad++;
			}
			
			return string.Format("Очистка завершена.\r\n"
			                     + "---------------------------------------------------------\r\n"
			                     + "Количество очищенных записей: {0}\r\n"
			                     + "Количество ошибок при очистке: {1}\r\n"
			                     + "---------------------------------------------------------\r\n",
			                     (cntLoad-cntError), cntError);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="org"></param>
		/// <returns></returns>
		[Remote(IsPure = true), Public]
		public static List<IPerson> GetPersonsByOurOrg(IBusinessUnit org)
		{
			return Employees.GetAll().Where(s => s.Department != null && Equals(s.Department.BusinessUnit, org)).Select(s => s.Person).ToList();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="org"></param>
		/// <returns></returns>
		[Remote(IsPure = true), Public]
		public static List<IDepartment> GetDepartmentsByOurOrg(IBusinessUnit org)
		{
			return Departments.GetAll().Where(s => Equals(s.BusinessUnit, org)).ToList();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="org"></param>
		/// <returns></returns>
		[Remote(IsPure = true), Public]
		public static List<IEmployee> GetEmployeesByOurOrg(IBusinessUnit org)
		{
			return Employees.GetAll().Where(s => s.Department != null && Equals(s.Department.BusinessUnit, org)).ToList();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="org"></param>
		/// <returns></returns>
		[Remote(IsPure = true), Public]
		public static List<IEmployee> GetEmployeesByDepartment(IDepartment dep)
		{
			return Employees.GetAll().Where(s => Equals(s.Department, dep)).ToList();
		}
		
		[Remote(IsPure = true), Public]
		public static List<IEmployee> GetEmployeesNotAccByOurOrg(IBusinessUnit org)
		{
			return Employees.GetAll().Where(p => p.Department != null && p.Login == null && Equals(p.Department.BusinessUnit, org)).ToList();
		}
		
		[Remote(IsPure = true), Public]
		public static List<IEmployee> GetEmployeesNotAccByDepartment(IDepartment dep)
		{
			return Employees.GetAll().Where(p => p.Login == null && Equals(p.Department, dep)).ToList();
		}
		
		/// <summary>
		/// 
		/// </summary>
		[Remote(PackResultEntityEagerly = true), Public]
		public static IOrder Sync_Order_Lotus(string path, IExternalEntityLink externalEntityLink, string docType, string subject, string note,
		                                      IEmployee defaultEmployee, IEmployee employeeMast, IEmployee employeeUserFrom,
		                                      List<IRecipient> listRecipients,
		                                      int countAttempts, int timeAttempts,
		                                      string logFileName, string errorMessageDefault)
		{
			IOrder order = externalEntityLink != null? GetOrdersByID((int)externalEntityLink.EntityId) : null;
			
			if (order == null)
				order = !string.IsNullOrEmpty(path) ? Orders.CreateFrom(path) : Orders.Create();
			else
			{
				if (!string.IsNullOrEmpty(path))
				{
					if (order.HasVersions)
					{
						try
						{
							try
							{
								Locks.Unlock(order);
							}
							catch
							{
							}
							
							try
							{
								Locks.Unlock(order.Versions.First());
							}
							catch
							{
							}

							try
							{
								Locks.Unlock(order.Versions.First().Body);
							}
							catch
							{
							}
//							throw new System.MemberAccessException("Ошибка сохранения документа");
							order.Versions.First().Import(path);
						}
						catch
						{
							var lockInfo = Locks.GetLockInfo(order.Versions.First().Body);
							using (var file = File.AppendText(logFileName + "(server)" + ".txt"))
							{
								file.WriteLine(string.Format("{0}[ID RX {1}] - {2}: {3}", errorMessageDefault, order.Id, lockInfo.OwnerName, lockInfo.LockedMessage));
							}
						}
					}
					else
					{
						try
						{
							order.CreateVersionFrom(path);
						}
						catch
						{
							using (var file = File.AppendText(logFileName + "(server)" + ".txt"))
							{
								file.WriteLine(string.Format("Ошибка при создании новой версии {0} ({1})[ID RX {2}]", subject, path, order.Id));
							}
						}
					}
				}
			}
			
			order.DocumentKind = GetDocumentKind(docType);
			
			order.Subject = subject.Substring(0, subject.Length > 250 ? 250 : subject.Length);
			
			order.Note = note.Substring(0, note.Length > 1000 ? 1000 : note.Length);
			
			order.Author = employeeMast != null ? employeeMast : defaultEmployee;
			order.PreparedBy = employeeMast != null ? employeeMast : defaultEmployee;
			order.OurSignatory = employeeUserFrom;
			order.Department = (employeeMast != null) ? employeeMast.Department : defaultEmployee.Department;
			
			try
			{
				foreach (var recipient in listRecipients)
				{
					if (!order.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, recipient))
					{
						order.AccessRights.Grant(recipient, DefaultAccessRightsTypes.FullAccess);
					}
				}
			}
			catch
			{
				using (var file = File.AppendText(logFileName + "(server)" + ".txt"))
				{
					file.WriteLine(string.Format("Ошибка при выдаче прав на докумен {0} ({1})[ID RX {2}]", subject, path, order.Id));
				}
			}
			
			var exceptionCount = 0;
			var saved = false;
			
			while (!saved && exceptionCount < countAttempts)
			{
				try
				{
					if (exceptionCount > 0)
						System.Threading.Thread.Sleep(timeAttempts);
//					await System.Threading.Tasks.Task.Delay(1000);
					
					order.Save();
					
					exceptionCount++;
					
					using (var file = File.AppendText(logFileName + "(server)" + ".txt"))
					{
						file.WriteLine(string.Format("[ID RX {3}] Попытка сохранения № {0} ({1})[{2}]", exceptionCount.ToString(), subject, path, order.Id));
					}
					
					exceptionCount = countAttempts;
					saved = true;
				}
				catch
				{
					exceptionCount++;
					
					using (var file = File.AppendText(logFileName + "(server)" + ".txt"))
					{
						file.WriteLine(string.Format("Попытка сохранения № {0} ({1})[{2}]", exceptionCount.ToString(), subject, path));
					}
				}
			}
			
			if(!saved)
			{
				throw new System.MemberAccessException("Ошибка сохранения документа");
			}
			
			return order;
		}
		
		/// <summary>
		/// Получить группу регистрации по имени
		/// </summary>
		[Remote(IsPure = true), Public]
		public static IRegistrationGroup GetRegGroupByName(string name)
		{
			return RegistrationGroups.GetAll().FirstOrDefault(s => string.Equals(s.Name, name));
		}
		
		[Remote(PackResultEntityEagerly = true)]
		public static IOrder CreateOrder()
		{
			return Orders.Create();
		}
		
		[Remote(PackResultEntityEagerly = true)]
		public static IIncomingLetter CreateLetter()
		{
			return IncomingLetters.Create();
		}
		
		/// <summary>
		/// 
		/// </summary>
		[Remote(PackResultEntityEagerly = true), Public]
		public static IExternalEntityLink CreateExEntityLink(string entityType, int? entityId, string extSystemId, string extEntityId)
		{
			var migrationLink = GetExEntityLinkByExID(extEntityId);
			
			if (migrationLink == null)
			{
				migrationLink = ExternalEntityLinks.Create();
			}
			
			migrationLink.EntityType = entityType;
			migrationLink.EntityId = entityId;
			migrationLink.ExtEntityType = "CompanyMedia";
			migrationLink.ExtSystemId = extSystemId;
			migrationLink.ExtEntityId = extEntityId;
			migrationLink.SyncDate = Calendar.Now;
			migrationLink.Save();
			
			return migrationLink;
		}
		
		/// <summary>
		/// Вернуть сущность связи с внешней системой.
		/// </summary>
		[Remote(IsPure = true), Public]
		public static IExternalEntityLink GetExEntityLinkByID(int id)
		{
			return ExternalEntityLinks.GetAll().FirstOrDefault(e => e.EntityId == id);
		}
		
		/// <summary>
		/// Вернуть сущность связи с внешней системой.
		/// </summary>
		[Remote(IsPure = true), Public]
		public static IExternalEntityLink GetExEntityLinkByExID(string id)
		{
			return ExternalEntityLinks.GetAll().FirstOrDefault(e => e.ExtEntityId.Equals(id));
		}
		
		/// <summary>
		/// Получить пользователя.
		/// </summary>
		[Remote(IsPure = true)]
		public static IUser GetUser(int id)
		{
			return Users.Get(id);
		}
		
		/// <summary>
		/// Получить корреспондента.
		/// </summary>
		[Remote(IsPure = true)]
		public static ICounterparty GetCounterparty(int id)
		{
			return Counterparties.Get(id);
		}
		
		/// <summary>
		/// Получить сотрудника.
		/// </summary>
		[Remote(IsPure = true)]
		public static IEmployee GetEmployee(int id)
		{
			return Employees.Get(id);
		}
		
		/// <summary>
		/// Получить пользователя.
		/// </summary>
		[Remote(IsPure = true)]
		public static IUser GetUser()
		{
			return Users.Get(88);
		}
		
		/// <summary>
		/// Получить корреспондента.
		/// </summary>
		[Remote(IsPure = true)]
		public static ICounterparty GetCounterparty()
		{
			return Counterparties.Get(1912);
		}
		
		/// <summary>
		/// Получить сотрудника.
		/// </summary>
		[Remote(IsPure = true)]
		public static IEmployee GetEmployee()
		{
			return Employees.Get(88);
		}
		
		/// <summary>
		/// Получить всех сотрудников.
		/// </summary>
		[Remote(IsPure = true)]
		public static List<IEmployee> GetAllEmployees()
		{
			return Employees.GetAll().ToList();
		}
		
		/// <summary>
		/// Получить сотрудника.
		/// </summary>
		[Remote(IsPure = true)]
		public static IDocumentKind GetDocumentKind(string nameKind)
		{
			return DocumentKinds.GetAll().FirstOrDefault(kind => kind.Name.Equals(nameKind));
		}
		
		/// <summary>
		/// Получить сотрудника.
		/// </summary>
		[Remote(IsPure = true)]
		public static IEmployee GetEmployeeByEmail(string email)
		{
			return Employees.GetAll().FirstOrDefault(e => e.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
		}
		
		/// <summary>
		/// Получить подразделение.
		/// </summary>
		[Remote(IsPure = true)]
		public static IDepartment GetDepartment()
		{
			return Sungero.Company.Departments.Get(87);
		}
		
		/// <summary>
		/// Получить список загруженных входящих писем.
		/// </summary>
		[Remote(IsPure = true)]
		public static List<IIncomingLetter> GetIncomingLettersByStrTemplate(string findTemplate)
		{
			return IncomingLetters.GetAll().Where(doc => doc.Note.Contains(findTemplate)).OrderBy(doc => doc.Id).ToList();
		}
		
		/// <summary>
		/// Получить список загруженных приказов или распоряжений.
		/// </summary>
		[Remote(IsPure = true)]
		public static List<IOrder> GetOrdersByStrTemplate(string findTemplate)
		{
			return Orders.GetAll().Where(doc => doc.Note.Contains(findTemplate)).OrderBy(doc => doc.Id).ToList();
		}
		
		/// <summary>
		/// Получить список загруженных входящих писем по ID.
		/// </summary>
		[Remote(IsPure = true)]
		public static IIncomingLetter GetIncomingLettersByID(int id)
		{
			return IncomingLetters.GetAll().FirstOrDefault(doc => doc.Id.Equals(id));
		}
		
		/// <summary>
		/// Получить список загруженных приказов или распоряжений по ID.
		/// </summary>
		[Remote(IsPure = true)]
		public static IOrder GetOrdersByID(int id)
		{
			return Orders.GetAll().FirstOrDefault(doc => doc.Id.Equals(id));
		}
		
		/// <summary>
		/// Подключиться к БД.
		/// </summary>
		[Remote]
		public ISimpleDocument ConnectToBD()
		{
			int i = 0;
			try
			{
				NotesSessionClass notesSession = new NotesSessionClass();
				notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
				NotesDatabase notesDatabase = notesSession.GetDatabase("Domino-02/burgaz", "CM\\2019\\int19dat.nsf");
				if (!notesDatabase.IsOpen)
					notesDatabase.Open();
				//NotesView notesView = notesDatabase.GetView("JournalByDate");
				//NotesDocument firstDocument = notesView.GetFirstDocument();
				NotesDocument NDoc = notesDatabase.GetDocumentByUNID("9E3972B8FA41517C4325838A00236E8E");
				string[] columnNames = {"Subject", "ReqType", "UserFrom", "to", "rprist", "RNumber", "rfin", "body", "$FILE"};
				string result = "УДАЧНО!\r\n";
				
				foreach (string columnName in columnNames)
				{
					string curValue = "|";
					object[] curValues = (object[])NDoc.GetItemValue(columnName);
					foreach (string valueString in curValues)
					{
						curValue += valueString.ToString() + "|";
					}
					if (!string.IsNullOrEmpty(curValue))
						result += string.Format("\r\n{0}.{1}: \"{2}\"", i, columnName, curValue);
					else
						result += string.Format("\r\n{0}.{1}: null", i, columnName);
					i++;
				}
				
				if (NDoc.HasEmbedded && NDoc.HasItem("$File"))
				{
					object[] AllDocItems = (object[])NDoc.Items;
					string pAttachment = "";
					string path = "";
					
					foreach (object CurItem in AllDocItems)
					{
						NotesItem nItem = (NotesItem)CurItem;
						if (IT_TYPE.ATTACHMENT == nItem.type)
						{
							pAttachment = ((object[])nItem.Values)[0].ToString();
							path = @"C:\DRX\TEST\" + pAttachment;
							NDoc.GetAttachment(pAttachment).ExtractFile(path);
							break;
						}
					}
					
					if (!string.IsNullOrEmpty(path))
					{
						ISimpleDocument SDoc = SimpleDocuments.CreateFrom(path);
						SDoc.Name = pAttachment;
						SDoc.Note = result;
						SDoc.Save();
						return SDoc;
					}
					//return result + "\r\n\r\nPath = " + path;
				}
				return null;
			}
			catch (Exception ex)
			{
				var message = "ОШИБКА!\r\n" + i + "\r\n" + ex.Message;
				return null;
				//return message;
			}
		}
		
	}
}