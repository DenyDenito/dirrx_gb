﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace SC.IntegrationLD.Structures.Module
{

	/// <summary>
	/// Настройки миграции документов
	/// </summary>
	partial class DocumentTypeSetting
	{
		public string Name { get; set; }
		public string NameDBLotus { get; set; }
		public string DocumentsView { get; set; }
	}
	
}