﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Docflow;
using Sungero.RecordManagement;
using Sungero.Parties;
using Domino;
using ProgressBarLibrary;
using System.IO;
using Sungero.Company;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;

namespace SC.IntegrationLD.Client
{
	
	public class ModuleFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		public virtual void Function2()
		{
			//    	Dialogs.ShowMessage(Functions.Module.Remote.Function1().ToString());
//			Functions.Module.Remote.Function1();
//			Functions.Module.Remote.ReSaveDocsWithAddressesStr();
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Function1()
		{
		}

		public virtual void ResaveEntities()
		{
			var employees= Functions.Module.Remote.GetAllEmployees();
			
			var progressBar = new ProgressBarDRX(employees.Count); //объект прогресс бар
			progressBar.Show();
			
			var cntLoad = 0;
			var cntError = 0;
			
			progressBar.Text(string.Format("Изменение статуса сотрудников: {0}/{1}...Ошибок: {2}", cntLoad, employees.Count, cntError));
			
			foreach (var employee in employees)
			{
				try
				{
					employee.Status = Sungero.CoreEntities.DatabookEntry.Status.Closed;
					employee.Status = Sungero.CoreEntities.DatabookEntry.Status.Active;
					employee.Save();
				}
				catch
				{
					cntError++;
				}
				
				cntLoad++;
				
				progressBar.Text(string.Format("Изменение статуса сотрудников: {0}/{1}...Ошибок: {2}", cntLoad, employees.Count, cntError));
				progressBar.Next();
			}
			
			progressBar.Hide();
			progressBar = null;
			
			string resultMessage = string.Format("Изменение завершено.\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество измененных записей: {0}\r\n"
			                                     + "Количество ошибок при загрузке: {1}\r\n"
			                                     + "---------------------------------------------------------\r\n",
			                                     (cntLoad-cntError), cntError);
			
			Dialogs.ShowMessage(resultMessage, MessageType.Information);
		}
		
		/// <summary>
		/// 
		/// </summary>
		public virtual void DeleteAccountsEmployees()
		{
			var dialog = Dialogs.CreateInputDialog("Укажите часть имени учетной записи");
			var dialogPostfix = dialog.AddString("Префикс/постфикс", true, "@main.burgaz.ru");
			
			// Отобразить диалог и проверить, что нажата кнопка «ОК».
			if (dialog.Show() == DialogButtons.Ok)
			{
				var accounts = Functions.Module.Remote.GetAccountsWithPostfix(dialogPostfix.Value);
				
				var progressBar = new ProgressBarDRX(accounts.Count); //объект прогресс бар
				progressBar.Show();
				
				var cntLoad = 0;
				var cntError = 0;
				
				foreach (var account in accounts)
				{
					progressBar.Text(string.Format("Очистка персональных данных: {0}/{1}...Ошибок: {2}", cntLoad, accounts.Count, cntError));
					
					try
					{
						Functions.Module.Remote.DeleteAccount(account);
					}
					catch
					{
						cntError++;
					}
					
					cntLoad++;

					progressBar.Next();
				}
				
				progressBar.Hide();
				progressBar = null;
				
				string resultMessage = string.Format("Очистка завершена.\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество очищенных записей: {0}\r\n"
				                                     + "Количество ошибок при очистке: {1}\r\n"
				                                     + "---------------------------------------------------------\r\n",
				                                     (cntLoad-cntError), cntError);
				
				Dialogs.ShowMessage(resultMessage, MessageType.Information);
			}
		}
		
		public virtual void CloseEmployees()
		{
			var dialog = Dialogs.CreateInputDialog("Параметры закрытия");
			var dialogOurOrg = dialog.AddSelect("Наша организация", true, BusinessUnits.Null);
			var dialogDepartment = dialog.AddSelect("Подразделение", false, Departments.Null);
			var dialogAction = dialog.AddSelect("Действие", true, 0).From("Закрыть", "Открыть");
			var dialogAccount = dialog.AddBoolean("Только без учетных записей", true);
			dialogOurOrg.SetOnValueChanged(func =>
			                               {
			                               	if (!Equals(dialogOurOrg.Value, null))
			                               		dialogDepartment.From(Functions.Module.Remote.GetDepartmentsByOurOrg(dialogOurOrg.Value));
			                               });
			
			// Отобразить диалог и проверить, что нажата кнопка «ОК».
			if (dialog.Show() == DialogButtons.Ok)
			{
				var employees = new List<IEmployee>();
				var isNotAccount = true;
				isNotAccount = (bool) dialogAccount.Value;
				if (Equals(dialogDepartment.Value, null))
				{
					//		employees = Functions.Module.Remote.GetEmployeesByOurOrg(dialogOurOrg.Value);
					if (isNotAccount)
						employees = Functions.Module.Remote.GetEmployeesNotAccByOurOrg(dialogOurOrg.Value);
					else
						employees = Functions.Module.Remote.GetEmployeesByOurOrg(dialogOurOrg.Value);
				}
				else
				{
					//		employees = Functions.Module.Remote.GetEmployeesByDepartment(dialogDepartment.Value);
					if (isNotAccount)
						employees= Functions.Module.Remote.GetEmployeesNotAccByDepartment(dialogDepartment.Value);
					else
						employees= Functions.Module.Remote.GetEmployeesByDepartment(dialogDepartment.Value);
				}
				
				var progressBar = new ProgressBarDRX(employees.Count); //объект прогресс бар
				progressBar.Show();
				
				var cntLoad = 0;
				var cntError = 0;
				
				progressBar.Text(string.Format("Изменение статуса сотрудников: {0}/{1}...Ошибок: {2}", cntLoad, employees.Count, cntError));
				
				foreach (var employee in employees)
				{
					try
					{
						if (Equals(dialogAction.Value, "Закрыть"))
						{
							if (!Equals(employee.Status, Sungero.CoreEntities.DatabookEntry.Status.Closed))
							{
								employee.Status = Sungero.CoreEntities.DatabookEntry.Status.Closed;
								employee.Save();
							}
						}
						else
						{
							if (!Equals(employee.Status, Sungero.CoreEntities.DatabookEntry.Status.Active))
							{
								employee.Status = Sungero.CoreEntities.DatabookEntry.Status.Active;
								employee.Save();
							}
						}
					}
					catch
					{
						cntError++;
					}
					
					cntLoad++;
					
					progressBar.Text(string.Format("Изменение статуса сотрудников: {0}/{1}...Ошибок: {2}", cntLoad, employees.Count, cntError));
					progressBar.Next();
				}
				
				progressBar.Hide();
				progressBar = null;
				
				string resultMessage = string.Format("Изменение завершено.\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество измененных записей: {0}\r\n"
				                                     + "Количество ошибок при загрузке: {1}\r\n"
				                                     + "---------------------------------------------------------\r\n",
				                                     (cntLoad-cntError), cntError);
				
				Dialogs.ShowMessage(resultMessage, MessageType.Information);
			}
		}

		public virtual void ClearPersonData()
		{
			var ourOrgs = Functions.Module.Remote.GetAllOurOrg();
			
			var progressBar = new ProgressBarDRX(ourOrgs.Count); //объект прогресс бар
			progressBar.Show();
			
			var cntLoad = 0;
			var cntError = 0;
			
			foreach (var ourOrg in ourOrgs)
			{
				progressBar.Text(string.Format("Очистка персональных данных {3}: {0}/{1}...Ошибок: {2}", cntLoad, ourOrgs.Count, cntError, ourOrg.Name));
				
				var persons = Functions.Module.Remote.GetPersonsByOurOrg(ourOrg);
				
				foreach (var person in persons)
				{
					try
					{
						person.TIN = null;
						person.INILA = null;
						person.City = null;
						person.Region = null;
						person.LegalAddress = null;
						person.PostalAddress = null;
						person.Phones = null;
						person.DateOfBirth = null;
						person.PSRN = null;
						person.NCEO = null;
						person.NCEA = null;
						
						person.Save();
					}
					catch
					{
						cntError++;
					}
					
					cntLoad++;
				}
				
				progressBar.Next();
			}
			
			progressBar.Hide();
			progressBar = null;
			
			string resultMessage = string.Format("Очистка завершена.\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество очищенных записей: {0}\r\n"
			                                     + "Количество ошибок при очистке: {1}\r\n"
			                                     + "---------------------------------------------------------\r\n",
			                                     (cntLoad-cntError), cntError);
			
//			var message = Functions.Module.Remote.ClearPersonData();
			
			Dialogs.ShowMessage(resultMessage, MessageType.Information);
		}

		public virtual void ClearPersonWithData()
		{
			var persons = Functions.Module.Remote.GetAllPersonsWithData();
			
			var progressBar = new ProgressBarDRX(persons.Count); //объект прогресс бар
			progressBar.Show();
			
			var cntLoad = 0;
			var cntError = 0;
			
			foreach (var person in persons)
			{
				progressBar.Text(string.Format("Очистка персональных данных: {0}/{1}...Ошибок: {2}", cntLoad, persons.Count, cntError));
				
				try
				{
					person.TIN = null;
					person.INILA = null;
					person.City = null;
					person.Region = null;
					person.LegalAddress = null;
					person.PostalAddress = null;
					person.Phones = null;
					person.DateOfBirth = null;
					person.PSRN = null;
					person.NCEO = null;
					person.NCEA = null;
					
					person.Save();
				}
				catch
				{
					cntError++;
				}
				
				cntLoad++;

				progressBar.Next();
			}
			
			progressBar.Hide();
			progressBar = null;
			
			string resultMessage = string.Format("Очистка завершена.\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество очищенных записей: {0}\r\n"
			                                     + "Количество ошибок при очистке: {1}\r\n"
			                                     + "---------------------------------------------------------\r\n",
			                                     (cntLoad-cntError), cntError);
			
//			var message = Functions.Module.Remote.ClearPersonData();
			
			Dialogs.ShowMessage(resultMessage, MessageType.Information);
		}

		/// <summary>
		/// 
		/// </summary>
		public static void CreateExcel()
		{
//			ExcelPreparation();
		}
		
		/// <summary>
		/// Диалог загрузки документов.
		/// </summary>
		public virtual void LoadDocumentsDialogTest()
		{
			List<Structures.Module.DocumentTypeSetting> settingsList = new List<Structures.Module.DocumentTypeSetting>()
			{
				new Structures.Module.DocumentTypeSetting() { Name = "Входящее письмо" , NameDBLotus = "int", DocumentsView = "JournalByDate"},
				new Structures.Module.DocumentTypeSetting() { Name = "Приказ" , NameDBLotus = "mis", DocumentsView = "JournalByType"},
				new Structures.Module.DocumentTypeSetting() { Name = "Распоряжение" , NameDBLotus = "mis", DocumentsView = "JournalByType"}
			};

			var dialog = Dialogs.CreateInputDialog("Параметры загрузки");
			var dialogCountDoc = dialog.AddInteger("Кол-во документов", true, 1);
			var dialogYear = dialog.AddString("Год", true, "2019");
			var dialogType = dialog.AddSelect("Вид документа", true).From("Входящее письмо", "Приказ", "Распоряжение");
			var dialogDefaultEmployee = dialog.AddSelect("Сотрудник по умолчанию", true, Employees.Null);
			
			// Отобразить диалог и проверить, что нажата кнопка «ОК».
			if (dialog.Show() == DialogButtons.Ok)
			{
				string year = dialogYear.Value;
				int maxCountDocument = (int) dialogCountDoc.Value;

//				Structures.Module.DocumentTypeSettings settings = Structures.Module.DocumentTypeSettings.Create();

				string tempFolder = @"C:\DRX\TEST\";
				string datetimeNow = Calendar.Now.ToString("yyyy.MM.dd HH.mm.ss");
				string pServer = "MSC-04/burgaz";
				string ndbOrgStructPath = "CM\\cmso.nsf";
				string ndbDominoPath = "names.nsf";
				string docType = dialogType.Value;
				IEmployee defaultEmployee = dialogDefaultEmployee.Value;
				string ndbName = settingsList.FirstOrDefault(s => s.Name == docType).NameDBLotus;
				string documentsView = settingsList.FirstOrDefault(s => s.Name == docType).DocumentsView;
				
				string nameDocFormat = string.Format("LOTUS {0} {1} №",
				                                     datetimeNow,
				                                     docType);
				
				string ndbPath = string.Format("CM\\{0}\\{1}{2}dat.nsf", year,
				                               ndbName,
				                               year.Substring(2));
				string ndbOrigPath = string.Format("CM\\{0}\\{1}{2}img.nsf", year,
				                                   ndbName,
				                                   year.Substring(2));
				
				AllLoadDocumentsTest(docType, documentsView, year, defaultEmployee, nameDocFormat, tempFolder, pServer, ndbPath, ndbOrigPath, ndbOrgStructPath, ndbDominoPath, datetimeNow, maxCountDocument);
			}
		}
		
		/// <summary>
		/// Загрузка группы документов.
		/// </summary>
		public virtual void AllLoadDocumentsTest(string docType, string documentsView, string year, IEmployee defaultEmployee, string nameDocFormat, string tempFolder,
		                                         string pServer, string ndbPath, string ndbOrigPath, string ndbOrgStructPath, string ndbDominoPath,
		                                         string datetimeNow,
		                                         int maxCountDocument = 1, int cntAll = 0, int cntLoad = 0, int cntError = 0, int cntWithoutAttach = 0)
		{
			Dialogs.ShowMessage(pServer);
			Dialogs.ShowMessage(ndbPath);
			
			ProgressBarDRX progressBar = new ProgressBarDRX(maxCountDocument); //объект прогресс бар
			progressBar.Show();
			progressBar.Text("Подключение к базе...");
			
			try
			{
				NotesSessionClass notesSession = new NotesSessionClass();
				notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
				NotesDatabase ndbDocuments = notesSession.GetDatabase(pServer, ndbPath);
				NotesDatabase ndbOrigDocuments = notesSession.GetDatabase(pServer, ndbOrigPath);
				NotesDatabase ndbOrgStructure = notesSession.GetDatabase(pServer, ndbOrgStructPath);
				NotesDatabase ndbDominoDirectory = notesSession.GetDatabase(pServer, ndbDominoPath);
				
				if (!ndbDocuments.IsOpen)
					ndbDocuments.Open();
				if (!ndbOrigDocuments.IsOpen)
				{
					try
					{
						ndbOrigDocuments.Open();
					}
					catch (System.Runtime.InteropServices.COMException)
					{
						ndbOrigDocuments = ndbDocuments;
					}
				}
				if (!ndbOrgStructure.IsOpen)
					ndbOrgStructure.Open();
				if (!ndbDominoDirectory.IsOpen)
					ndbDominoDirectory.Open();

				NotesView notesView = ndbDocuments.GetView(documentsView);
				NotesDocument document = null;
				NotesDocumentCollection documents = null;
				int allDocumentCount = 0;
				
				if (docType.Equals("Входящее письмо"))
				{
					allDocumentCount = notesView.EntryCount;
					document = notesView.GetFirstDocument();
				}
				else
				{
					documents = notesView.GetAllDocumentsByKey(docType);
					allDocumentCount = documents.Count;
					document = documents.GetFirstDocument();
				}
				
				System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
				sw.Start();
				
				progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
				
				while (document != null && ((maxCountDocument <= 0 && cntLoad < allDocumentCount && cntError < allDocumentCount) || (maxCountDocument > 0 && cntLoad < maxCountDocument && cntError < maxCountDocument)))
				{
					string note = ((object[])document.GetItemValue("DocID"))[0].ToString();
					string noteAttach = "";
					string link = string.Format("notes://{0}/{1}/0/{2}",
					                            "MSC-04",
					                            ndbDocuments.ReplicaID,
					                            note);
					
					try
					{
//						var ReplicaID = document.HasItem("Orig_ReplicaId_F");
						
//						string paramIDName = int.Parse(year) < 2019 ? "Orig_ID" : "Orig_ID_F";
						string paramIDName = docType.Equals("Входящее письмо") ? "Orig_ID_F" : "Orig_ID";
						
//						Dialogs.ShowMessage(((object[])document.GetItemValue("Orig_ID"))[0].ToString() + " 1\r\n"
//						                    + ((object[])document.GetItemValue("Orig_ID_F"))[0].ToString() + " 2\r\n"
//						                   + ((object[])document.GetItemValue("Orig_ReplicaId"))[0].ToString() + " 3\r\n"
//						                  + ((object[])document.GetItemValue("Orig_ReplicaId_F"))[0].ToString() + " 4\r\n");
						
						string docAttachID = ((object[])document.GetItemValue(paramIDName))[0].ToString();

						NotesDocument documentWithAttach = !string.IsNullOrEmpty(docAttachID) ? ndbOrigDocuments.GetDocumentByUNID(docAttachID) : document;
						
						noteAttach = ((object[])documentWithAttach.GetItemValue("DocID"))[0].ToString();
						
						List<string> nameDocumentList = GetNameAttachmentList(documentWithAttach);
						
						if (nameDocumentList.Count > 0 && documentWithAttach != null)
						{
							string path = string.Format("{0}LOTUS {1} {2} (ID {3}){4}",
							                            tempFolder,
							                            datetimeNow,
							                            docType,
							                            note,
							                            System.IO.Path.GetExtension(nameDocumentList[0]));
							
							documentWithAttach.GetAttachment(nameDocumentList[0]).ExtractFile(path);
							
							var columnNamesLetter = new List<string>() {"Subject", "DocToolTip", "From", "OutDate", "OutNumber", "Links_DocName", "Userfrom", "Mast", "strorg", "Registration", "To", "RNumber_Full", "RDate", "Stamp", "Inventory_BelongToFileType", "Inventory_DateOfAddingToFile", "DocID"};
							
							var columnNamesOrder = new List<string>() {"Subject", "strorg", "Registration", "Userfrom", "Mast", "RNumber_Full", "OutDate", "DocID"};
							
							int? docID = null;
							string entityType = "";
							var lotusValues = new Dictionary<string, string>();

							if (docType.Equals("Входящее письмо"))
							{
								lotusValues = GetValueRequisites(document, 1000, columnNamesLetter);
								
								var externalEntityLink = Functions.Module.Remote.GetExEntityLinkByExID(lotusValues["DocID"]);
								
								IIncomingLetter incomingLetter = externalEntityLink != null ? Functions.Module.Remote.GetIncomingLettersByID((int)externalEntityLink.EntityId) : null;
								
								if (incomingLetter == null)
									incomingLetter = IncomingLetters.CreateFrom(path);
								else
									incomingLetter.Versions.First().Import(path);

								incomingLetter.Subject = string.Format("{0} (№ {1} [{3}] от {2} [{4}])",
								                                       lotusValues["Subject"],
								                                       lotusValues["RNumber_Full"],
								                                       lotusValues["RDate"],
								                                       lotusValues["OutNumber"],
								                                       lotusValues["OutDate"]);

								incomingLetter.Note = string.Format("{0}{1} ({2}) [{3}]\r\n{4}",
								                                    nameDocFormat,
								                                    !string.IsNullOrEmpty(lotusValues["RNumber_Full"]) ? lotusValues["RNumber_Full"] : (cntLoad + 1).ToString(),
								                                    nameDocumentList[0],
								                                    Hyperlinks.Functions.GetLinkExEntity(link),
								                                    lotusValues["ResultText"]);
								
								incomingLetter.Author = defaultEmployee;
								incomingLetter.Correspondent = Functions.Module.Remote.GetCounterparty();
								incomingLetter.Department = defaultEmployee.Department;
								incomingLetter.Save();
								
								docID = incomingLetter.Id;
								entityType = "aabe3f4a-d52b-41b4-9e45-8a62a0d8ad13";
							}
							else
							{
								lotusValues = GetValueRequisites(document, 1000, columnNamesOrder);
								
								var emailMast = GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, lotusValues["Mast"]);
								var emailUserFrom = GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, lotusValues["Userfrom"]);
								
								var employeeMast = Functions.Module.Remote.GetEmployeeByEmail(emailMast);
								var employeeUserFrom = Functions.Module.Remote.GetEmployeeByEmail(emailUserFrom);

								var externalEntityLink = Functions.Module.Remote.GetExEntityLinkByExID(lotusValues["DocID"]);
								
								IOrder order = externalEntityLink != null? Functions.Module.Remote.GetOrdersByID((int)externalEntityLink.EntityId) : null;
								
								if (order == null)
									order = Orders.CreateFrom(path);
								else
									order.Versions.First().Import(path);
								
								order.DocumentKind = Functions.Module.Remote.GetDocumentKind(docType);
								
								order.Subject = string.Format("{0} (№ {1} от {2})",
								                              lotusValues["Subject"],
								                              lotusValues["RNumber_Full"],
								                              lotusValues["OutDate"]);
								
								order.Note = string.Format("{0}{1} ({2} [({3}]\r\n{4}",
								                           nameDocFormat,
								                           !string.IsNullOrEmpty(lotusValues["RNumber_Full"]) ? lotusValues["RNumber_Full"] : (cntLoad + 1).ToString(),
								                           nameDocumentList[0],
								                           Hyperlinks.Functions.GetLinkExEntity(link),
								                           lotusValues["ResultText"]);
								
								order.Author = employeeMast != null ? employeeMast : defaultEmployee;
								order.PreparedBy = employeeMast != null ? employeeMast : defaultEmployee;
								order.OurSignatory = employeeUserFrom;
								order.Department = (employeeMast != null) ? employeeMast.Department : defaultEmployee.Department;
								order.Save();
								
								docID = order.Id;
								entityType = "cbc14e48-3ebb-4105-b1c6-8c86b0fdb773";
							}
							
							cntLoad++;
							
							Functions.Module.Remote.CreateExEntityLink(entityType,
							                                           docID,
							                                           link,
							                                           lotusValues["DocID"]);
							
							progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
							progressBar.Next();
						}
						else
						{
							cntWithoutAttach++;
						}
					}
					catch (Exception ex)
					{
						cntError++;
						var message = ex.Message;
						using (var file = File.AppendText(string.Format(@"C:\DRX\log\log_migration_{0}.txt", datetimeNow)))
						{
							file.WriteLine(string.Format("Документ ID: {0} ({1})\r\n{2}", note, noteAttach, message));
						}
					}
					
					cntAll++;
					
					if (docType.Equals("Входящее письмо"))
						document = notesView.GetNextDocument(document);
					else
						document = documents.GetNextDocument(document);
				}
				progressBar.Hide();
				progressBar = null;
				
				sw.Stop();
				string resultMessage = string.Format("Загрузка завершена.\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество загруженных документов: {0}\r\n"
				                                     + "Количество документов без вложений: {1}\r\n"
				                                     + "Количество ошибок при загрузке: {2}\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество просмотренных документов: {3}\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Время выполнения: {4}",
				                                     cntLoad, cntWithoutAttach, cntError, cntAll, sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
				
				Dialogs.ShowMessage(resultMessage, MessageType.Information);
				
				if (docType.Equals("Входящее письмо"))
					Functions.Module.Remote.GetIncomingLettersByStrTemplate(nameDocFormat).ShowModal();
				else
					Functions.Module.Remote.GetOrdersByStrTemplate(nameDocFormat).ShowModal();

				sw = null;
			}
			catch (Exception ex)
			{
				Dialogs.ShowMessage("ОШИБКА.\r\n-------------------\r\n"
				                    + ex.Message +
				                    "\r\n-------------------\r\n" + ex.ToString(), MessageType.Information);
				if (progressBar != null)
				{
					progressBar.Hide();
					progressBar = null;
				}
			}
		}
		
		/// <summary>
		/// Вернуть ссылку
		/// </summary>
		[Public, Hyperlink(DisplayNameResource="CompanyMedia")]
		public void GetLinkExEntity(string link)
		{
			Hyperlinks.Open(link);
		}

		/// <summary>
		/// Получить Email пользователя по его Фамилия И.О.
		/// </summary>
		public virtual string GetEmailFromFIO(Domino.NotesDatabase orgStructureDB, Domino.NotesDatabase dominoDirectoryDB, string userFIO)
		{
			try
			{
				string query, userNotesName, userMail;

				query = string.Format("EShortName = \"{0}\"", userFIO);
				NotesDocument userInOrgStructure = orgStructureDB.Search(query, null, 0).GetFirstDocument();
				userNotesName = ((object[])userInOrgStructure.GetItemValue("NotesName"))[0].ToString();
				
				query = string.Format("FullName = \"{0}\"", userNotesName);
				NotesDocument userInDominoDirectory = dominoDirectoryDB.Search(query, null, 0).GetFirstDocument();
				userMail = ((object[])userInDominoDirectory.GetItemValue("InternetAddress"))[0].ToString();
				return userMail;
			}
			catch (Exception ex)
			{
				var message = ex.Message;
//				Dialogs.ShowMessage("ОШИБКА.\r\n-------------------\r\n"
//				                    + ex.Message +
//				                    "\r\n-------------------\r\n" + ex.ToString(), MessageType.Information);
				return string.Empty;
			}
		}
		
		/// <summary>
		/// Получить лист с наименованиями вложений.
		/// </summary>
		public virtual List<string> GetNameAttachmentList(Domino.NotesDocument document)
		{
			List<string> NameAttachList = new List<string>();
			
			try
			{
//				if (document.HasItem("$File"))
				if (document.HasEmbedded && document.HasItem("$File"))
				{
					object[] AllDocItems = (object[])document.Items;
					
					foreach (object currentItem in AllDocItems)
					{
						try
						{
							NotesItem nItem = (NotesItem)currentItem;
							
							if (IT_TYPE.ATTACHMENT == nItem.type)
							{
								foreach (object NameAttach in (object[])nItem.Values)
								{
									NameAttachList.Add(NameAttach.ToString());
								}
							}
						}
						catch
						{
							
						}
					}
				}
			}
			catch
			{
				
			}
			
			return NameAttachList;
		}
		
		/// <summary>
		/// Получить лист с наименованиями вложений.
		/// </summary>
		public virtual List<string> GetNameItemsList(Domino.NotesDocument document)
		{
			List<string> NameList = new List<string>();
			
			try
			{
//				if (document.HasEmbedded && document.HasItem("$File"))
//				if (document.HasItem("$File"))
//				{
				object[] AllDocItems = (object[])document.Items;
				
				if (AllDocItems != null)
				{
					foreach (object currentItem in AllDocItems)
					{
						try
						{
							NotesItem nItem = (NotesItem)currentItem;
							
							foreach (object NameAttach in (object[])nItem.Values)
							{
								NameList.Add(NameAttach.ToString());
							}
//							NameList.Add("|");
						}
						catch
						{
							
						}
					}
				}
//				}
			}
			catch
			{
				
			}
			
			return NameList;
		}

		/// <summary>
		/// Считать реквизиты документа.
		/// </summary>
		public virtual string GetValueRequisites(Domino.NotesDocument document, int maxCount)
		{
			string[] columnNames = {"Subject", "ReqType", "rfin", "to", "rprist", "RNumber", "UserFrom", "body"};
			string result = "Реквизиты документа:";
			int i = 1;
			
			foreach (string columnName in columnNames)
			{
				try
				{
					string curValue = string.Format("\r\n{0}. {1} = |", i, columnName);
					object[] curValues = (object[])document.GetItemValue(columnName);
					foreach (string valueString in curValues)
					{
						curValue += valueString.ToString() + "|";
					}
					result += curValue;
					i++;
				}
				catch
				{
					
				}
			}
			result = result.Count() > maxCount ? result.Substring(0, maxCount) : result;
			return result;
		}
		
		/// <summary>
		/// Считать реквизиты документа.
		/// </summary>
		/// <returns>Возвращает коллекцию с именем и значением параметров, один из которых ResultText - оформленный текст со значениями всех параметров.</returns>
		public static System.Collections.Generic.Dictionary<string,string> GetValueRequisites(Domino.NotesDocument document, int maxCount, List<string> columnNames)
		{
			Dictionary<string, string> requisites = new Dictionary<string,string>();
			
			string result = "Реквизиты документа:";
			int i = 1;
			
			foreach (string columnName in columnNames)
			{
				try
				{
					object[] curValuesParam = (object[])document.GetItemValue(columnName);
					string paramValuesStr = string.Join("; ", curValuesParam.Select(s => s is DateTime ? ((DateTime)s).ToString("d") : s.ToString()));
					requisites.Add(columnName, paramValuesStr);
					string curValue = string.Format("\r\n{0}. {1} = [{2}]", i, columnName, paramValuesStr);
					result += curValue;
					i++;
				}
				catch
				{
				}
			}
			result = result.Length > maxCount ? result.Substring(0, maxCount) : result;
			requisites.Add("ResultText", result);
			
			return requisites;
		}
		
		/// <summary>
		/// Подключиться к БД и загрузить документ.
		/// </summary>
		public virtual void ConnectToBD()
		{
			int i = 0;
			ISimpleDocument SDoc = null;
			string result = "УДАЧНО!\r\n";
			try
			{
				NotesSessionClass notesSession = new NotesSessionClass();
				notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
				NotesDatabase nDatabaseDocuments = notesSession.GetDatabase("Domino-02/burgaz", "CM\\2019\\int19dat.nsf");
				NotesDatabase nDatabaseOrgStructure = notesSession.GetDatabase("MSC-04/burgaz", "CM\\cmso.nsf");
				NotesDatabase nDatabaseDominoDirectory = notesSession.GetDatabase("MSC-04/burgaz", "names.nsf");
				if (!nDatabaseDocuments.IsOpen)
					nDatabaseDocuments.Open();
				if (!nDatabaseOrgStructure.IsOpen)
					nDatabaseOrgStructure.Open();
				if (!nDatabaseDominoDirectory.IsOpen)
					nDatabaseDominoDirectory.Open();
				// NotesView notesView = notesDatabase.GetView("JournalByDate");
				// NotesDocument NDoc = notesView.GetFirstDocument();
				// Создать диалог ввода.
				
				var dialog = Dialogs.CreateInputDialog("Введите DocID из Lotus");
				var docID = dialog.AddString("DocID", true, "9E3972B8FA41517C4325838A00236E8E");
				// Отобразить диалог и проверить, что нажата кнопка «ОК».
				if (dialog.Show() == DialogButtons.Ok)
				{
					NotesDocument NDoc = nDatabaseDocuments.GetDocumentByUNID(docID.Value);
					if (NDoc != null)
					{
						string[] columnNames = {"Subject", "ReqType", "UserFrom", "to", "rprist", "RNumber", "rfin", "body", "$FILE"};
						string subject = "Найденные пользователи = ";
						
						foreach (string columnName in columnNames)
						{
							string curValue = "|";
							object[] curValues = (object[])NDoc.GetItemValue(columnName);
							foreach (string valueString in curValues)
							{
								curValue += valueString.ToString() + "|";
							}
							if (!string.IsNullOrEmpty(curValue))
								result += string.Format("\r\n{0}.{1}: \"{2}\"", i, columnName, curValue);
							else
								result += string.Format("\r\n{0}.{1}: null", i, columnName);
							i++;
						}

						if (NDoc.HasEmbedded && NDoc.HasItem("$File"))
						{
							object[] AllDocItems = (object[])NDoc.Items;
							string pAttachment = "";
							string path = "";
							
							foreach (object CurItem in AllDocItems)
							{
								NotesItem nItem = (NotesItem)CurItem;
								if (IT_TYPE.ATTACHMENT == nItem.type)
								{
									pAttachment = ((object[])nItem.Values)[0].ToString();
									path = @"C:\DRX\TEST\" + pAttachment;
									NDoc.GetAttachment(pAttachment).ExtractFile(path);
									break;
								}
							}
							
							if (!string.IsNullOrEmpty(pAttachment))
							{
								string nameForFind = ((object[])NDoc.GetItemValue("To"))[0].ToString();
								if (!string.IsNullOrEmpty(nameForFind))
								{
									string query = string.Format("EShortName = \"{0}\"", nameForFind);
									NotesDocumentCollection nDocCollection = nDatabaseOrgStructure.Search(query, null, 0);
									subject += string.Format("{0} ({1})\r\n", nameForFind, nDocCollection.Count.ToString());
									NotesDocument currentPosition = nDocCollection.GetFirstDocument();
									while (currentPosition != null) {
										string notesName = ((object[])currentPosition.GetItemValue("NotesName"))[0].ToString();
										string userMail;
										query = string.Format("FullName = \"{0}\"", notesName);
										NotesDocumentCollection userCollection = nDatabaseDominoDirectory.Search(query, null, 0);
										subject += string.Format("\r\n|{0}| = (", notesName);
										NotesDocument user = userCollection.GetFirstDocument();
										while (user != null) {
											userMail = ((object[])user.GetItemValue("InternetAddress"))[0].ToString();
											subject += string.Format("{{{0}}}", userMail);
											user = userCollection.GetNextDocument(user);
										}
										subject += ")" + "{{{{" + GetEmailFromFIO(nDatabaseOrgStructure, nDatabaseDominoDirectory, nameForFind);
										currentPosition = nDocCollection.GetNextDocument(currentPosition);
									}
								}
								SDoc = SimpleDocuments.CreateFrom(path);
								SDoc.Name = pAttachment;
								SDoc.Subject = subject.Count() > 250 ? subject.Substring(1, 250) : subject;
								SDoc.Note = result.Count() > 1000 ? result.Substring(1, 1000) : result;
								SDoc.Save();
							}
							//return result + "\r\n\r\nPath = " + path;
						}
					}
					else
					{
						Dialogs.ShowMessage("Документ не найден.", MessageType.Warning);
					}
				}
				else
				{
					Dialogs.NotifyMessage("DocID не введён.");
				}
			}
			catch (Exception ex)
			{
				Dialogs.ShowMessage("ОШИБКА!\r\n" + "\r\n" + ex.Message + "\r\n\r\n" + ex.ToString(), MessageType.Information);
			}
			
			//===============================================================
			if (SDoc != null)
				SDoc.Show();
			else
				Dialogs.ShowMessage(result, MessageType.Information);
		}

		/// <summary>
		/// Загрузить группу документов.
		/// </summary>
		public virtual void AllLoadDocuments2()
		{
			var dialog = Dialogs.CreateInputDialog("Количество документов для загрузки");
			var dialogCountDoc = dialog.AddInteger("Кол-во документов", true, 1);
			
			// Отобразить диалог и проверить, что нажата кнопка «ОК».
			if (dialog.Show() == DialogButtons.Ok && dialogCountDoc.Value > 0)
			{
				int cntAll = 0, cntLoad = 0, cntError = 0, cntWithoutAttach = 0;
				int maxCountDocument = (int) dialogCountDoc.Value;
				string tempFolder = @"C:\DRX\TEST\";
				string datetimeNow = Calendar.Now.ToString("yyyy.MM.dd_HH.mm.ss");
				string nameLetterFormat = string.Format("TestLoad {0} Входящее письмо №",
				                                        Calendar.Now.ToString("yyyy.MM.dd HH:mm:ss"));
				
				ProgressBarDRX progressBar = new ProgressBarDRX(maxCountDocument); //объект прогресс бар
				progressBar.Show();
				progressBar.Text("Подключение к базе...");
				
				try
				{
					NotesSessionClass notesSession = new NotesSessionClass();
					notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
					NotesDatabase ndbDocuments = notesSession.GetDatabase("MSC-04/burgaz", "CM\\2018\\inp18dat.nsf");
					NotesDatabase ndbOrigDocuments = notesSession.GetDatabase("MSC-04/burgaz", "CM\\2018\\inp18img.nsf");
					NotesDatabase ndbOrgStructure = notesSession.GetDatabase("MSC-04/burgaz", "CM\\cmso.nsf");
					NotesDatabase ndbDominoDirectory = notesSession.GetDatabase("MSC-04/burgaz", "names.nsf");
					if (!ndbDocuments.IsOpen)
						ndbDocuments.Open();
					if (!ndbOrigDocuments.IsOpen)
						ndbOrigDocuments.Open();
					if (!ndbOrgStructure.IsOpen)
						ndbOrgStructure.Open();
					if (!ndbDominoDirectory.IsOpen)
						ndbDominoDirectory.Open();
					NotesView notesView = ndbDocuments.GetView("JournalByDate");
					
					System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
					sw.Start();
					
					progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument,cntWithoutAttach, cntError));
					
					NotesDocument document = notesView.GetFirstDocument();
					while (cntLoad < maxCountDocument && document != null && cntError < maxCountDocument)
					{
						try
						{
							string docAttachID = ((object[])document.GetItemValue("Orig_ID"))[0].ToString();
							NotesDocument documentWithAttach = docAttachID != string.Empty ? ndbOrigDocuments.GetDocumentByUNID(docAttachID) : document;
							List<string> nameDocumentList = GetNameAttachmentList(documentWithAttach);
							if (nameDocumentList.Count > 0 && documentWithAttach != null)
							{
								string path = string.Format("{0}TestLoad_{1}_№{2}{3}",
								                            tempFolder,
								                            datetimeNow,
								                            cntLoad + 1,
								                            System.IO.Path.GetExtension(nameDocumentList[0]));
								string nameLetter = string.Format("{0}{1} ({2})",
								                                  nameLetterFormat,
								                                  datetimeNow,
								                                  cntLoad + 1,
								                                  nameDocumentList[0]);
								
								string userFIO = ((object[])document.GetItemValue("To"))[0].ToString();
								documentWithAttach.GetAttachment(nameDocumentList[0]).ExtractFile(path);
								
								IIncomingLetter incomingLetter = IncomingLetters.CreateFrom(path);
								incomingLetter.Subject = string.Format("{0}\r\n{1}: {2}",
								                                       nameLetter,
								                                       userFIO,
								                                       GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, userFIO));
								incomingLetter.Note = GetValueRequisites(document, 1000);
								incomingLetter.Author = Functions.Module.Remote.GetUser();
								incomingLetter.Correspondent = Functions.Module.Remote.GetCounterparty();
								incomingLetter.Department = Functions.Module.Remote.GetDepartment();
								incomingLetter.Save();
								cntLoad++;
								
								progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument,cntWithoutAttach, cntError));
								progressBar.Next();
							}
							else
							{
								cntWithoutAttach++;
							}
						}
						catch (Exception ex)
						{
							cntError++;
							var message = ex.Message;
//							sw.Stop();
//							Dialogs.ShowMessage("Ошибка при загрузке документа.\r\n-------------------\r\n"
//							                    + ex.Message +
//							                    "\r\n-------------------\r\n" + ex.ToString(), MessageType.Information);
//							sw.Start();
						}
						cntAll++;
						document = notesView.GetNextDocument(document);
					}
					progressBar.Hide();
					progressBar = null;
					
					sw.Stop();
					string resultMessage = string.Format("Загрузка завершена.\r\n"
					                                     + "---------------------------------------------------------\r\n"
					                                     + "Количество загруженных документов: {0}\r\n"
					                                     + "Количество документов без вложений: {1}\r\n"
					                                     + "Количество ошибок при загрузке: {2}\r\n"
					                                     + "---------------------------------------------------------\r\n"
					                                     + "Количество просмотренных документов: {3}\r\n"
					                                     + "---------------------------------------------------------\r\n"
					                                     + "Время выполнения: {4}",
					                                     cntLoad, cntWithoutAttach, cntError, cntAll, sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
					Dialogs.ShowMessage(resultMessage, MessageType.Information);
					Functions.Module.Remote.GetIncomingLettersByStrTemplate(nameLetterFormat).ShowModal();
					sw = null;
				}
				catch (Exception ex)
				{
					Dialogs.ShowMessage("ОШИБКА.\r\n-------------------\r\n"
					                    + ex.Message +
					                    "\r\n-------------------\r\n" + ex.ToString(), MessageType.Information);
					if (progressBar != null)
					{
						progressBar.Hide();
						progressBar = null;
					}
				}
			}
		}
		
		
		/// <summary>
		/// 
		/// </summary>
		public virtual void TestFunction()
		{
//			var dlg = Dialogs.CreateInputDialog("Даты");
//			var date1 = dlg.AddString("Дата старта", true, "29.08.2019 22:00:00");
//			var date2 = dlg.AddString("Срок", true, "23.09.2019 18:00:00");
//			var dateV1 = Calendar.Now;
//			var dateV2 = Calendar.Now;
//			var dateE1 = Calendar.Now;
//			var dateE2 = Calendar.Now;
//			if (dlg.Show() == DialogButtons.Ok)
//			{
//				Calendar.TryParseDateTime(date1.Value, out dateV1);
//				Calendar.TryParseDateTime(date2.Value, out dateV2);
//				var hours = WorkingTime.GetDurationInWorkingHours(dateV1, dateV2);
//				Dialogs.ShowMessage(hours.ToString());
//				Dialogs.ShowMessage(dateV1.AddHours(hours).ToString());
//				Dialogs.ShowMessage(dateV1.AddWorkingHours(hours).ToString());
//			}
			
//			try
//			{
//				NotesSessionClass notesSession = new NotesSessionClass();
//				notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
//				NotesDatabase nDatabaseDocuments = notesSession.GetDatabase("Domino-02/burgaz", "CM\\2019\\int19dat.nsf");
//				NotesDatabase nDatabaseOrgStructure = notesSession.GetDatabase("MSC-04/burgaz", "CM\\cmso.nsf");
//				NotesDatabase nDatabaseDominoDirectory = notesSession.GetDatabase("MSC-04/burgaz", "names.nsf");
//				if (!nDatabaseDocuments.IsOpen)
//					nDatabaseDocuments.Open();
//				if (!nDatabaseOrgStructure.IsOpen)
//					nDatabaseOrgStructure.Open();
//				if (!nDatabaseDominoDirectory.IsOpen)
//					nDatabaseDominoDirectory.Open();
			
//				string result = "УДАЧНО!\r\n";
//				try
//				{
//					NotesSessionClass notesSession = new NotesSessionClass();
//					notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
//					NotesDatabase nDatabaseDocuments = notesSession.GetDatabase("Domino-02/burgaz", "CM\\2019\\int19dat.nsf");
//					NotesDatabase nDatabaseOrgStructure = notesSession.GetDatabase("MSC-04/burgaz", "CM\\cmso.nsf");
//					NotesDatabase nDatabaseDominoDirectory = notesSession.GetDatabase("MSC-04/burgaz", "names.nsf");
//					if (!nDatabaseDocuments.IsOpen)
//						nDatabaseDocuments.Open();
//					if (!nDatabaseOrgStructure.IsOpen)
//						nDatabaseOrgStructure.Open();
//					if (!nDatabaseDominoDirectory.IsOpen)
//						nDatabaseDominoDirectory.Open();
//					// Создать диалог ввода.
//
//					var dialog = Dialogs.CreateInputDialog("Введите DocID из Lotus");
//					var docID = dialog.AddString("DocID", true, "9E3972B8FA41517C4325838A00236E8E");
//					// Отобразить диалог и проверить, что нажата кнопка «ОК».
//					if (dialog.Show() == DialogButtons.Ok)
//					{
//						NotesDocument NDoc = nDatabaseDocuments.GetDocumentByUNID(docID.Value);
//						if (NDoc != null)
//						{
			////							Dialogs.ShowMessage(string.Format("NotesURL: \"{0}\"", NDoc.NotesURL), MessageType.Information);
			////							Hyperlinks.Open(NDoc.NotesURL);
			////							var dialog1 = Dialogs.CreateInputDialog("NotesURL");
			////							var docID1 = dialog1.AddString("URL", false, NDoc.NotesURL);
			////							dialog1.Show();
//							Dialogs.ShowMessage("", MessageType.Warning);
//						}
//						else
//						{
//							Dialogs.ShowMessage("Документ не найден.", MessageType.Warning);
//						}
//					}
//					else
//					{
//						Dialogs.NotifyMessage("DocID не введён.");
//					}
//				}
//				catch (Exception ex)
//				{
//					Dialogs.ShowMessage("ОШИБКА!\r\n" + "\r\n" + ex.Message + "\r\n\r\n" + ex.ToString(), MessageType.Information);
//				}
//
//
//
			////				var dialog = Dialogs.CreateInputDialog("Введите DocID из Lotus");
			////				var fio = dialog.AddString("Фамилия И.О.", true, "");
			////
			////				// Отобразить диалог и проверить, что нажата кнопка «ОК».
			////				if (dialog.Show() == DialogButtons.Ok)
			////				{
			////
			////					Dialogs.ShowMessage(fio.Value + ": " + GetEmailFromFIO(nDatabaseOrgStructure, nDatabaseDominoDirectory, fio.Value), MessageType.Information);
			////				}
//			}
//			catch
//			{
//
//			}
		}
		
		
		/// <summary>
		/// Диалог загрузки документов.
		/// </summary>
		public virtual void LoadDocumentsDialog()
		{
			List<Structures.Module.DocumentTypeSetting> settingsList = new List<Structures.Module.DocumentTypeSetting>()
			{
//				new Structures.Module.DocumentTypeSetting() { Name = "Входящее письмо" , NameDBLotus = "int", DocumentsView = "JournalByDate"},
				new Structures.Module.DocumentTypeSetting() { Name = "Приказ" , NameDBLotus = "mis", DocumentsView = "JournalByType"},
				new Structures.Module.DocumentTypeSetting() { Name = "Распоряжение" , NameDBLotus = "mis", DocumentsView = "JournalByType"}
			};
			
			var lotusDB = new Dictionary<string, List<string>>
			{
				{"MSC-04/burgaz", new List<string>{"mis16dat.nsf", "mis17dat.nsf", "mis18dat.nsf", "mis19dat.nsf", "mis16img.nsf", "mis17img.nsf", "mis18img.nsf", "mis19img.nsf"} },
				{"AS-01/ABG/burgaz", new List<string>{"missions-2016.nsf", "missions-2017.nsf", "missions-2018.nsf", "missions-2019.nsf", "origs-missions-2016.nsf", "origs-missions-2017.nsf", "origs-missions-2018.nsf", "origs-missions-2019.nsf"} },
				{"CB-01/CGB/burgaz", new List<string>{"16misdat.nsf", "17misdat.nsf", "18misdat.nsf", "19misdat.nsf", "16misimg.nsf", "17misimg.nsf", "18misimg.nsf", "19misimg.nsf"} },
				{"CCS-01/CCS/burgaz", new List<string>{"ccs_missions2016.nsf", "ccs_missions2017.nsf", "ccs_missions2018.nsf", "ccs_missions2019.nsf", "ccs_origs_missions_2016.nsf", "ccs_origs_missions_2017.nsf", "ccs_origs_missions_2018.nsf", "ccs_origs_missions_2019.nsf"} },
				{"IRB-01/IRB/burgaz", new List<string>{"missions.nsf", "missions2019.nsf"} },
				{"NU-01/TBG/burgaz", new List<string>{"missions2016.nsf", "missions2017.nsf", "missions2018.nsf", "missions2019.nsf", "miss2016.nsf", "miss2017.nsf", "miss2018.nsf", "miss2019.nsf"} },
				{"OR-01/OBG/burgaz", new List<string>{"missions2016.nsf", "missions2017.nsf", "missions2018.nsf", "missions2019.nsf", "origs_missions2016.nsf", "origs_missions2017.nsf", "origs_missions2018.nsf", "origs_missions2019.nsf"} },
				{"KR-01/KBG/burgaz", new List<string>{"missions2016.nsf", "missions2017.nsf", "missions2018.nsf", "missions2019.nsf", "missOr2016.nsf", "missOr2017.nsf", "missOr2018.nsf", "missOr2019.nsf"} }
			};
			
			var regGroups = new Dictionary<string, string>
			{
				{"MSC-04/burgaz", "АУП. Канцелярия Приказы/Распоряжения"},
				{"AS-01/ABG/burgaz", "АБ. Канцелярия"},
				{"CB-01/CGB/burgaz", "ЦГБ. Канцелярия"},
				{"CCS-01/CCS/burgaz", "ЦЦС. Канцелярия"},
				{"IRB-01/IRB/burgaz", "ИБ. Канцелярия"},
				{"NU-01/TBG/burgaz", "УрБ. Канцелярия"},
				{"OR-01/OBG/burgaz", "ОБ. Канцелярия"},
				{"KR-01/KBG/burgaz", "КБ. Канцелярия"}
			};

			var dialog = Dialogs.CreateInputDialog("Параметры загрузки");
			var dialogCountDoc = dialog.AddInteger("Кол-во документов", true, 1);
			var dialogYear = dialog.AddSelect("Год", true, 3).From("2016", "2017", "2018", "2019");
			var dialogType = dialog.AddSelect("Вид документа", true, 0).From("Приказ", "Распоряжение");
			var dialogServer = dialog.AddSelect("Сервер", true).From(lotusDB.Select(s => s.Key).ToArray());
			var dialogPrefixDB = dialog.AddString("Префикс базы", true, "CM\\2019\\");
			var dialogMain = dialog.AddSelect("БД документов", true);
			var dialogOrig = dialog.AddSelect("БД оригиналов", false);
			var dialogOrgStruct = dialog.AddString("БД оргструктуры", true, "CM\\cmso.nsf");
			var dialogDomino = dialog.AddString("БД сотрудников", true, "names.nsf");
			var dialogDefaultEmployee = dialog.AddSelect("Сотрудник по умолчанию", true, Employees.Null);
			var dialogRegGroup = dialog.AddSelect("Группа регистрации", false).From(regGroups.Select(s => s.Value).ToArray());
			var dialogIsServer = dialog.AddBoolean("Выполняется на сервере", false);
			var dialogCountAttempts = dialog.AddInteger("Количество попыток загрузки", true, 5);
			var dialogTimeAttempts = dialog.AddInteger("Время между попытками (мс)", true, 1000);
			var dialogMyServer = dialog.AddString("Произвольный сервер", false);
			var dialogMyDB = dialog.AddString("Произвольная база", false);
			var dialogMyOrigDB = dialog.AddString("Произвольная база вложений", false);
			var dialogIsExcel = dialog.AddBoolean("Импорт на основании Excel документа", true);
			var dialogIsRewriteExcel = dialog.AddBoolean("Проанализировать БД заново и перезаписать Excel", false);
			var dialogIsUpdateInfo = dialog.AddBoolean("Подгружать данные из БД", false);
			dialogServer.SetOnValueChanged(func =>
			                               {
			                               	if (!string.IsNullOrEmpty(dialogServer.Value))
			                               	{
			                               		dialogMain.Value = string.Empty;
			                               		dialogOrig.Value = string.Empty;
			                               		
			                               		var listDB = lotusDB[dialogServer.Value];
			                               		var nameDB = listDB.Where(s => s.Contains(dialogYear.Value.Substring(2)));
			                               		if (nameDB.Count() > 0)
			                               		{
			                               			dialogMain.From(listDB.Take(4).ToArray());
			                               			dialogOrig.From(listDB.Skip(listDB.Count > 4 ? 4 : 0).ToArray());
			                               			dialogMain.Value = nameDB.First();
			                               			dialogOrig.Value = nameDB.Last();
			                               		}
			                               		else
			                               		{
			                               			dialogMain.From(listDB.ToArray());
			                               			dialogOrig.From(listDB.ToArray());
			                               		}
			                               		
			                               		dialogRegGroup.Value = regGroups[dialogServer.Value];
			                               	}
			                               });
			dialogYear.SetOnValueChanged(func =>
			                             {
			                             	if (!string.IsNullOrEmpty(dialogYear.Value))
			                             	{
			                             		dialogPrefixDB.Value = string.Format("CM\\{0}\\", dialogYear.Value);
			                             		var dServerValue = dialogServer.Value;
			                             		dialogServer.Value = string.Empty;
			                             		dialogServer.Value = dServerValue;
			                             	}
			                             });
//			dialogIsExcel.SetOnValueChanged(func =>
			dialog.SetOnRefresh(func =>
			                    {
			                    	if ((bool) dialogIsExcel.Value)
			                    	{
			                    		dialogIsRewriteExcel.IsEnabled = true;
			                    		dialogIsUpdateInfo.IsEnabled = true;
			                    	}
			                    	else
			                    	{
			                    		dialogIsRewriteExcel.IsEnabled = false;
			                    		dialogIsUpdateInfo.IsEnabled = false;
			                    	}
			                    });

//			dialog.SetOnRefresh(func =>
//			                    {
//			                    	if (!string.IsNullOrEmpty(dialogType.Value))
//			                    	{
//			                    		var ndbName = settingsList.First(s => s.Name == dialogType.Value).NameDBLotus;
//			                    		var yearDB = dialogYear.Value;
//			                    		var mainDB = string.Format("CM\\{0}\\{1}{2}dat.nsf", yearDB,
//			                    		                           ndbName,
//			                    		                           yearDB.Substring(2));
//			                    		var origDB = string.Format("CM\\{0}\\{1}{2}img.nsf", yearDB,
//			                    		                           ndbName,
//			                    		                           yearDB.Substring(2));
//			                    		if (string.IsNullOrEmpty(dialogMain.Value))
//			                    			dialogMain.Value = mainDB;
//			                    		if (string.IsNullOrEmpty(dialogOrig.Value))
//			                    			dialogOrig.Value = origDB;
//			                    	}
//			                    });
			
			// Отобразить диалог и проверить, что нажата кнопка «ОК».
			if (dialog.Show() == DialogButtons.Ok)
			{
				var pServer = string.Empty;
				var ndbPath = string.Empty;
				var ndbOrigPath = string.Empty;
				
				string datetimeNow = Calendar.Now.ToString("yyyy.MM.dd HH.mm.ss");
				if (!string.IsNullOrEmpty(dialogMyServer.Value))
					pServer = dialogMyServer.Value;
				else
					pServer = dialogServer.Value;
				string ndbOrgStructPath = dialogOrgStruct.Value;
				string ndbDominoPath = dialogDomino.Value;
				if (!string.IsNullOrEmpty(dialogMyDB.Value))
					ndbPath = dialogMyDB.Value;
				else
					ndbPath = dialogPrefixDB.Value + dialogMain.Value;
				if (!string.IsNullOrEmpty(dialogMyOrigDB.Value))
					ndbOrigPath = dialogMyOrigDB.Value;
				else
					ndbOrigPath = dialogPrefixDB.Value + dialogOrig.Value;
				
				string year = dialogYear.Value;
				int maxCountDocument = (int) dialogCountDoc.Value;

//				Structures.Module.DocumentTypeSettings settings = Structures.Module.DocumentTypeSettings.Create();

				string nameServerFix = Regex.Replace(pServer, "[\\W]", "_");
				
				string tempFolder = string.Format(@"C:\DRX\DocsCM\{0}\{1} ({2})\",
				                                  nameServerFix,
				                                  dialogType.Value,
				                                  dialogYear.Value);
				
				string logsFolder = string.Format(@"C:\DRX\logs\{0}\",
				                                  nameServerFix,
				                                  dialogType.Value,
				                                  dialogYear.Value);
				
				if (!Directory.Exists(tempFolder))
					Directory.CreateDirectory(tempFolder);

				if (!Directory.Exists(logsFolder))
					Directory.CreateDirectory(logsFolder);
				
				string docType = dialogType.Value;
				IEmployee defaultEmployee = dialogDefaultEmployee.Value;
				string ndbName = settingsList.FirstOrDefault(s => s.Name == docType).NameDBLotus;
				string documentsView = settingsList.FirstOrDefault(s => s.Name == docType).DocumentsView;
				
				string nameDocFormat = datetimeNow;
				
				List<IRecipient> listRecipients = new List<IRecipient>();
				
				var regGroup = Functions.Module.Remote.GetRegGroupByName(dialogRegGroup.Value);
				if (regGroup != null)
					listRecipients = regGroup.RecipientLinks.Select(s => s.Member).ToList();
				
				var countAttempts = dialogCountAttempts.Value != null ? (int) dialogCountAttempts.Value : 5;
				var timeAttempts = dialogTimeAttempts.Value != null ? (int) dialogTimeAttempts.Value : 1000;
				
				var isServer = (bool) dialogIsServer.Value;
				var isExcel = (bool) dialogIsExcel.Value;
				var isRewriteExcel = (bool) dialogIsRewriteExcel.Value;
				var isUpdateInfo = (bool) dialogIsUpdateInfo.Value; //не запрашивать повторно данные с сервера;
				
				var excelPath = @"C:\DRX\Migration.xlsx";
				
				if (dialogIsRewriteExcel.IsEnabled && isRewriteExcel)
				{
					ExcelPreparation(docType, documentsView, year, defaultEmployee, nameDocFormat, tempFolder, logsFolder,
					                 pServer, ndbPath, ndbOrigPath, ndbOrgStructPath, ndbDominoPath,
					                 datetimeNow, listRecipients,
					                 maxCountDocument, isServer, excelPath, countAttempts, timeAttempts);
				}
				
				if (isExcel)
				{
					ExcelLoadDocuments(docType, documentsView, year, defaultEmployee, nameDocFormat, tempFolder, logsFolder,
					                   pServer, ndbPath, ndbOrigPath, ndbOrgStructPath, ndbDominoPath,
					                   datetimeNow, listRecipients,
					                   maxCountDocument, isServer, isExcel, isUpdateInfo, excelPath, countAttempts, timeAttempts);
				}
				else
				{
					AllLoadDocuments(docType, documentsView, year, defaultEmployee, nameDocFormat, tempFolder, logsFolder,
					                 pServer, ndbPath, ndbOrigPath, ndbOrgStructPath, ndbDominoPath,
					                 datetimeNow, listRecipients,
					                 maxCountDocument, isServer, isExcel, excelPath, countAttempts, timeAttempts);
				}
			}
		}

		/// <summary>
		/// Загрузка группы документов.
		/// </summary>
		public virtual void AllLoadDocuments(string docType, string documentsView, string year, IEmployee defaultEmployee, string nameDocFormat, string tempFolder, string logsFolder,
		                                     string pServer, string ndbPath, string ndbOrigPath, string ndbOrgStructPath, string ndbDominoPath,
		                                     string datetimeNow, List<IRecipient> listRecipients,
		                                     int maxCountDocument, bool isServer, bool isExcel, string excelPath, int countAttempts = 5, int timeAttempts = 1000,
		                                     int cntAll = 0, int cntLoad = 0, int cntError = 0, int cntWithoutAttach = 0)
		{
			NotesSessionClass notesSession = new NotesSessionClass();
			notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
			NotesDatabase ndbDocuments = notesSession.GetDatabase(pServer, ndbPath);
			NotesDatabase ndbOrigDocuments = notesSession.GetDatabase(pServer, ndbOrigPath);
			NotesDatabase ndbOrgStructure = notesSession.GetDatabase(pServer, ndbOrgStructPath);
			NotesDatabase ndbDominoDirectory = notesSession.GetDatabase(pServer, ndbDominoPath);
			
			if (!ndbDocuments.IsOpen)
				ndbDocuments.Open();
			if (!ndbOrigDocuments.IsOpen)
			{
				try
				{
					ndbOrigDocuments.Open();
				}
				catch (System.Runtime.InteropServices.COMException)
				{
					ndbOrigDocuments = ndbDocuments;
				}
			}
			if (!ndbOrgStructure.IsOpen)
				ndbOrgStructure.Open();
			if (!ndbDominoDirectory.IsOpen)
				ndbDominoDirectory.Open();

			NotesView notesView = ndbDocuments.GetView(documentsView);
			NotesDocument document = null;
			NotesDocumentCollection documents = null;
			int allDocumentCount = 0;
			
			if (docType.Equals("Входящее письмо"))
			{
				allDocumentCount = notesView.EntryCount;
				document = notesView.GetFirstDocument();
			}
			else
			{
				documents = notesView.GetAllDocumentsByKey(docType);
				allDocumentCount = documents.Count;
				document = documents.GetFirstDocument();
			}
			
			maxCountDocument = maxCountDocument > 0 ? maxCountDocument : allDocumentCount;
			
			var progressBar = new ProgressBarDRX(maxCountDocument); //объект прогресс бар
			progressBar.Show();
//				progressBar.Text("Подключение к базе...");
			
			try
			{
				System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
				sw.Start();
				
				progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
				
				while (document != null && ((maxCountDocument <= 0 && cntLoad < allDocumentCount && cntError < allDocumentCount) || (maxCountDocument > 0 && cntLoad < maxCountDocument && cntError < maxCountDocument)))
				{
					string noteID = ((object[])document.GetItemValue("DocID"))[0].ToString();
					string noteAttachID = string.Empty;
					string noteAttachReplicaID = string.Empty;
					string path = string.Empty;
					string link = string.Format("notes://{0}/{1}/0/{2}",
					                            pServer.Split('/')[0],
					                            ndbDocuments.ReplicaID,
					                            noteID);
					var nameDocumentList = new List<string>();
					
					cntAll++;
					
					try
					{
						var columnNamesBase = new List<string>()
						{
							"Orig_ID", "Orig_ReplicaId", "Orig_ID_F", "Orig_ReplicaId_F"
						};
						
						var baseValues = GetValueRequisites(document, 1000, columnNamesBase);
						
						noteAttachID = !string.IsNullOrEmpty(baseValues["Orig_ID"]) ? baseValues["Orig_ID"] :
							!string.IsNullOrEmpty(baseValues["Orig_ID_F"]) ? baseValues["Orig_ID_F"] :
							string.Empty;
						
						noteAttachReplicaID = !string.IsNullOrEmpty(baseValues["Orig_ReplicaId"]) ? baseValues["Orig_ReplicaId"] :
							!string.IsNullOrEmpty(baseValues["Orig_ReplicaId_F"]) ? baseValues["Orig_ReplicaId_F"] :
							string.Empty;

//						NotesDocument documentWithAttach = !string.IsNullOrEmpty(noteAttachID) ? ndbOrigDocuments.GetDocumentByUNID(noteAttachID) : null;
						
						NotesDocument documentWithAttach = document;
						nameDocumentList = GetNameAttachmentList(documentWithAttach);
						
						if (!string.IsNullOrEmpty(noteAttachID) && nameDocumentList.Count == 0)
						{
							documentWithAttach = ndbOrigDocuments.GetDocumentByUNID(noteAttachID);
							nameDocumentList = GetNameAttachmentList(documentWithAttach);
						}
						
						if (!string.IsNullOrEmpty(noteAttachReplicaID) && nameDocumentList.Count == 0)
						{
							documentWithAttach = ndbOrigDocuments.GetDocumentByUNID(noteAttachReplicaID);
							nameDocumentList = GetNameAttachmentList(documentWithAttach);
						}
						
						if (nameDocumentList.Count > 0 && documentWithAttach != null)
						{
//							path = string.Format("{0}LOTUS {1} {2} (ID {3}){4}",
//							                     tempFolder,
//							                     datetimeNow,
//							                     docType,
//							                     noteID,
//							                     System.IO.Path.GetExtension(nameDocumentList[0]));
							
							path = string.Format("{0}CM {1} {2} {6} ({5})[ID {3}]{4}",
							                     tempFolder,
							                     datetimeNow,
							                     docType,
							                     noteID,
							                     System.IO.Path.GetExtension(nameDocumentList[0]),
							                     System.IO.Path.GetFileNameWithoutExtension(nameDocumentList[0]),
							                     (1).ToString());
							
							documentWithAttach.GetAttachment(nameDocumentList[0]).ExtractFile(path);
						}
						else
						{
							cntWithoutAttach++;
							
							using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
							{
								file.WriteLine(string.Format("{5}. Нет вложенного документа: {0} (AttachID {1}; ReplicaID {2}) [DataBases {3} and {4}]", noteID, noteAttachID, noteAttachReplicaID, ndbPath, ndbOrigPath, cntAll));
							}
						}
						
						var columnNamesLetter = new List<string>()
						{
							"Subject", "DocToolTip", "From", "OutDate", "OutNumber", "Links_DocName",
							"Userfrom", "Mast", "strorg", "Registration", "To", "RNumber_Full",
							"RDate", "Stamp", "Inventory_BelongToFileType", "Inventory_DateOfAddingToFile",
							"DocID"
						};
						
						var columnNamesOrder = new List<string>()
						{
							"Subject", "strorg", "Registration", "Userfrom", "Mast", "RNumber_Full", "OutDate",
							"DocID"
						};
						
						int? docID = null;
						string entityType = "";
						var lotusValues = new Dictionary<string, string>();

						if (docType.Equals("Входящее письмо"))
						{
							lotusValues = GetValueRequisites(document, 1000, columnNamesLetter);
							
							var externalEntityLink = Functions.Module.Remote.GetExEntityLinkByExID(noteID);
							
							IIncomingLetter incomingLetter = externalEntityLink != null ? Functions.Module.Remote.GetIncomingLettersByID((int)externalEntityLink.EntityId) : null;
							
							if (incomingLetter == null)
								incomingLetter = !string.IsNullOrEmpty(path) ? IncomingLetters.CreateFrom(path) : Functions.Module.Remote.CreateLetter();
							else
								incomingLetter.Versions.First().Import(path);
							
							var subject = string.Format("{0} (№ {1} [{3}] от {2} [{4}])",
							                            lotusValues["Subject"],
							                            lotusValues["RNumber_Full"],
							                            lotusValues["RDate"],
							                            lotusValues["OutNumber"],
							                            lotusValues["OutDate"]);
							
							incomingLetter.Subject = subject.Substring(0, subject.Length > 250 ? 250 : subject.Length);
							
							var note = string.Format("{0}{1} ({2}) [{3}]\r\n{4}",
							                         nameDocFormat,
							                         !string.IsNullOrEmpty(lotusValues["RNumber_Full"]) ? lotusValues["RNumber_Full"] : (cntLoad + 1).ToString(),
							                         nameDocumentList.Count > 0 ? nameDocumentList[0] : "нет вложения",
							                         Hyperlinks.Functions.GetLinkExEntity(link),
							                         lotusValues["ResultText"]);

							incomingLetter.Note = note.Substring(0, note.Length > 1000 ? 1000 : note.Length);
							
							incomingLetter.Author = defaultEmployee;
							incomingLetter.Correspondent = Functions.Module.Remote.GetCounterparty();
							incomingLetter.Department = defaultEmployee.Department;
							incomingLetter.Save();
							
							docID = incomingLetter.Id;
							entityType = "aabe3f4a-d52b-41b4-9e45-8a62a0d8ad13";
						}
						else
						{
							lotusValues = GetValueRequisites(document, 1000, columnNamesOrder);
							
							var mastFirst = lotusValues["Mast"].Split(';')[0];
							var userfromFirst = lotusValues["Userfrom"].Split(';')[0];
							
							var emailMast = GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, mastFirst);
							var emailUserFrom = GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, userfromFirst);
							
							var employeeMast = Functions.Module.Remote.GetEmployeeByEmail(emailMast);
							var employeeUserFrom = Functions.Module.Remote.GetEmployeeByEmail(emailUserFrom);
							
							var subject = string.Format("{0} (№ {1} от {2})",
							                            lotusValues["Subject"],
							                            lotusValues["RNumber_Full"],
							                            lotusValues["OutDate"]);
							var note = string.Format("{0} № {1} от {2} CM [{3}] [{5}]\r\n{6}\r\n{7}",
							                         docType,
							                         lotusValues["RNumber_Full"],
							                         lotusValues["OutDate"],
							                         pServer,
							                         Path.GetFileName(path),
							                         Hyperlinks.Functions.GetLinkExEntity(link),
							                         lotusValues["ResultText"],
							                         nameDocFormat);
//							var documentKind = Functions.Module.Remote.GetDocumentKind(docType);
							var logFileName = string.Format(logsFolder + "log_migration_{0}({1})_{2}", docType, year, datetimeNow);
							var errorMessageDefault = string.Format("{5}. Первая версия не доступна для перезаписи вложения: {0} (AttachID {1}; ReplicaID {2}) [DataBases {3} and {4}]", noteID, noteAttachID, noteAttachReplicaID, ndbPath, ndbOrigPath, cntAll);
							
							var externalEntityLink = Functions.Module.Remote.GetExEntityLinkByExID(noteID);
							if (isServer)
							{
								IOrder order = Functions.Module.Remote.Sync_Order_Lotus(path, externalEntityLink, docType, subject, note, defaultEmployee, employeeMast, employeeUserFrom, listRecipients, countAttempts, timeAttempts, logFileName, errorMessageDefault);
								
								docID = order.Id;
								order = null;
							}
							else
							{
								IOrder order = externalEntityLink != null? Functions.Module.Remote.GetOrdersByID((int)externalEntityLink.EntityId) : null;
								
								if (order == null)
									order = !string.IsNullOrEmpty(path) ? Orders.CreateFrom(path) : Orders.Create();
								else
								{
									if (!string.IsNullOrEmpty(path))
									{
										if (order.HasVersions)
										{
											try
											{
												try
												{
													Locks.Unlock(order);
												}
												catch
												{
												}

												try
												{
													Locks.Unlock(order.Versions.First());
												}
												catch
												{
												}

												try
												{
													Locks.Unlock(order.Versions.First().Body);
												}
												catch
												{
												}
												//							throw new System.MemberAccessException("Ошибка сохранения документа");
												order.Versions.First().Import(path);
											}
											catch
											{
												var lockInfo = Locks.GetLockInfo(order.Versions.First().Body);
												using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
												{
													file.WriteLine(string.Format("{0}[ID RX {1}] - {2}: {3}", errorMessageDefault, order.Id, lockInfo.OwnerName, lockInfo.LockedMessage));
												}
											}
										}
										else
										{
											try
											{
												order.CreateVersionFrom(path);
											}
											catch
											{
												using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
												{
													file.WriteLine(string.Format("Ошибка при создании новой версии {0} ({1})[ID RX {2}]", subject, path, order.Id));
												}
											}
										}
									}
								}
								
								order.DocumentKind = Functions.Module.Remote.GetDocumentKind(docType);
								
								order.Subject = subject.Substring(0, subject.Length > 250 ? 250 : subject.Length);
								
								order.Note = note.Substring(0, note.Length > 1000 ? 1000 : note.Length);
								
								order.Author = employeeMast != null ? employeeMast : defaultEmployee;
								order.PreparedBy = employeeMast != null ? employeeMast : defaultEmployee;
								order.OurSignatory = employeeUserFrom;
								order.Department = (employeeMast != null) ? employeeMast.Department : defaultEmployee.Department;
								
								try
								{
									foreach (var recipient in listRecipients)
									{
										if (!order.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, recipient))
										{
											order.AccessRights.Grant(recipient, DefaultAccessRightsTypes.FullAccess);
										}
									}
								}
								catch
								{
									using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
									{
										file.WriteLine(string.Format("Ошибка при выдаче прав на докумен {0} ({1})[ID RX {2}]", subject, path, order.Id));
									}
								}
								
								var exceptionCount = 0;
								var saved = false;
								
								while (!saved && exceptionCount < countAttempts)
								{
									try
									{
										if (exceptionCount > 0)
											System.Threading.Thread.Sleep(timeAttempts);
										
										order.Save();
										
										exceptionCount++;
										
										using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
										{
											file.WriteLine(string.Format("[ID RX {3}] Попытка сохранения № {0} ({1})[{2}]", exceptionCount.ToString(), subject, path, order.Id));
										}
										
										exceptionCount = countAttempts;
										saved = true;
									}
									catch
									{
										exceptionCount++;
										
										using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
										{
											file.WriteLine(string.Format("Попытка сохранения № {0} ({1})[{2}]", exceptionCount.ToString(), subject, path));
										}
									}
									
									
									docID = order.Id;
									order = null;
								}
							}
							entityType = "cbc14e48-3ebb-4105-b1c6-8c86b0fdb773";
							
							Functions.Module.Remote.CreateExEntityLink(entityType,
							                                           docID,
							                                           link,
							                                           noteID);
							
							cntLoad++;
						}
					}
					catch (Exception ex)
					{
						cntError++;
						var message = ex.Message;
						using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
						{
							file.WriteLine(string.Format("{6}. Документ ID: {0} (AttachID {1}; ReplicaID {2}) [DataBases {3} and {4}]\r\n{5}", noteID, noteAttachID, noteAttachReplicaID, ndbPath, ndbOrigPath, message, cntAll));
						}
					}
					
					if (docType.Equals("Входящее письмо"))
						document = notesView.GetNextDocument(document);
					else
						document = documents.GetNextDocument(document);
					
					progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
					progressBar.Next();
				}
				progressBar.Hide();
				progressBar = null;
				
				sw.Stop();
				string resultMessage = string.Format("Загрузка завершена.\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество загруженных документов: {0}\r\n"
				                                     + "Количество документов без вложений: {1}\r\n"
				                                     + "Количество ошибок при загрузке: {2}\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество просмотренных документов: {3}\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Время выполнения: {4}",
				                                     cntLoad, cntWithoutAttach, cntError, cntAll, sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
				
				Dialogs.ShowMessage(resultMessage, MessageType.Information);
				
				if (docType.Equals("Входящее письмо"))
					Functions.Module.Remote.GetIncomingLettersByStrTemplate(nameDocFormat).ShowModal();
				else
					Functions.Module.Remote.GetOrdersByStrTemplate(nameDocFormat).ShowModal();

				sw = null;
			}
			catch (Exception ex)
			{
				Dialogs.ShowMessage("ОШИБКА.\r\n-------------------\r\n"
				                    + ex.Message +
				                    "\r\n-------------------\r\n" + ex.ToString(), MessageType.Information);
				if (progressBar != null)
				{
					progressBar.Hide();
					progressBar = null;
				}
			}
			
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
		}
		
		/// <summary>
		/// Загрузка группы документов.
		/// </summary>
		public virtual void ExcelLoadDocuments(string docType, string documentsView, string year, IEmployee defaultEmployee, string nameDocFormat, string tempFolder, string logsFolder,
		                                       string pServer, string ndbPath, string ndbOrigPath, string ndbOrgStructPath, string ndbDominoPath,
		                                       string datetimeNow, List<IRecipient> listRecipients,
		                                       int maxCountDocument, bool isServer, bool isExcel, bool isUpdateInfo, string excelPath, int countAttempts = 5, int timeAttempts = 1000,
		                                       int cntAll = 0, int cntLoad = 0, int cntError = 0, int cntWithoutAttach = 0)
		{
			if (File.Exists(excelPath))
			{
				//Объявляем приложение
				Excel.Application ex = new Excel.Application();
				
				ex.Workbooks.Open(excelPath,
				                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				                  Type.Missing, Type.Missing);
				
				Excel.Workbook workBook = ex.Workbooks.get_Item(1);
				//Отобразить Excel
				ex.Visible = false;
				//Отключить отображение окон с сообщениями
				ex.DisplayAlerts = false;
				var strServer = Regex.Replace(pServer, "[\\W]", "_") + "_" + year + "_" + docType;
				//Получаем первый лист документа (счет начинается с 1)
				Excel.Worksheet sheet = (Excel.Worksheet)ex.Worksheets.get_Item(1);
				
				try
				{
					sheet = (Excel.Worksheet)ex.Worksheets.get_Item(strServer);
				}
				catch
				{
					workBook.Close();
					ex.Quit();
					
					sheet = null;
					workBook = null;
					ex = null;
					
					GC.Collect();
					GC.WaitForPendingFinalizers();
					GC.Collect();
					
					ExcelPreparation(docType, documentsView, year, defaultEmployee, nameDocFormat, tempFolder, logsFolder,
					                 pServer, ndbPath, ndbOrigPath, ndbOrgStructPath, ndbDominoPath,
					                 datetimeNow, listRecipients,
					                 maxCountDocument, isServer, excelPath, countAttempts, timeAttempts);
					
					//Объявляем приложение
					ex = new Excel.Application();
					
					ex.Workbooks.Open(excelPath,
					                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
					                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
					                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
					                  Type.Missing, Type.Missing);
					
					workBook = ex.Workbooks.get_Item(1);
					//Отобразить Excel
					ex.Visible = false;
					//Отключить отображение окон с сообщениями
					ex.DisplayAlerts = false;
					//Получаем первый лист документа (счет начинается с 1)
					sheet = (Excel.Worksheet)ex.Worksheets.get_Item(strServer);
				}
				
				System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
				sw.Start();
				
				try
				{
					var rowIndex = 1;
					var columnIndex = 0;
					int allDocumentCount = 0;

					while (((Excel.Range)sheet.Cells[++rowIndex, 14]).Value2 != null)
					{
						if (((Excel.Range)sheet.Cells[rowIndex, 14]).Value2.ToString() == "Загружен")
							continue;
						
						allDocumentCount++;
					}
					
//				if (isUpdateInfo)
//				{
					NotesSessionClass notesSession = new NotesSessionClass();
					notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
					NotesDatabase ndbDocuments = notesSession.GetDatabase(pServer, ndbPath);
					NotesDatabase ndbOrigDocuments = notesSession.GetDatabase(pServer, ndbOrigPath);
					NotesDatabase ndbOrgStructure = notesSession.GetDatabase(pServer, ndbOrgStructPath);
					NotesDatabase ndbDominoDirectory = notesSession.GetDatabase(pServer, ndbDominoPath);
					
					if (!ndbDocuments.IsOpen)
						ndbDocuments.Open();
					if (!ndbOrigDocuments.IsOpen)
					{
						try
						{
							ndbOrigDocuments.Open();
						}
						catch (System.Runtime.InteropServices.COMException)
						{
							ndbOrigDocuments = ndbDocuments;
						}
					}
					if (!ndbOrgStructure.IsOpen)
						ndbOrgStructure.Open();
					if (!ndbDominoDirectory.IsOpen)
						ndbDominoDirectory.Open();

//					NotesView notesView = ndbDocuments.GetView(documentsView);
//				NotesDocument document = null;
//				NotesDocumentCollection documents = null;
//					allDocumentCount = 0;
//
//					if (docType.Equals("Входящее письмо"))
//					{
//						allDocumentCount = notesView.EntryCount;
//					document = notesView.GetFirstDocument();
//					}
//					else
//					{
//						documents = notesView.GetAllDocumentsByKey(docType);
//						allDocumentCount = documents.Count;
//					document = documents.GetFirstDocument();
//					}
//				}
					
					maxCountDocument = maxCountDocument > 0 ? maxCountDocument : allDocumentCount;
					
					var progressBar = new ProgressBarDRX(maxCountDocument); //объект прогресс бар
					progressBar.Show();
					
					progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
					
					var columnsList = new List<string>();
					
					var lotusValues = new Dictionary<string, string>();
					
					rowIndex = 1;
					columnIndex = 0;
					
					while (((Excel.Range)sheet.Cells[1, ++columnIndex]).Value2 != null)
					{
						lotusValues.Add(((Excel.Range)sheet.Cells[1, columnIndex]).Value2.ToString(), string.Empty);
					}
					
					var columnNames = new List<string>()
					{
						"DocID", "Subject", "strorg", "Registration", "Userfrom", "Mast", "RNumber_Full", "OutDate",
						"Orig_ID", "Orig_ReplicaId", "Orig_ID_F", "Orig_ReplicaId_F"
					};
					
					while (((Excel.Range)sheet.Cells[++rowIndex, 14]).Value2 != null)
					{
						if (((Excel.Range)sheet.Cells[rowIndex, 14]).Value2.ToString() == "Загружен")
							continue;
						
						cntAll++;
						
						columnIndex = 0;
						
						NotesDocument document = null;
						
						if (isUpdateInfo)
						{
							document = ndbDocuments.GetDocumentByUNID(((Excel.Range)sheet.Cells[rowIndex, 1]).Value2.ToString());
							lotusValues.Clear();
							lotusValues = GetValueRequisites(document, 1000, columnNames);
						}
						else
						{
							
							foreach (var entry in lotusValues.Keys.ToList())
							{
								var val = ((Excel.Range)sheet.Cells[rowIndex, ++columnIndex]).Value2;
								lotusValues[entry] = val != null ? val.ToString() : string.Empty;
							}
						}
						
						string noteID = lotusValues["DocID"];
						string noteAttachID = !string.IsNullOrEmpty(lotusValues["Orig_ID"]) ? lotusValues["Orig_ID"] :
							!string.IsNullOrEmpty(lotusValues["Orig_ID_F"]) ? lotusValues["Orig_ID_F"] :
							string.Empty;
						
						string noteAttachReplicaID = !string.IsNullOrEmpty(lotusValues["Orig_ReplicaId"]) ? lotusValues["Orig_ReplicaId"] :
							!string.IsNullOrEmpty(lotusValues["Orig_ReplicaId_F"]) ? lotusValues["Orig_ReplicaId_F"] :
							string.Empty;
						string path = string.Empty;
						string link = string.Format("notes://{0}/{1}/0/{2}",
						                            pServer.Split('/')[0],
						                            ndbDocuments.ReplicaID,
						                            noteID);
						int docID = 0;
						string entityType = string.Empty;
						
						var nameDocumentList = new List<string>();
						
						if (isUpdateInfo)
						{
							columnIndex = 0;
							
							foreach (var lotusValue in lotusValues)
							{
								sheet.Cells[rowIndex, ++columnIndex] = lotusValue.Value;
							}
							
							columnIndex = columnIndex + 2;
							
							NotesDocument documentWithAttach = document;
							nameDocumentList = GetNameAttachmentList(documentWithAttach);
							
							if (!string.IsNullOrEmpty(noteAttachID) && nameDocumentList.Count == 0)
							{
								documentWithAttach = ndbOrigDocuments.GetDocumentByUNID(noteAttachID);
								nameDocumentList = GetNameAttachmentList(documentWithAttach);
							}
							
							if (!string.IsNullOrEmpty(noteAttachReplicaID) && nameDocumentList.Count == 0)
							{
								documentWithAttach = ndbOrigDocuments.GetDocumentByUNID(noteAttachReplicaID);
								nameDocumentList = GetNameAttachmentList(documentWithAttach);
							}
							
							if (nameDocumentList.Count > 0 && documentWithAttach != null)
							{
//								foreach (var nameDocument in nameDocumentList)
//								{
//									path = string.Format("{0}LOTUS {1} {2} ({5})[ID {3}]{4}",
//									                     tempFolder,
//									                     datetimeNow,
//									                     docType,
//									                     lotusValues["DocID"],
//									                     System.IO.Path.GetExtension(nameDocument),
//									                     System.IO.Path.GetFileNameWithoutExtension(nameDocument));
//
//									documentWithAttach.GetAttachment(nameDocument).ExtractFile(path);
//
//									sheet.Cells[rowIndex, ++columnIndex] = path;
//									nameDocument = path;
//								}
								for (var i = 0; i < nameDocumentList.Count; i++)
								{
									path = string.Format("{0}CM {1} {2} {6} ({5})[ID {3}]{4}",
									                     tempFolder,
									                     datetimeNow,
									                     docType,
									                     lotusValues["DocID"],
									                     System.IO.Path.GetExtension(nameDocumentList[i]),
									                     System.IO.Path.GetFileNameWithoutExtension(nameDocumentList[i]),
									                     i.ToString());

									documentWithAttach.GetAttachment(nameDocumentList[i]).ExtractFile(path);
									
									sheet.Cells[rowIndex, ++columnIndex] = path;
									nameDocumentList[i] = path;
								}
							}
							else
							{
								cntWithoutAttach++;
								
								using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
								{
									file.WriteLine(string.Format("{5}. Нет вложенного документа: {0} (AttachID {1}; ReplicaID {2}) [DataBases {3} and {4}]", noteID, noteAttachID, noteAttachReplicaID, ndbPath, ndbOrigPath, cntAll));
								}
							}
						}
						else
						{
							nameDocumentList.Add(lotusValues["Path"]);
							
							while (((Excel.Range)sheet.Cells[rowIndex, ++columnIndex]).Value2 != null)
							{
								nameDocumentList.Add(((Excel.Range)sheet.Cells[rowIndex, columnIndex]).Value2.ToString());
							}
						}
						
						var docIndex = 0;
						do
						{
							path = nameDocumentList[docIndex++];
							var b = Path.GetExtension(path);
						} while (docIndex < nameDocumentList.Count && !string.Equals(Path.GetExtension(path), ".pdf", StringComparison.OrdinalIgnoreCase));
						
						try
						{
							if (docType.Equals("Входящее письмо"))
							{
//								"DocID", "Subject", "strorg", "Registration", "Userfrom", "Mast", "RNumber_Full", "OutDate",
//								"Orig_ID", "Orig_ReplicaId", "Orig_ID_F", "Orig_ReplicaId_F"
							}
							else
							{
								var mastFirst = lotusValues["Mast"].Split(';')[0];
								var userfromFirst = lotusValues["Userfrom"].Split(';')[0];
								
								var emailMast = GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, mastFirst);
								var emailUserFrom = GetEmailFromFIO(ndbOrgStructure, ndbDominoDirectory, userfromFirst);
								
								var employeeMast = Functions.Module.Remote.GetEmployeeByEmail(emailMast);
								var employeeUserFrom = Functions.Module.Remote.GetEmployeeByEmail(emailUserFrom);
								
								var subject = string.Format("{0} (№ {1} от {2})",
								                            lotusValues["Subject"],
								                            lotusValues["RNumber_Full"],
								                            lotusValues["OutDate"]);
								
								var note = string.Format("{0} № {1} от {2} CM [{3}] [{5}]\r\n{6}\r\n{7}",
								                         docType,
								                         lotusValues["RNumber_Full"],
								                         lotusValues["OutDate"],
								                         pServer,
								                         Path.GetFileName(path),
								                         Hyperlinks.Functions.GetLinkExEntity(link),
								                         lotusValues["ResultText"],
								                         nameDocFormat);
//							var documentKind = Functions.Module.Remote.GetDocumentKind(docType);
								var logFileName = string.Format(logsFolder + "log_migration_{0}({1})_{2}", docType, year, datetimeNow);
								var errorMessageDefault = string.Format("{5}. Первая версия не доступна для перезаписи вложения: {0} (AttachID {1}; ReplicaID {2}) [DataBases {3} and {4}]", noteID, noteAttachID, noteAttachReplicaID, ndbPath, ndbOrigPath, cntAll);
								
								var externalEntityLink = Functions.Module.Remote.GetExEntityLinkByExID(noteID);
								
								var isWarning = false;
								
								if (isServer)
								{
									IOrder order = Functions.Module.Remote.Sync_Order_Lotus(path, externalEntityLink, docType, subject, note, defaultEmployee, employeeMast, employeeUserFrom, listRecipients, countAttempts, timeAttempts, logFileName, errorMessageDefault);
									
									docID = order.Id;
									order = null;
								}
								else
								{
									IOrder order = externalEntityLink != null? Functions.Module.Remote.GetOrdersByID((int)externalEntityLink.EntityId) : null;
									
									if (order == null)
										order = !string.IsNullOrEmpty(path) ? Orders.CreateFrom(path) : Orders.Create();
									else
									{
										if (!string.IsNullOrEmpty(path))
										{
											if (order.HasVersions)
											{
												try
												{
//													try
//													{
//														Locks.Unlock(order);
//													}
//													catch
//													{
//													}
//
//													try
//													{
//														Locks.Unlock(order.Versions.First());
//													}
//													catch
//													{
//													}
//
//													try
//													{
//														Locks.Unlock(order.Versions.First().Body);
//													}
//													catch
//													{
//													}
													
//													throw new System.MemberAccessException("Ошибка сохранения документа");
													
													order.Versions.First().Import(path);
												}
												catch
												{
													var lockInfo = Locks.GetLockInfo(order.Versions.First().Body);
													using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
													{
														file.WriteLine(string.Format("{0}[ID RX {1}] - {2}: {3}", errorMessageDefault, order.Id, lockInfo.OwnerName, lockInfo.LockedMessage));
													}
													
													sheet.Cells[rowIndex, 15] = "Ошибка импорта в первую версию документа";
													isWarning = true;
												}
											}
											else
											{
												try
												{
													order.CreateVersionFrom(path);
												}
												catch
												{
													using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
													{
														file.WriteLine(string.Format("Ошибка при создании новой версии {0} ({1})[ID RX {2}]", subject, path, order.Id));
													}
													
													sheet.Cells[rowIndex, 15] = "Ошибка при создании новой версии";
													isWarning = true;
												}
											}
										}
									}
									
									order.DocumentKind = Functions.Module.Remote.GetDocumentKind(docType);
									order.Subject = subject.Substring(0, subject.Length > 250 ? 250 : subject.Length);
									order.Note = note.Substring(0, note.Length > 1000 ? 1000 : note.Length);
									order.Author = employeeMast != null ? employeeMast : defaultEmployee;
									order.PreparedBy = employeeMast != null ? employeeMast : defaultEmployee;
									order.OurSignatory = employeeUserFrom;
									order.Department = (employeeMast != null) ? employeeMast.Department : defaultEmployee.Department;
									
									try
									{
										foreach (var recipient in listRecipients)
										{
											if (!order.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, recipient))
											{
												order.AccessRights.Grant(recipient, DefaultAccessRightsTypes.FullAccess);
											}
										}
									}
									catch
									{
										using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
										{
											file.WriteLine(string.Format("Ошибка при выдаче прав на докумен {0} ({1})[ID RX {2}]", subject, path, order.Id));
										}
										
										sheet.Cells[rowIndex, 15] = "Ошибка при выдаче прав";
										isWarning = true;
									}
									
									var exceptionCount = 0;
									var isSaved = false;
									
									while (!isSaved && exceptionCount < countAttempts)
									{
										try
										{
											if (exceptionCount > 0)
												System.Threading.Thread.Sleep(timeAttempts);
											
											order.Save();
											
											exceptionCount++;
											
											using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
											{
												file.WriteLine(string.Format("[ID RX {3}] Попытка сохранения № {0} ({1})[{2}]", exceptionCount.ToString(), subject, path, order.Id));
											}
											
											exceptionCount = countAttempts;
											isSaved = true;
										}
										catch
										{
											exceptionCount++;
											
											using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
											{
												file.WriteLine(string.Format("Попытка сохранения № {0} ({1})[{2}]", exceptionCount.ToString(), subject, path));
											}
										}
									}
									
									if (isSaved)
									{
										docID = order.Id;
										order = null;
										
										if (isWarning)
											sheet.Cells[rowIndex, 14] = "Загружен с ошибками";
										else
										{
											sheet.Cells[rowIndex, 14] = "Загружен";
											sheet.Cells[rowIndex, 15] = "";
										}
									}
									else
									{
										sheet.Cells[rowIndex, 14] = "Не загружен";
									}
								}
								
								entityType = "cbc14e48-3ebb-4105-b1c6-8c86b0fdb773";
								
								Functions.Module.Remote.CreateExEntityLink(entityType,
								                                           docID,
								                                           link,
								                                           noteID);
								
								cntLoad++;
							}
						}
						catch (Exception exc)
						{
							cntError++;
							var message = exc.Message;
							using (var file = File.AppendText(string.Format(logsFolder + "log_migration_{0}({1})_{2}.txt", docType, year, datetimeNow)))
							{
								file.WriteLine(string.Format("{6}. Документ ID: {0} (AttachID {1}; ReplicaID {2}) [DataBases {3} and {4}]\r\n{5}", noteID, noteAttachID, noteAttachReplicaID, ndbPath, ndbOrigPath, message, cntAll));
							}
						}
						
//						if (docType.Equals("Входящее письмо"))
//							document = notesView.GetNextDocument(document);
//						else
//							document = documents.GetNextDocument(document);
						
						progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
						progressBar.Next();
					}
					
					progressBar.Hide();
					progressBar = null;
					
					sw.Stop();
				}
				finally
				{
					ex.Application.ActiveWorkbook.Save();
//					ex.Application.ActiveWorkbook.Close();
					workBook.Close();
					ex.Quit();
					
					sheet = null;
					workBook = null;
					ex = null;
					
					GC.Collect();
					GC.WaitForPendingFinalizers();
					GC.Collect();
				}
				
				string resultMessage = string.Format("Загрузка завершена.\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество загруженных документов: {0}\r\n"
				                                     + "Количество документов без вложений: {1}\r\n"
				                                     + "Количество ошибок при загрузке: {2}\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Количество просмотренных документов: {3}\r\n"
				                                     + "---------------------------------------------------------\r\n"
				                                     + "Время выполнения: {4}",
				                                     cntLoad, cntWithoutAttach, cntError, cntAll, sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
				sw = null;
				Dialogs.ShowMessage(resultMessage, MessageType.Information);
				
				if (docType.Equals("Входящее письмо"))
					Functions.Module.Remote.GetIncomingLettersByStrTemplate(nameDocFormat).ShowModal();
				else
					Functions.Module.Remote.GetOrdersByStrTemplate(nameDocFormat).ShowModal();
			}
			else
			{
				Dialogs.ShowMessage("Excel документ с данными для миграции не найден: " + excelPath);
			}
		}
		
		/// <summary>
		/// Загрузка группы документов.
		/// </summary>
		public virtual void ExcelPreparation(string docType, string documentsView, string year, IEmployee defaultEmployee, string nameDocFormat, string tempFolder, string logsFolder,
		                                     string pServer, string ndbPath, string ndbOrigPath, string ndbOrgStructPath, string ndbDominoPath,
		                                     string datetimeNow, List<IRecipient> listRecipients,
		                                     int maxCountDocument, bool isServer, string excelPath, int countAttempts = 5, int timeAttempts = 1000,
		                                     int cntAll = 0, int cntLoad = 0, int cntError = 0, int cntWithoutAttach = 0)
		{
			NotesSessionClass notesSession = new NotesSessionClass();
			notesSession.Initialize("#EDC4rfv"); //Directum/burgaz
			NotesDatabase ndbDocuments = notesSession.GetDatabase(pServer, ndbPath);
			NotesDatabase ndbOrigDocuments = notesSession.GetDatabase(pServer, ndbOrigPath);
			NotesDatabase ndbOrgStructure = notesSession.GetDatabase(pServer, ndbOrgStructPath);
			NotesDatabase ndbDominoDirectory = notesSession.GetDatabase(pServer, ndbDominoPath);
			
			if (!ndbDocuments.IsOpen)
				ndbDocuments.Open();
			if (!ndbOrigDocuments.IsOpen)
			{
				try
				{
					ndbOrigDocuments.Open();
				}
				catch (System.Runtime.InteropServices.COMException)
				{
					ndbOrigDocuments = ndbDocuments;
				}
			}
			if (!ndbOrgStructure.IsOpen)
				ndbOrgStructure.Open();
			if (!ndbDominoDirectory.IsOpen)
				ndbDominoDirectory.Open();

			NotesView notesView = ndbDocuments.GetView(documentsView);
			NotesDocument document = null;
			NotesDocumentCollection documents = null;
			int allDocumentCount = 0;
			
			var columnNames = new List<string>();
			
			if (docType.Equals("Входящее письмо"))
			{
				allDocumentCount = notesView.EntryCount;
				document = notesView.GetFirstDocument();
				
				columnNames = new List<string>()
				{
					"DocID", "Subject", "DocToolTip", "From", "OutDate", "OutNumber", "Links_DocName",
					"Userfrom", "Mast", "strorg", "Registration", "To", "RNumber_Full",
					"RDate", "Stamp", "Inventory_BelongToFileType", "Inventory_DateOfAddingToFile",
					"Orig_ID", "Orig_ReplicaId", "Orig_ID_F", "Orig_ReplicaId_F"
				};
			}
			else
			{
				documents = notesView.GetAllDocumentsByKey(docType);
				allDocumentCount = documents.Count;
				document = documents.GetFirstDocument();
				
				columnNames = new List<string>()
				{
					"DocID", "Subject", "strorg", "Registration", "Userfrom", "Mast", "RNumber_Full", "OutDate",
					"Orig_ID", "Orig_ReplicaId", "Orig_ID_F", "Orig_ReplicaId_F"
				};
			}
			
			//Объявляем приложение
			Excel.Application ex = new Excel.Application();
			Excel.Workbook workBook = null;
			
			if (File.Exists(excelPath))
			{
				ex.Workbooks.Open(excelPath,
				                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				                  Type.Missing, Type.Missing, Type.Missing, Type.Missing,
				                  Type.Missing, Type.Missing);
				
				workBook = ex.Workbooks.get_Item(1);
			}
			else
			{
				//Количество листов в рабочей книге
				ex.SheetsInNewWorkbook = 1;
				//Добавить рабочую книгу
				workBook = ex.Workbooks.Add(Type.Missing);
			}
			
			//Отобразить Excel
			ex.Visible = false;

			//Отключить отображение окон с сообщениями
			ex.DisplayAlerts = false;
			
			var strServer = Regex.Replace(pServer, "[\\W]", "_") + "_" + year + "_" + docType;
			//Получаем первый лист документа (счет начинается с 1)
			
			Excel.Worksheet sheet;
			try
			{
				sheet = (Excel.Worksheet)ex.Worksheets.get_Item(strServer);
			}
			catch
			{
				sheet = (Excel.Worksheet)ex.Worksheets.Add();
				sheet.Name = strServer;
			}
			
			sheet.Cells.ClearContents();
			
			var lotusValues = new Dictionary<string, string>();
			
			var rowIndex = 1;
			var columnIndex = 1;
			
			foreach (var column in columnNames)
			{
				sheet.Cells[1, columnIndex] = column;
				columnIndex++;
			}
			
			sheet.Cells[1, columnIndex] = "ResultText";
			sheet.Cells[1, ++columnIndex] = "Status";
			var columnStatus = columnIndex;
			sheet.Cells[1, ++columnIndex] = "Error";
			var columnError = columnIndex;
			sheet.Cells[1, ++columnIndex] = "Path";
			var columnPath = columnIndex;
			
			rowIndex++;
			
			maxCountDocument = maxCountDocument > 0 ? maxCountDocument : allDocumentCount;
			
			var progressBar = new ProgressBarDRX(maxCountDocument); //объект прогресс бар
			progressBar.Show();

			progressBar.Text(string.Format("Загрузка документов в Excel: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));

			while (document != null && ((maxCountDocument <= 0 && cntLoad < allDocumentCount) || (maxCountDocument > 0 && cntLoad < maxCountDocument)))
			{
				cntAll++;
				columnIndex = 1;
				
				try
				{
					lotusValues = GetValueRequisites(document, 1000, columnNames);
					
					foreach (var lotusValue in lotusValues)
					{
						sheet.Cells[rowIndex, columnIndex] = lotusValue.Value;
						columnIndex++;
					}
					
					if (docType.Equals("Входящее письмо"))
						document = notesView.GetNextDocument(document);
					else
						document = documents.GetNextDocument(document);
					
					
					var noteAttachID = !string.IsNullOrEmpty(lotusValues["Orig_ID"]) ? lotusValues["Orig_ID"] :
						!string.IsNullOrEmpty(lotusValues["Orig_ID_F"]) ? lotusValues["Orig_ID_F"] :
						string.Empty;

					var noteAttachReplicaID = !string.IsNullOrEmpty(lotusValues["Orig_ReplicaId"]) ? lotusValues["Orig_ReplicaId"] :
						!string.IsNullOrEmpty(lotusValues["Orig_ReplicaId_F"]) ? lotusValues["Orig_ReplicaId_F"] :
						string.Empty;

//					NotesDocument documentWithAttach = !string.IsNullOrEmpty(noteAttachID) ? ndbOrigDocuments.GetDocumentByUNID(noteAttachID) : null;

					NotesDocument documentWithAttach = document;
					var nameDocumentList = GetNameAttachmentList(documentWithAttach);

					if (!string.IsNullOrEmpty(noteAttachID) && nameDocumentList.Count == 0)
					{
						documentWithAttach = ndbOrigDocuments.GetDocumentByUNID(noteAttachID);
						nameDocumentList = GetNameAttachmentList(documentWithAttach);
					}

					if (!string.IsNullOrEmpty(noteAttachReplicaID) && nameDocumentList.Count == 0)
					{
						documentWithAttach = ndbOrigDocuments.GetDocumentByUNID(noteAttachReplicaID);
						nameDocumentList = GetNameAttachmentList(documentWithAttach);
					}

					if (documentWithAttach != null && nameDocumentList.Count > 0)
					{
						sheet.Cells[rowIndex, columnStatus] = "Не загружен";
						columnIndex = columnPath;
						
						for (var i = 0; i < nameDocumentList.Count; i++)
						{
							var path = string.Format("{0}CM {1} {2} {6} ({5})[ID {3}]{4}",
							                         tempFolder,
							                         datetimeNow,
							                         docType,
							                         lotusValues["DocID"],
							                         System.IO.Path.GetExtension(nameDocumentList[i]),
							                         System.IO.Path.GetFileNameWithoutExtension(nameDocumentList[i]),
							                         i.ToString());

							documentWithAttach.GetAttachment(nameDocumentList[i]).ExtractFile(path);
							
							sheet.Cells[rowIndex, columnIndex] = path;
							columnIndex++;
						}
					}
					else
					{
						cntWithoutAttach++;
						sheet.Cells[rowIndex, columnStatus] = "Не загружено вложение";
						sheet.Cells[rowIndex, columnError] = "Нет вложенных документов";
					}
					
					cntLoad++;
					
					progressBar.Text(string.Format("Загрузка документов: {0}/{1}...\r\nБез вложений: {2}. Кол-во ошибок: {3}.", cntLoad, maxCountDocument, cntWithoutAttach, cntError));
					progressBar.Next();
					
				}
				catch (Exception error)
				{
					cntError++;
					sheet.Cells[rowIndex, columnStatus] = "Ошибка миграции";
					sheet.Cells[rowIndex, columnError] = error.Message;
				}
				
				rowIndex++;
			}
			
			var range = sheet.UsedRange;
			
			range.EntireColumn.AutoFit();
			range.RowHeight = 25;
			
			progressBar.Hide();
			progressBar = null;
			
			ex.Application.ActiveWorkbook.SaveAs(excelPath, Type.Missing,
			                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange,
			                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
			
			workBook.Close();
			ex.Quit();
			
			sheet = null;
			workBook = null;
			ex = null;
			
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
		}
		
		/// <summary>
		/// Выдача прав на приказ/распоряжение.
		/// </summary>
		public static void GrantRightsOnDocument(IOrder document, List<IRecipient> listRecipients)
		{
			foreach (var recipient in listRecipients)
			{
				if (document.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, recipient))
				{
					document.AccessRights.Grant(recipient, DefaultAccessRightsTypes.FullAccess);
					document.AccessRights.Save();
				}
			}
		}
		
	}
}